@extends('shop.master.index')
@section('shop-main')
<section class="featured spad hide" id="main">
    <div class="col-lg-12">
        <div class="checkout__title">
            <div class="container">
               @if ($res == "1")
               <div><img style="width: 50vw;max-width:22vh;padding-bottom: 30px;" src="/img/svg/undraw_successful_purchase_uyin.svg" alt=""></div>
               <h2 style="margin-bottom: 20px;"><i class="fa fa-check-circle" style="padding-left: 10px;color:#54a6ff;"></i>سفارش شما با موفقیت انجام شد</h2>
               @else
               <div><img style="width: 50vw;max-width:22vh;padding-bottom: 30px;" src="/img/svg/undraw_access_denied_6w73.svg" alt=""></div>
               <h2   style="color: #af0000;background-color: #ff9595;font-size: calc(12px + 1vw);border-radius: 50px;"><i class="fa fa-times-circle" style="padding-left: 10px;color:#ec131d;"></i>سفارش شما با موفقیت انجام نشد</h2>
               @endif
           
    @if ($res==1)
          
      
            <div class="featured__controls">
                <ul>
                    <li class="active alert alert-warning">لطفا شماره سفارش را برای پیگیری یادداشت نمایید</li><br/>
                <li class="active" style="padding-top: 25px">شماره سفارش: {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($orderID)}}</li>
                </ul>
            </div>
        @if ($havefile!=0)

            <div class="shoping__cart__table">
            <table id="fillcart">
                <thead>
                    <tr>
                        <th>نام</th>
                        <th>لینک دانلود</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($orders as $item)
                @if ($item->ofile!=0)

                <tr>
                    <td>
                        {{$item->name}}
                    </td>
                    <td>
                    <a id="previewlink"  href="{{ URL::to('/') }}/getfile/{{$item->ofile}}/{{$orderID}}" class="primary-btn" style="background-color: #acff9d;">دریافت</a>
                    </td>
                </tr>
                @endif 
                
                @endforeach
            </tbody>
            </table></div>
        @endif
        
    @endif
    </div>
    </div>
    </div>
    </section>
@if ($res==1)c
<script>
    $(window).on('load', function() {
        var myJsonData = { "_token": "{{ csrf_token() }}", orderID: "{{$orderID}}", site: "{{$site}}",havefile: "{{$havefile}}" , mail: "{{$orders[0]->email}}" };
        $.post('/sendmail', myJsonData, function(response) {
            console.log(response);
    });
    });
    
    </script>
@endif
@endsection