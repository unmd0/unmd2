@extends('shop.master.index')
@section('shop-main')
        <!-- Featured Section Begin -->

        <section class="featured spad hide" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h2 {{DB::table('profiles')->where('userid','=',$site)->first()->name? :'style=font-size: 1em;color: red;'}}>
                                @php echo(DB::table('profiles')->where('userid','=',$site)->first()->name ? DB::table('profiles')->where('userid','=',$site)->first()->name : "<small>" . 'لطفا در تنظیمات<a href="http://8190.org/admin/profile" style="color: #0296ff !important"> پروفایل </a> نام فروشگاه خود را به فارسی وارد نمایید' . "</small>") @endphp
                            </h2>
                        </div>
                        <div class="featured__controls">
                            <ul>

                                <li class="active" data-filter="*">همه</li>
                                @foreach (DB::table('category')->where('shopname','=',$site)->get() as $item)
                                @if (DB::table('products')->where('shopname','=',$site)->where('category','=',$item->cname)->first())
                                <li data-filter=".{{str_replace(' ', '_', $item->cname)}}">{{$item->cname}}</li>
                                @endif

                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row featured__filter">

                    @foreach ($products as $product)
                    @if (DB::table('big_s_products_type')->where("ProductId","=",$product->productID)->first()!="")
                    <div class="col-lg-3 col-md-4 col-sm-6 mix oranges {{str_replace(' ', '_', $product->category)}}">
                        <a href="{{url()->current()}}/details/{{$product->productID}}">
                        <div class="featured__item">

                            <div class="featured__item__pic set-bg" data-setbg="{{ URL::to('/') }}/img/<?php echo(explode("*",$product->pic)[1]); ?>.jpg">


                                {{-- <ul class="featured__item__pic__hover">
                                    <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                </ul> --}}

                            </div>
                            @php $isoff=0; @endphp
                            @foreach (DB::table('big_s_products_type')->where("ProductId","=",$product->productID)->orderByRaw('LENGTH(offer)', 'ASC')->orderBy('offer' , 'asc')->get() as $prc)
                                @if ($prc->offer > 0)
                                    @php
                                    $pric = $prc->Price;
                                    $off = $prc->offer;
                                    $isoff=1;
                                    @endphp
                            @break
                                @endif
                            @endforeach
                            <div class="featured__item__text">
                            <h6>{{$product->name}}</h6>

                            @if ($isoff == 0)
                            <h5 style="color: #000;">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(DB::table('big_s_products_type')->where("ProductId","=",$product->productID)->orderByRaw('LENGTH(Price)', 'ASC')->orderBy('Price' , 'asc')->first()->Price,3)}} تومان</h5>
                            @else
                            <h5 style="color: #000;text-decoration: line-through;">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($pric,3)}} تومان</h5>
                            <h5 class="offer_price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($off,3)}} تومان</h5>
                            @endif

                            </div>

                        </div>
                    </a>
                    </div>
                    @endif
                    @endforeach

                </div>
            </div>
        </section>
        <!-- Featured Section End -->
@endsection
