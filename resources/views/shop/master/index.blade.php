<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>فروشگاه {{DB::table('profiles')->where('userid','=',$site)->first()->name? DB::table('profiles')->where('userid','=',$site)->first()->name:''}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
@yield('meta_add')
@yield('add_link')
    @include('shop.master.layouts.links.index')

</head>

<body>
    @include('shop.master.layouts.header.index')
    {{-- @include('shop.master.layouts.menu.index') --}}
    @yield('shop-main')
    @include('shop.master.layouts.footer.index')
    @yield('add_js')
</body>

</html>
