<footer class="footer spad hide" id="footer">
    <div class="container">
        <div class="row">
<div style="display: flex;margin: auto;list-style-type: none;">
    @if (DB::table('profiles')->where('userid','=',$site)->first()->instagram != "")
    <li><a target="_blank" href="https://www.instagram.com/{{DB::table('profiles')->where('userid','=',$site)->first()->instagram}}"><i class="fab fa-instagram instafooter"></i></a></li>
    @endif
    @if (DB::table('profiles')->where('userid','=',$site)->first()->telegram != "")
    <li><a target="_blank" href="https://t.me/{{DB::table('profiles')->where('userid','=',$site)->first()->telegram}}"><i class="fab fa-telegram" style="color: #0088CC;font-size: 35px;border-radius: 100%;padding: 2px 6px;"></i></a></li>
    @endif
</div></div></div><br/>
<form action="/order/track" method="post" style="display: grid;margin: auto;">
    @csrf
    <input type="text" name="q" class="hide">
<button class="btn brn-success" type="submit">پیگیری سفارش</button>
</form>


            <br/>
            <div id="zarinpal" style="margin: auto;display: table;padding-bottom: 35px;">
            <script src="https://www.zarinpal.com/webservice/TrustCode" type="text/javascript"></script>
</div>
<br/>
</footer>

<script src="/js/organi/bootstrap.min.js"></script>
{{-- <script src="/js/organi/jquery.nice-select.min.js"></script> --}}
<script src="/js/organi/jquery-ui.min.js"></script>
<script src="/js/organi/jquery.slicknav.js"></script>
<script src="/js/organi/mixitup.min.js"></script>
<script src="/js/organi/owl.carousel.min.js"></script>
<script src="/js/organi/main.js"></script>
<script>
        function updatecart(X,Y){
        var myJsonData = { "_token": "{{ csrf_token() }}", id: Y, qty: X }
    $.post('{{url()->current()}}/updatecart', myJsonData, function(response) {
        var responce = JSON.parse(response);
    document.getElementById("cartcount").innerHTML=toPersianNum(responce[0]);
    document.getElementById(Y).innerHTML=toPersianNum(responce[1]) + "<span> تومان </span>";
    document.getElementById("subtotal").innerHTML=toPersianNum(responce[2]);
});
}
function deletecart(X,Y){
    window.Z;
X.parentElement.parentElement.parentElement.remove();

    var myJsonData = { "_token": "{{ csrf_token() }}", id: Y }
    $.post('{{url()->current()}}/deletecart', myJsonData, function(response) {
        var responce = JSON.parse(response);
    document.getElementById("cartcount").innerHTML=toPersianNum(responce[0]);
    document.getElementById("subtotal").innerHTML=toPersianNum(responce[1]) ;
    emptycart(responce[0]);
});

}
$( "#cartcount" ).hover(
  function() {
    $( "#bagi" ).css( "font-weight","900" );
  }, function() {
    $( "#bagi" ).css( "font-weight","300" );
  }
);
function emptycart(X){
    // alert(X);
    // var X = document.getElementById("cartcount").innerHTML;
    if (X != 0){
        document.getElementById('emptycart').classList.add('hide');
        document.getElementById('checkoutf').classList.remove('hide');
        document.getElementById('fillcart').classList.remove('hide');
    }
    else{
        document.getElementById('fillcart').classList.add('hide');
        document.getElementById('checkoutf').classList.add('hide');
        document.getElementById('emptycart').classList.remove('hide');
    }

}


function toPersianNum( num, dontTrim ) {

var i = 0,

    dontTrim = dontTrim || false,

    num = dontTrim ? num.toString() : num.toString().trim(),
    len = num.length,

    res = '',
    pos,

    persianNumbers = typeof persianNumber == 'undefined' ?
        ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'] :
        persianNumbers;

for (; i < len; i++)
    if (( pos = persianNumbers[num.charAt(i)] ))
        res += pos;
    else
        res += num.charAt(i);

return res;
}
</script>
<script>
    function logElementEvent(eventName, element) {
      console.log(
        Date.now(),
        eventName,
        element.getAttribute("data-bg88da112-bg-hidpi=bg")
      );
    }

    var callback_enter = function (element) {
      logElementEvent("🔑 ENTERED", element);
    };
    var callback_exit = function (element) {
      logElementEvent("🚪 EXITED", element);
    };
    var callback_loading = function (element) {
      logElementEvent("⌚ LOADING", element);
    };
    var callback_loaded = function (element) {
      logElementEvent("👍 LOADED", element);
    };
    var callback_error = function (element) {
      logElementEvent("💀 ERROR", element);
      element.src =
        "https://via.placeholder.com/220x280/?text=Error+Placeholder";
    };
    var callback_finish = function () {
      logElementEvent("✔️ FINISHED", document.documentElement);
    };
    var callback_cancel = function (element) {
      logElementEvent("🔥 CANCEL", element);
    };

    LL = new LazyLoad({
      // Assign the callbacks defined above
      callback_enter: callback_enter,
      callback_exit: callback_exit,
      callback_cancel: callback_cancel,
      callback_loading: callback_loading,
      callback_loaded: callback_loaded,
      callback_error: callback_error,
      callback_finish: callback_finish
    });
    $( document ).ready(function() {

    document.getElementById('footer').classList.remove('hide');
    document.getElementById('main').classList.remove('hide');


});
  </script>
