@php
    $stylever = "2.0249";
@endphp
<link rel="stylesheet" href="/css/organi/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/organi/font-awesome.min.css?version=5.0" type="text/css">
<link rel="stylesheet" href="/css/organi/brands.min.css?version={{$stylever}}" type="text/css">
<link rel="stylesheet" href="/css/organi/light.min.css?version={{$stylever}}" type="text/css">
<link rel="stylesheet" href="/css/organi/regular.min.css?version={{$stylever}}" type="text/css">
<link rel="stylesheet" href="/css/organi/solid.min.css?version={{$stylever}}" type="text/css">
<link rel="stylesheet" href="/css/organi/elegant-icons.css" type="text/css">
<link rel="stylesheet" href="/css/organi/nice-select.css" type="text/css">
<link rel="stylesheet" href="/css/organi/jquery-ui.min.css" type="text/css">
<link rel="stylesheet" href="/css/organi/owl.carousel.min.css" type="text/css">
<link rel="stylesheet" href="/css/organi/slicknav.min.css" type="text/css">
<link rel="stylesheet" href="/css/organi/style.css?version={{$stylever}}" type="text/css">
<link rel="stylesheet" href="/css/organi/searchbar.css" type="text/css">
<script src="/js/organi/jquery-3.3.1.min.js"></script>
<script src="/js/organi/lazy.js"></script>
{{-- <link rel="stylesheet" href="/css/index.css"> --}}

<!-- <script src="js/e1281774.js.download"></script>
    <script src="js/3f518f9b.js.download"></script>
    <script src="js/17f93318.js.download"></script>
    <script src="js/7cd0b97a.js.download"></script>  -->

