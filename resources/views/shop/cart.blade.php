@extends('shop.master.index')
@section('shop-main')
<section class="shoping-cart spad hide" id="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <div class="topcontainer">
        
                        <h2><li class="fas fa-shopping-bag"></li></h2>
                    <div class="line"></div> 
                </div>
                    <div id="emptycart" class="hide">
                        <div class="col-lg-12">
                            <div class="section-title">
                                <h3>سبد کالا خالی میباشد</h3>
                            </div>
                            <div class="section-title">
                                <a href="/"><h5 style="color: #538ce0 !important;">محصولات</h5></a>
                            </div>
                        </div>
                    </div>
                    {{-- <table id="fillcart" class="hide">
                        <thead>
                            <tr>
                                <th class="shoping__product">محصول</th>
                                <th>قیمت</th>
                                <th>تعداد</th>
                                <th>قیمت کل</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                                @foreach ($carts as $item)

                                <tr>
                                <td class="shoping__cart__item">
                                    <img src="{{$item->options->pic}}" alt=""><br class="br" style="display: none;"><br class="br" style="display: none;">
                                    <h5>{{$item->name}} - {{$item->options->type->Name}}</h5>
                                </td>
                                <td class="shoping__cart__price" style="text-align: bottom;">
                                    {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->price,3)}}
                                </td>
                                @if ($item->options->type->ofile == 0)
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                        <div class="pro-qty" data-id="{{$item->rowId}}">
                                                <span class="dec qtybtn" >-</span>
                                                <input type="text" value="{{$item->qty}}">
                                                <span class="inc qtybtn">+</span>
                                            </div>
                                        </div>
                                    </td>
                                @else
                                <td><span style="background-color: whitesmoke;padding: 2px 15px;">{{$item->qty}}</span></td>
                                @endif
                               
                                <td class="shoping__cart__total" id="{{$item->rowId}}">
                                    {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->subtotal,3)}}
                                </td>
                                <td class="shoping__cart__item__close" onclick="deletecart(this,'{{$item->rowId}}')">
                                    <span class="icon_close"></span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table> --}}

                    <div class="container" id="fillcart" class="hide">
                        {{-- <h2 style="text-align: right;margin-bottom: 36px;"><small style="font-size: 20px; font-family: kalameh;"><a href="/" style="color: rgb(12, 62, 104)">صفحه اصلی </a> > </small> {{$ctg->cname}} </h2> --}}
                        <div class="row featured__filter">
        
                            @foreach ($carts as $item)
                                
                            
                            <div class="col-lg-3 col-md-4 col-sm-8 mix cartcart oranges">
                                
                                {{-- <a href="{{url("/details/".$product->productID)}}"> --}}
                                <div class="featured__item featured__item2">
                                    
                                    <div class="featured__item__pic featured__item__pic2 set-bg" data-setbg="{{$item->options->pic}}">
                                       
                                    
                                        {{-- <ul class="featured__item__pic__hover">
                                            <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
                                        </ul> --}}
                                    
                                    </div>
                                    <div class="featured__item__text featured__item__text2">
                                        <h6>{{$item->name}} - {{$item->options->type->Name}}</h6>
                                        <hr/>
                                        <h5 class="normal_price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->price,3)}} <span>تومان</span></h5>
                                    @if ($item->options->type->ofile == 0)
                                        <td class="shoping__cart__quantity">
                                            <div class="quantity">
                                            <div class="pro-qty" data-id="{{$item->rowId}}">
                                                    <span class="dec qtybtn" >-</span>
                                                    <input type="text" value="{{$item->qty}}">
                                                    <span class="inc qtybtn">+</span>
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                        <h5 class="offer_price" id="{{$item->rowId}}">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->subtotal,3)}} <span>تومان</span></h5>

                                        <h5  class="btn btn-danger imclosebtn" onclick="deletecart(this,'{{$item->rowId}}')"><i class="fas fa-times-circle"></i> </h5>
                                        </div>
                                   
                                </div>
                                {{-- </a> --}}
                            </div>
                            @endforeach
        
                        </div>
                    </div>





                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6">
                <div class="shoping__checkout hide" id="checkoutf">
                   
                    <ul>
                        <li>قیمت مجموع <span class="price" id="subtotal">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::subtotal(0,0,""),3)}}</span><span class="toman">تومان</span></li>
                    </ul>
                    <a href="/checkout" class="primary-btn primary-btn-green fixbtn">رفتن به مرحله بعد</a>
                </div>
            </div>
        </div>
    </div>
</section>




<script>
    $( document ).ready(function() {

     emptycart({{Cart::count()}});
     $("#footer").hide();
});
</script>
@endsection
