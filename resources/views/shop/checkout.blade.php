@extends('shop.master.index')
@section('shop-main')
<style>
    textarea::placeholder {
  color: #adadad;
}
</style>
<section class="checkout spad">
    <div class="container">
        <div class="checkout__form">
            <div class="topcontainer">

                <h2><i class="fas fa-shipping-fast"></i></h2>
            <div class="line"></div>
        </div>
            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @php
            $phy=0;
            $ifile=0;
        @endphp
        @foreach ($carts as $item2)
        @if ($item2->options->type->ofile == 0)
        @php
            $phy=1;
        @endphp
        @else
        @php
            $ifile=1;
        @endphp
        @endif
        @endforeach


            <form action="/checkoutstore" method="POST">
                @csrf
                @method('PUT')
                <input type="text" name="ofile" value="{{$phy}}" class="hide">
                <div class="row">
                    <div class="col-lg-8 col-md-6">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout__input">

                                    <p>نام<span>*</span></p>
                                    <input type="text" name="firstname">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>نام خانوادگی<span>*</span></p>
                                    <input type="text" name="lastname">
                                </div>
                            </div>
                        </div>
                        <div id="address_section" class="{{$phy == 0 ? "hide": ""}}">
                        <div class="checkout__input">
                            <p>استان<span>*</span></p>
                            <input type="text" name="state">
                        </div>
                        <div class="checkout__input">
                            <p>شهر<span>*</span></p>
                            <input type="text"  name="city">
                        </div>
                        <div class="checkout__input">
                            <p>آدرس<span>*</span></p>
                            <input type="text"  name="address" placeholder="آدرس پستی" class="checkout__input__add">
                        </div>

                        <div class="checkout__input">
                            <p>کد پستی<span>*</span></p>
                            <input type="text"  name="zipcode">
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>شماه همراه<span>*</span></p>
                                    <input type="text"  name="phone">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="checkout__input">
                                    <p>ایمیل @if ($ifile==1) <span>*</span> @endif </p>
                                    <input type="text" style="direction: ltr;" id="email" name="email">
                                </div>
                            </div>
                        </div>
                            <div class="alert alert-warning">
                                <h6 style="color: #6f5507;
                                text-align: center;
                                background: #f7de92;
                                border-bottom: 1px solid #0fbd8524;
                                padding: 12px 0 12px;
                                margin-bottom: 0px;
                                font-size: 0.8rem;
                                font-weight: bold;
                                line-height: 32px;">اطلاعات ذیل را با دقت مطالعه و به ترتیب در قسمت توضیحات سفارش وارد کنید</h6><br />
                                اطلاعات مربوط به هدیه گیرنده :<br />
                                *نام و نام خانوادگی<br />
                                *مناسبت<br />
                                *تاریخ مناسبت (در صورت امکان همراه با ساعت)<br />
                                *شهر و استان تولد<br />
                                *جمله فارسی یا انگلیسی<br />
                                *رنگ قاب سفید و مشکی  (رنگ ها و طرح های دیگر به صورت سفارشی پذیرفته میشود)<br />
                                *کد طرح (در صورت وجود کد)<br />
                                *چنانچه مایل به اضافه کردن آیتم و یا ایجاد تغییر در طراحی هستید ذکر کنید.<br />
                                *برای مشاوره برای سفارش با شماره 09396941599 تماس و یا به واتس اپ پیام دهید.<br />
                            </div>
                        <div class="checkout__input">
                            <p>توضیحات سفارش</p>
                            <textarea type="text"
                                placeholder="صدف مهرتاش
تولد
1375/11/21 00:00
البرز-کرج
زمینی شدنت مبارک
قاب سفید
" name="desc" rows="3" style="margin-top: 0px;margin-bottom: 0px;height: 154px;width: 100%;resize: none;border-color: #ebebeb;"></textarea>
                        </div>
                        <div class="checkout__input__checkbox">
                            <label for="acc">
                                ذخیره اطلاعات ارسال برای سفارشات بعدی شما
                                <input type="checkbox" id="acc">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <p>با فعال کردن
                             این گزینه اطلاعات ارسال ورودی در مرورگر
                            شما ذخیره میشود و در خریدهای بعدی
                             میتوانید بدون وارد کردن
                             اطلاعات ارسال سفارش خود را نهایی کنید</p>

                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="checkout__order">
                            <div class="checkout__order__products">سفارشات</div>
                            <ul>
                                @foreach ($carts as $item)
                                <li><div class="dontover">{{$item->name}}</div><span class="toman">تومان</span> <span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->subtotal,3)}}</span></li>
                                @endforeach
                            </ul>
                            <div class="checkout__order__subtotal">جمع خرید <span class="toman">تومان</span><span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::subtotal(0,0,""),3)}}</span></div>
                            <div class="checkout__order__total">هزینه ارسال <span class="toman">تومان</span><span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($ship,3)}}</span></div>
                            <div class="checkout__order__total">پرداختی <span class="toman">تومان</span><span class="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::subtotal(0,0,"")+$ship,3)}}</span></div>
                            <button onclick="sb('1')" type="button" class="site-btn primary-btn primary-btn-green"> پرداخت آنلاین</button>
                            <button onclick="sb('0')" type="button" class="site-btn primary-btn primary-btn-green">پرداخت پس از طراحی</button>
                            <input type="text"  name="mny" id="mny" class="hide">
                            <input type="submit" id="sub" class="hide">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    function sb(x){
    if({{$ifile}}==1 && document.getElementById('email').value==""){
        alert("به علت وجود فایل در سبد خرید، لطفا آدرس ایمیل خود را وارد کنید.");
        return "";
    }
    document.getElementById("mny").value=x;
    document.getElementById("sub").click();
}
</script>
@endsection
