@extends('shop.master.index')
@section('shop-main')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
    <!-- Product Details Section Begin -->
    <section class="product-details spad hide" id="main">
        <div class="container">
            <div class="row">
            <span id="idhere">{{$id}}</span>
            <span id="Tidhere">{{$tp0->TypeId}}</span>
            <span id="pichere">{{ URL::to('/') }}/img/<?php echo(explode("*",$products->pic)[1]); ?>.jpg</span>
                <div class="col-lg-6 col-md-6" style="overflow: hidden;">
                    <!-- قسمت عکس ها -->
                    <div class="product__details__pic">
                        <!-- عکس اصلی -->
                        <div class="product__details__pic__item">
                            <img class="featured__item__pic" style="background-size: cover;
                                background-image: url({{ URL::to('/') }}/img/<?php echo(explode("*",$products->pic)[1]); ?>.jpg);" alt="">
                        </div>
                        <!-- عکس اصلی -->
                        <!-- عکس ها -->
                        <div class="product__details__pic__slider owl-carousel">
                  @foreach (explode(",",str_replace("*","",$products->pic)) as $item)
                  <img onclick="chngpic(this)" data-imgbigurl="{{ URL::to('/')}}/img/{{$item}}.jpg"
                  src="{{ URL::to('/')}}/img/{{$item}}.jpg" alt="">
                  @endforeach

                        </div>
                        <!-- عکس ها -->
                    </div>
                </div>




                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3 id="name">{{$products->name}}</h3>
                        {{-- @if ($tp0->offer > 0) --}}
                        @php
                        $finalprice=$tp0->Price;
                        $secondprice="";
                        if($tp0->offer != 0){
                            $finalprice=$tp0->offer;
                            $secondprice=$tp0->Price;
                            $secondprice=MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($secondprice,3);
                          }
                        @endphp
                        <small id="secondprice" style="text-decoration: line-through;">{{ $secondprice }}</small>
                        <div class="product__details__price"><span id="price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($finalprice,3)}}</span><span class="toman"> تومان </span> </div>

                        <div class="form-group">
                            <label for="sel1">انتخاب تنوع:</label>
                            <select  onchange="typec(this)"  class="form-control" id="sel1">
                              @foreach ($types as $type)
                              @php
                              $finalprice=$type->Price;
                              $secondprice="";
                              if($type->offer != 0){
                                  $finalprice=$type->offer;
                                  $secondprice=$type->Price;
                                  $secondprice=MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($secondprice,3);
                                }
                              @endphp
                            <option data-price="{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($finalprice,3)}}"
                                    data-offer="{{$secondprice}}"
                                    data-id="{{$type->TypeId}}"
                            @if ($tp0->ofile != 0)
                            data-file="{{$type->ofile}}"
                            data-preview="{{$type->pfile != 0 ? \App\filemanage::where('id','=',$type->pfile)->first()->ect : '0'}}"
                            data-previewlink="{{$type->pfile != 0 ?  URL::to('/') . \App\filemanage::where('id','=',$type->pfile)->first()->address : '0' }}"
                            @endif>
                            {{$type->Name}}</option>
                              @endforeach
                            </select>
                          </div>


                        @if ($tp0->ofile == 0)
                        <input type="text" id="ofile" class="hide" value="0">
                        <div class="product__details__quantity">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <span class="dec qtybtn" >-</span>
                                    <input type="text" id="qty" value="1">
                                    <span class="inc qtybtn">+</span>
                                </div>
                            </div>
                        </div>
                        @elseif ($tp0->pfile != 0)
                        <input type="text" id="ofile" class="hide" value="{{$tp0->ofile}}">
                        <div class="product__details__quantity hidemain">
                            <div class="quantity">
                                <div class="pro-qty">
                                    <span class="dec qtybtn" >-</span>
                                    <input type="text" id="qty" value="1">
                                    <span class="inc qtybtn">+</span>
                                </div>
                            </div>
                        </div>
                            <a id="previewlink" download="{{\App\filemanage::where('id','=',$tp0->pfile)->first()->ect}}" href="{{ URL::to('/') }}{{\App\filemanage::where('id','=',$tp0->pfile)->first()->address}}" class="primary-btn" style="background-color: #acff9d;">دریافت فایل پیش نمایش</a>
                        @endif

                        <a onclick="addcart(this)" class="primary-btn fixbtn">اضافه کردن به سبد خرید</a>
                        <div style="margin-right: 10px;">
                            <input value="{{ Request::url() }}" class="itslabael" id="target">
                        <i class="fa fa-share-alt iscopy apashe" style="cursor: pointer;margin-left: 5px;" data-clipboard-target="#target" aria-hidden="true"></i>
                        <a href="https://t.me/share/url?url={{ Request::url() }}&text={{$products->name}}" target="_blank"><i class="fab fa-telegram-plane" aria-hidden="true"></i></a>
                        </div>
                        <ul>

                            <!-- <li><b>Availability</b> <span>In Stock</span></li> -->
                            <!-- <li><b>Shipping</b> <span>01 day shipping. <samp>Free pickup today</samp></span></li>
                            <li><b>Weight</b> <span>0.5 kg</span></li> -->
                        </ul>
                        <p style="white-space: pre-line">{{$products->desc}}</p>

                    </div>
                </div>
                <div class="col-lg-12">

                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->
<script>
    function typec(x)
    {
        document.getElementById("price").innerHTML=$(x).find(':selected').data('price');
        document.getElementById("secondprice").innerHTML=$(x).find(':selected').data('offer');
        document.getElementById("Tidhere").innerHTML=$(x).find(':selected').data('id');
        $("#previewlink").hide();
        if($(x).find(':selected').data('previewlink') != 0){
        $("#previewlink").show();
        document.getElementById("ofile").value=$(x).find(':selected').data('file');
        $("#previewlink").prop("href",$(x).find(':selected').data('previewlink'));
        $("#previewlink").prop("download",$(x).find(':selected').data('preview'));
        }
    }
        function chngpic(x)
    {
        $('.featured__item__pic').css('background-image', 'url(' + $(x).data( "imgbigurl" ) + ')');
    }
    function addcart(X){
        var idj=document.getElementById('idhere').innerHTML;
        var tidj=document.getElementById('Tidhere').innerHTML;
        var picj=document.getElementById('pichere').innerHTML;
        var namej = document.getElementById('name').innerHTML;
        var pricej = document.getElementById('price').innerHTML;
        var qtyj = document.getElementById('qty').value;
        var ofilej = document.getElementById('ofile').value;
        pricej=toEnglishDigits(pricej.replace(",",""));
        var myJsonData = { "_token": "{{ csrf_token() }}", id: idj, tid: tidj,name: namej, price: pricej, qty: qtyj,pic: picj,ofile: ofilej }
    $.post('{{url()->current()}}/addcart', myJsonData, function(response) {
    document.getElementById("cartcount").innerHTML=toPersianNum(response);
    window.location.replace("/cart");
});

    }
    function toEnglishDigits(str) {

// convert persian digits [۰۱۲۳۴۵۶۷۸۹]
var e = '۰'.charCodeAt(0);
str = str.replace(/[۰-۹]/g, function(t) {
    return t.charCodeAt(0) - e;
});

// convert arabic indic digits [٠١٢٣٤٥٦٧٨٩]
e = '٠'.charCodeAt(0);
str = str.replace(/[٠-٩]/g, function(t) {
    return t.charCodeAt(0) - e;
});
return str;
}
</script>
<script>
    $(document).ready(function(){
      var clipboard = new Clipboard('.iscopy');
      clipboard.on('success', function(e) {
    alert("لینک محصول کپی شد.");

        e.clearSelection();
    });
    });

    </script>

    @endsection
