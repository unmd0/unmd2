<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Verify your email address</title>
  
  
<style> @font-face {
    font-family: 'BYekan';
    src: url('http://fontonline.ir/fonts/BYekan.eot?#') format('eot'),
    url('http://fontonline.ir/fonts/BYekan.ttf') format('truetype'),
    url('http://fontonline.ir/fonts/BYekan.woff') format('woff');
    }
body {
width: 100% !important; height: 100%; margin: 0; line-height: 1.4; background-color: #F5F7F9; color: #839197; -webkit-text-size-adjust: none; float: right; direction: rtl;
}
@media only screen and (max-width: 600px) {
  .email-body_inner {
    width: 100% !important;
  }
  .email-footer {
    width: 100% !important;
  }
}
@media only screen and (max-width: 500px) {
  .button {
    width: 100% !important;
  }
}
</style></head>
<body style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100% !important; height: 100%; line-height: 1.4; color: #839197; -webkit-text-size-adjust: none; float: right; direction: rtl; margin: 0;" bgcolor="#F5F7F9">
  <table width="50%" cellpadding="0" cellspacing="0" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; width: 50%; margin: 0; padding: 0;margin: auto;" bgcolor="#F5F7F9">
    <tr>
      <td align="center" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box;">
        <table width="100%" cellpadding="0" cellspacing="0" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 0; padding: 0;">
          <!-- Logo -->
          <tr>
            <td style="direction: ltr; font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 25px 0;" align="center">
              <a href="https://www.8190.org" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; color: #839197; font-size: 16px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">8190.org</a>
            </td>
          </tr>
          <!-- Email Body -->
          <tr>
            <td width="100%" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; border-top-width: 1px; border-top-color: #E7EAEC; border-top-style: solid; border-bottom-width: 1px; border-bottom-color: #E7EAEC; border-bottom-style: solid; margin: 0; padding: 0;" bgcolor="#FFFFFF">
              <table align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; width: 570px; margin: 0 auto; padding: 0;">
                <!-- Body content -->
                <tr>
                  <td style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 35px;">
                    <h1 style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; margin-top: 0; color: #292E31; font-size: 19px; font-weight: bold;" align="right">بزودی کسب و کارت تحول پیدا میکنه</h1>
                    <p style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; margin-top: 0; color: #839197; font-size: 16px; line-height: 1.5em;" align="right">با تشکر از ثبت نام شما در پلتفرم 8190.</p>
                    <h3 style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; margin-top: 0; color: #292E31; font-size: 14px; font-weight: bold;" align="right">برای تایید حساب خود برروی دکمه زیر کلیلک کنید.</h3>
                    <!-- Action -->
                    <table align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; text-align: center; margin: 30px auto; padding: 0;">
                      <tr>
                        <td align="center" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box;">
                          <div style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box;">
                            <!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{$actionUrl}}" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="7%" stroke="f" fill="t">
                            <v:fill type="tile" color="#414EF9" />
                            <w:anchorlock/>
                            <center style="color:#ffffff;font-family:sans-serif;font-size:15px;">Verify Email</center>
                          </v:roundrect><![endif]-->
                            <a href="{{$actionUrl}}" style="font-size: 17px; font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; color: #ffffff; display: inline-block; width: 200px; background-color: #414EF9; border-radius: 3px; line-height: 45px; text-align: center; text-decoration: none; -webkit-text-size-adjust: none; mso-hide: all;">تایید ایمیل</a>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <p style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; margin-top: 0; color: #839197; font-size: 16px; line-height: 1.5em;" align="right">با تشکر<br />تیم 8190</p>
                    <!-- Sub copy -->
                    <table style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; margin-top: 25px; padding-top: 25px; border-top-width: 1px; border-top-color: #E7EAEC; border-top-style: solid;">
                      <tr>
                        <td style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box;">
                          <p style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; margin-top: 0; color: #839197; font-size: 12px; line-height: 1.5em;" align="right">همچنین میتوانید از آدرس زیر برای تایید حساب کاربری استفاده کنید.
                          </p>
                          <p style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; margin-top: 0; color: #839197; font-size: 12px; line-height: 1.5em;" align="right"><a href="{{$actionUrl}}" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; color: #414EF9;">{{$actionUrl}}</a></p>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box;">
              <table align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; width: 570px; text-align: center; margin: 0 auto; padding: 0;">
                <tr>
                  <td style="font-family: BYekan,'BYekan',tahoma; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 35px;">
                    <p style="direction: ltr; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; color: #839197; margin-top: 0; font-size: 12px; line-height: 1.5em;" align="center">
                      8190 Inc.
                      <br />
                    </p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
