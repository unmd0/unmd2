<!DOCTYPE html>
<html>

<head>
    <!-- Google Tag Manager -->
{{-- <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PB6F5RD');</script> --}}
    <!-- End Google Tag Manager -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>سایت ساز 8190</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/fonts/index/icomoon/style.css">
    <link rel="stylesheet" href="{{ asset('gadgets/macmenu/index.css?ver=1.1') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/index/bootstrap.min.css">
    <link rel="stylesheet" href="/css/index/jquery-ui.css">
    <link rel="stylesheet" href="/css/index/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/index/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/index/owl.theme.default.min.css">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="stylesheet" href="/css/index/jquery.fancybox.min.css">

    <link rel="stylesheet" href="/css/index/bootstrap-datepicker.css">

    <link rel="stylesheet" href="/css/fonts/index/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="/css/index/aos.css">

    <link rel="stylesheet" href="/css/index/style.css?ver=1.216">

  </head>

  <body style="overflow: hidden;">
    <form action="/register" method="get">
    <header class="header">
        <div class="header___data">

            <input type="text" name="phone" id="emailgetter"  type="tel" maxlength="11" autocomplete="tel" placeholder="شماره موبایل">
            <div id="mdbtnemail">
                <button id="btnemail" type="submit" class="btn btn-sucssess">ثبت نام</button>
            </div>

        </div>
    </header>
  </form>
  <header class="header2 ">
    <a class="btn btn-success btn_register" onclick="startreg()" href="#register" style="margin-top: -10px;
    width: 100%;
    height: 50px;
    justify-content: center;
    display: flex;
    /* padding-bottom: 0px; */
    /* margin-bottom: 10px; */
    background: white !important;
    padding-top: 0px;
    color: #da0047 !important;
    font-family: kalamehb;
    font-size: 1.9rem;
    border: 3px solid #171717 !important;">همین حالا شروع کن</a>
</header>
<form method="POST" action="{{ route('register') }}" id="regi">
    @csrf
<div id="regdiv" style="
    position: fixed;
    width: 300%;
    display: flex;
    height: 100%;
    background: white;
    z-index: 9999999999;
    left: 100%;
">
    <div style="
    background: rgb(69 126 212);
    width: 100%;
    height: 100%;
    text-align: center;
    color: white;
    ">
      <h4 style="
      padding-top: 15vh;
    font-family: kalamehb;
    font-size: 3.5rem;
      ">
        نام کاربری
      </h4>
      <h6 style="
      line-height: 30px;
    direction: rtl;
    color: #b1c9ff;
    padding: 0px 25%;
      ">
      توصیه میشود نام فروشگاه خودتون رو به انگلیسی وارد کنید
      </h6>
      <hr style="
    height: 1px;
    background: #00000036;
    width: 50%;
      ">

      <input type="text" name="name" placeholder="نام کاربری" id="username" style="
border: 0px;
    border-radius: 10px 0px 10px 10px;
    height: 35px;
    width: 50%;
    font-size: 18px;
    padding-top: 5px;

    text-align: center;
    font-weight: 900;
    font-family: yekan bakh;
      ">
          <a class="btn btn-success" onclick="usernamevalidate()" style="
border: 0px;
    border-radius: 10px 10px 0px 10px;
    height: 35px;
    width: 50%;
    font-size: 18px;
    border-color: #07005f !important;
    background: #053379 !important;
    color: white !important;
    margin-top: 10px;
    padding-top: 5px;
    height: 39px !important;
    /*  */
    margin-left: 0px !important;
    font-weight: 900;
    font-family: yekan bakh;
          ">
          <span>مرحله بعد</span>
        <i class="fa fa-caret-right" style="
            margin-top: 1px;
    font-size: 22px;
    position: absolute;
    margin-left: 10px;
        "></i>
        </a>




        <a class="btn btn-success" onclick="back(100)" style="
        border: 0px;
            border-radius: 10px 10px 10px 0px;
            height: 35px;
            width: 50%;
            font-size: 18px;
            background: #7fb2ff  !important;
            margin-top: 10px;
            padding-top: 5px;
            height: 39px !important;
            /*  */
            margin-left: 0px !important;
            font-weight: 900;
            font-family: yekan bakh;
            direction: rtl;
            text-align: left;
                  ">

                  <span>بازگشت</span>
                  <i class="fa fa-caret-left" style="
                  margin-top: 1px;
          font-size: 22px;
          position: absolute;
          margin-right: 10px;
              "></i>
                </a>


                <h6 style="
                line-height: 30px;
              direction: rtl;
              font-size: 11px;
              color: #b1c9ff;
              padding: 30px 20px;
                ">
                  نام کاربری به ما اجازه میده تا اسم کسب و کارتون رو بدونیم و فروشگاه هارو از هم جدا کنیم
                  <br/>
                  همچنین برای مثال اگر شما نام کاربری admin را نتخاب کنید در ابتدا آدرس وبسایت شما به صورت admin.8190.org خواهد بود
                </h6>

    </div>
    <div style="background: rgb(69 126 212);width: 100%;height: 100%;text-align: center;color: white;">
        <h4 style="
        padding-top: 15vh;
        font-family: kalamehb;
        font-size: 3.5rem;
        ">
          شماره موبایل
        </h4>
        <h6 style="
        line-height: 30px;
        direction: rtl;
        color: #b1c9ff;
        padding: 0px 25%;
        ">
                     برای فعالیت در پلتفرم 8190 ملزم به تایید شماره همراه خود هستید
        </h6>
        <hr style="
        height: 1px;
        background: #00000036;
        width: 50%;
        ">

        <input type="text" name="phone" placeholder="09123456789" id="phone" style="
        border: 0px;
          border-radius: 10px 0px 10px 10px;
          height: 35px;
          width: 50%;
          font-size: 18px;
          padding-top: 5px;

          font-weight: 900;
          text-align: center;
          font-family: yekan bakh;
        ">
            <a class="btn btn-success" onclick="phonevalidate()" style="
          border: 0px;
          border-radius: 10px 10px 0px 10px;
          height: 35px;
          width: 50%;
          font-size: 18px;
          border-color: #07005f !important;
          background: #053379 !important;
          color: white !important;
          margin-top: 10px;
          padding-top: 5px;
          height: 39px !important;
          /*  */
          margin-left: 0px !important;
          font-weight: 900;
          font-family: yekan bakh;
            ">
            <span>مرحله بعد</span>
            <i class="fa fa-caret-left" style="
            margin-top: 1px;
    font-size: 22px;
    position: absolute;
    margin-right: 10px;
        "></i>
          </a>




          <a class="btn btn-success" onclick="back(0)" style="
          border: 0px;
              border-radius: 10px 10px 10px 0px;
              height: 35px;
              width: 50%;
              font-size: 18px;
              background: #7fb2ff  !important;
              margin-top: 10px;
              padding-top: 5px;
              height: 39px !important;
              /*  */
              margin-left: 0px !important;
              font-weight: 900;
              font-family: yekan bakh;
              direction: rtl;
              text-align: left;
                    ">

                    <span>بازگشت</span>
                    <i class="fa fa-caret-left" style="
                    margin-top: 1px;
            font-size: 22px;
            position: absolute;
            margin-right: 10px;
                "></i>
                  </a>


                  <h6 style="
                  line-height: 30px;
                direction: rtl;
                font-size: 11px;
                color: #b1c9ff;
                padding: 30px 20px;
                  ">
                      تلفن همراه معتبر خود را وارد کنید
                  </h6>
    </div>
    <div style="background: rgb(69 126 212);width: 100%;height: 100%;text-align: center;color: white;">
        <h4 style="
        padding-top: 15vh;
        font-family: kalamehb;
        font-size: 3.5rem;
        ">
          رمز عبور
        </h4>
        <h6 style="
        line-height: 30px;
        direction: rtl;
        color: #b1c9ff;
        padding: 0px 25%;
        ">
            حتما باید بیش از 8 حرف باشد
        </h6>
        <hr style="
        height: 1px;
        background: #00000036;
        width: 50%;
        ">

        <input type="text" name="password" placeholder="رمز عبور" id="password" style="
        border: 0px;
          border-radius: 10px 0px 10px 10px;
          height: 35px;
          width: 50%;
          font-size: 18px;
          padding-top: 5px;

          text-align: center;
          font-weight: 900;
          font-family: yekan bakh;
        ">
            <a class="btn btn-success" onclick="passwordvalidate()" style="
          border: 0px;
          border-radius: 10px 10px 0px 10px;
          height: 35px;
          width: 50%;
          font-size: 18px;
          border-color: #07005f !important;
          background: #053379 !important;
          color: white !important;
          margin-top: 10px;
          padding-top: 5px;
          height: 39px !important;
          /*  */
          margin-left: 0px !important;
          font-weight: 900;
          font-family: yekan bakh;
            ">
            <span>تکمیل ثبت نام</span>
          {{-- <i class="fa fa-check" style="
              margin-top: 1px;
          font-size: 22px;
          position: absolute;
          margin-left: 10px;
              "></i> --}}
          </a>




          <a class="btn btn-success" onclick="back(-100)" style="
          border: 0px;
              border-radius: 10px 10px 10px 0px;
              height: 35px;
              width: 50%;
              font-size: 18px;
              background: #7fb2ff  !important;
              margin-top: 10px;
              padding-top: 5px;
              height: 39px !important;
              /*  */
              margin-left: 0px !important;
              font-weight: 900;
              font-family: yekan bakh;
              direction: rtl;
              text-align: left;
                    ">

                    <span>بازگشت</span>
                    <i class="fa fa-caret-left" style="
                    margin-top: 1px;
            font-size: 22px;
            position: absolute;
            margin-right: 10px;
                "></i>
                  </a>


                  <h6 style="
                  line-height: 30px;
                direction: rtl;
                font-size: 11px;
                color: #b1c9ff;
                padding: 30px 20px;
                  ">
                      رمز عبور خود را فراموش نکنید
                  </h6>
    </div>
</div>
</form>
  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
      <span class="sr-only">درحال بارگذاری</span>
    </div>
  </div>


  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>


    <header class="site-navbar js-sticky-header site-navbar-target" role="banner" >

      <div class="container">
        <div class="row align-items-center">

          <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo"><a class="btn btn-success" href="/login" style="margin-top: -10px;">ورود</a></h1>
          </div>

          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block rtl">
                <li><a href="#home-section" class="nav-link">خانه</a></li>
                <li><a href="#pricing-section" class="nav-link">تعرفه ها</a></li>
                <li><a href="/login" class="nav-link">ورود</a></li>
                <li><a href="/register" class="nav-link">ثبت نام</a></li>
                <li><a href="#faq-section" class="nav-link">سوالات متداول</a></li>
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>

    </header>


    <section class="site-blocks-cover overflow-hidden bg-light" id="home-section" style="background: radial-gradient(#da0047, #da0047);">
      <div class="container">
        <div class="row" style="background: #da0047;">
          <div class="col-md-12 align-self-center text-center text-md-left">

              <h1 class="build_your_website" style="text-align: center;
              text-shadow: 0px 0px 5px #00000073;
              font-family: 'kalamehb';
              font-size: 3.125rem;
              font-weight: 800;
              color: rgb(255 255 255);
              text-align: center;
              padding-bottom: 1.25rem;
              -webkit-user-select: none;
              -moz-user-select: none;
              -ms-user-select: none;
              user-select: none;
              position: relative;
              text-shadow: 2px 2px 0 #1cafa5;
              font-size: 6rem;
              text-shadow: 3px 4px 0 #000000;">فروشگاه خودتو بساز</h1>
              <p class="mb-4"  style="margin-top: -10px;padding-top: 0px;">
                  <span  id="changeText">در کمترین زمان فروشگاه اینترنتی بساز و کسب و کارت رو گسترش بده</span>
              </p>

          </div>
        </div>
      </div>
    </section>

    <section class="site-section">
      <div class="container">
        <div class="row justify-content-center" data-aos="fade-up">
          <div class="col-lg-6 text-center heading-section mb-5">
            <h1 class="text-black mb-2" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;"> چطور<h4 style="text-shadow: 0px 0px 5px #00000073;padding-top: 15px;"> میتونم فروشگاه اینترنتی داشته باشم؟</h4></h1>
            <p></p>
          </div>
        </div>

        <div class="row hover-1-wrap mb-5 mb-lg-0">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6 order-lg-2" data-aos="fade-right">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-612xjk612.jpg?ver=2" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 mr-auto text-lg-right align-self-center order-lg-1" data-aos="fade-left"><br/>
                <h3 class="text-black" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">ثبت نام رایگان</h3><br/>
                <p class="mb-4">با ثبت نام در پلتفرم ۸۱۹۰ یک اکانت ۳۶ روزه رایگان به شما متعلق میشود </p>
              </div>
            </div>
          </div>
        </div>

        <div class="row hover-1-wrap mb-5 mb-lg-0">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6 imgh"  data-aos="fade-left">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-612x612.jpg?ver=2" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 ml-auto align-self-center"  data-aos="fade-right">
                <h2 class="text-black" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">ساخت محصول</h2><br/>
                <p class="mb-4">در پنل مدیریت محصولات خود را به راحتی اضافه کنید<br/>
                فقط کافیست عکس محصولاتتان را تهییه کنید
                </p>
            </div>
            </div>
          </div>
        </div>

        <div class="row hover-1-wrap">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6 order-lg-2" data-aos="fade-right">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-6__12x612.jpg?ver=2" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 mr-auto text-lg-right align-self-center order-lg-1" data-aos="fade-left"><br/>
                <h2 class="text-black" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">تغییر ظاهر قالب</h2><br/>
                <p class="mb-4">به سادگی ظاهر وبسایت خود را به دلخواه تغییر دهیید</p>
              </div>
            </div>
          </div>
        </div>

        <div class="row hover-1-wrap mb-5 mb-lg-0">
          <div class="col-12">
            <div class="row">
              <div class="mb-4 mb-lg-0 col-lg-6 imgh"  data-aos="fade-left">
                <a href="#" class="hover-1">
                  <img src="/img/index/istockphoto-1077760200-x612.jpg?ver=2" alt="Image" class="img-fluid">
                </a>
              </div>
              <div class="col-lg-5 ml-auto align-self-center"  data-aos="fade-right"><br/>
                <h2 class="text-black" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">مدیریت کنید</h2><br/>
                <p class="mb-4">آدرس وبسایت خود را به مشتریان خود بدهید و فروش را شروع کنید</p>
              </div>
            </div>
          </div>
        </div>



      </div>
    </section>

    {{-- <section class="site-section" id="pricing-section">
      <div class="container">
        <div class="row justify-content-center" data-aos="fade-up">
          <div class="col-lg-7 text-center heading-section mb-5">
            <div class="paws">
              <span class="icon-paw"></span>
            </div>
            <h2 class="mb-2 text-black heading">تعرفه ها</h2>
            <p>بهترین قیمت ها</p>
          </div>
        </div>
        <div class="row no-gutters rtl">




          <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-primary p-3 p-md-5" data-aos="fade-up" data-aos-delay="">
            <div class="pricing">
              <h3 class="text-center text-uppercase">تست</h3>
              <div class="price text-center mb-4 text-black">
                <span class="text-black"><span class="text-black" style="color: #ff046c !important;"> رایگان </span>۶روزه</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">

                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li class="remove">Lorem ipsum dolor sit amet</li>
                <li class="remove">Consectetur adipisicing elit</li>
                <li class="remove">Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
                <a href="/register/" class="btn btn-secondary">ثبت نام</a>
              </p>
            </div>
          </div>




          <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-dark  p-3 p-md-5" style="    background-color: #d1e1f5 !important;" data-aos="fade-up" data-aos-delay="100">
            <div class="pricing">
              <h3 class="text-center text-white text-uppercase rtl text-black">۹۶ روزه</h3>
              <div class="price text-center mb-4 rtl text-black">
                <span class="rtl text-black"><span  style="color: #ff046c !important;">۹۶</span> هزار تومان</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">

                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li class="remove">Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
                <a href="/register/" class="btn btn-secondary">ثبت نام</a>
              </p>
            </div>
          </div>


          <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-primary  p-3 p-md-5" style="background-color: #c5daf5 !important;" data-aos="fade-up" data-aos-delay="200">
            <div class="pricing">
              <h3 class="text-center  text-uppercase rtl">۳۶۹ روزه</h3>
              <div class="price text-center mb-4 rtl text-black">
                <span class="rtl text-black"><span class="text-black" style="color: #ff046c !important;" >۳۹۶</span> هزار تومان</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">

                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li>Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
                <a href="/register/" class="btn btn-secondary">ثبت نام</a>
              </p>
            </div>
          </div>






        </div>
      </div>
    </section> --}}



    <section class="site-section" id="faq-section">
        <div class="container" id="accordion">
          <div class="row justify-content-center" data-aos="fade-up">
            <div class="col-lg-6 text-center heading-section mb-5">
              <div class="paws">
                <span class="icon-paw"></span>
              </div>
              <h2 class="text-black mb-2">سوالات متداول</h2>
              <!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> -->
            </div>
          </div>
          <div class="row accordion justify-content-center block__76208">
            <!-- <div class="col-lg-6 order-lg-2 mb-5 mb-lg-0" data-aos="fade-up"  data-aos-delay="">
              <img src="/img/index/dogger_img_sm_1.jpg" alt="Image" class="img-fluid rounded">
            </div> -->
            <div class="col-lg-5 order-lg-1 text-right" data-aos="fade-up"  data-aos-delay="100">

                <div class="accordion-item">
                    <h3 class="mb-0 heading">
                      <a class="btn-block" data-toggle="collapse" href="#collapseone" role="button" aria-expanded="true" aria-controls="collapseSix" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">آدرس وبسایت من به چه صورتی میشود؟<span class="icon"></span></a>
                    </h3>
                    <div id="collapseone" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="body-text">
                        <p style="direction: rtl;">آدرس وبسایت براساس نام فروشگاهتان ساخته میشود<br/>اگر نام فروشگاهتان shop باشد آدرس وبسایتتان به صورت shop.8190.org میشود.<br/>درصورتی که نیاز به آدرس اختصاصی میباشید به پشتیبانی پیام دهید.</p>
                      </div>
                    </div>
                  </div> <!-- .accordion-item -->

                <div class="accordion-item">
                <h3 class="mb-0 heading text-right">
                  <a class="btn-block" data-toggle="collapse" href="#collapseFive" role="button" aria-expanded="false" aria-controls="collapseFive" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">آیا برای راه اندازی فروشگاه اینترنتی در این پلتفرم نیاز به برنامه نویسی و تخصص است؟<span class="icon"></span></a>
                </h3>
                <div id="collapseFive" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="body-text">
                    <p>خیر. راه اندازی فروشگاه در این پلتفرم به سادگی امکان پذیر است و کافیست شما در این سایت ثبت نام کنید و محصولات خود را اضافه کنید تا فروش شما شروع شود</p>
                  </div>
                </div>
              </div> <!-- .accordion-item -->

              <div class="accordion-item">
                <h3 class="mb-0 heading">
                  <a class="btn-block" data-toggle="collapse" href="#collapseSix" role="button" aria-expanded="false" aria-controls="collapseSix" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">آیا نیاز به خرید هاست و دامین میباشد؟<span class="icon"></span></a>
                </h3>
                <div id="collapseSix" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="body-text">
                    <p>خیر. هاست و دامین را به ما بسپارید، اما در صورتی که نیاز به دامین اختصاصی خود بودین با ما ارتباط برقرار کنید تا روش انتقال را برای شما انجام دهیم</p>
                  </div>
                </div>
              </div> <!-- .accordion-item -->

              <div class="accordion-item">
                <h3 class="mb-0 heading">
                  <a class="btn-block" data-toggle="collapse" href="#collapseSeven" role="button" aria-expanded="false" aria-controls="collapseSeven" style="color: #ff317d !important;text-shadow: 0px 0px 5px #d801507a;">اگر از درگاه پرداخت این پلتفرم استفاده کنم چگونه پول برای من واریز می شود.<span class="icon"></span></a>
                </h3>
                <div id="collapseSeven" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="body-text">
                    <p>پس از کسر ۴% از مبلغ، کمتر از ۴۸ ساعت برای شما پرداخت می شود</p>
                  </div>
                </div>
              </div> <!-- .accordion-item -->

              <!-- <div class="accordion-item">
                <h3 class="mb-0 heading">
                  <a class="btn-block" data-toggle="collapse" href="#collapseEight" role="button" aria-expanded="false" aria-controls="collapseEight">Is a warm dry nose a sign of illness in dogs?<span class="icon"></span></a>
                </h3>
                <div id="collapseEight" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="body-text">
                    <p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                  </div>
                </div>
              </div>  -->

            </div>


          </div>
        </div>
      </section>



    @include('admin.buy.buy')




    <!-- <section class="site-section bg-light block-13" id="testimonials-section" data-aos="fade">
      <div class="container">

        <div class="row justify-content-center" data-aos="fade-up">
          <div class="col-lg-6 text-center heading-section mb-5">
            <div class="paws">
              <span class="icon-paw"></span>
            </div>
            <h2 class="text-black mb-2">Happy Customers</h2>
          </div>
        </div>
        <div  data-aos="fade-up" data-aos-delay="200">
          <div class="owl-carousel nonloop-block-13">
            <div>
              <div class="block-testimony-1 text-center">

                <blockquote class="mb-4">
                  <p>&ldquo;The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_4.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 text-black">Ricky Fisher</h3>
              </div>
            </div>

            <div>
              <div class="block-testimony-1 text-center">



                <blockquote class="mb-4">
                  <p>&ldquo;Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_2.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 mb-4 text-black">Ken Davis</h3>


              </div>
            </div>

            <div>
              <div class="block-testimony-1 text-center">


                <blockquote class="mb-4">
                  <p>&ldquo;A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_1.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 text-black">Mellisa Griffin</h3>


              </div>
            </div>

            <div>
              <div class="block-testimony-1 text-center">



                <blockquote class="mb-4">
                  <p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
                </blockquote>

                <figure>
                  <img src="/img/index/person_3.jpg" alt="Image" class="img-fluid rounded-circle mx-auto">
                </figure>
                <h3 class="font-size-20 mb-4 text-black">Robert Steward</h3>


              </div>
            </div>


          </div>
        </div>
      </div>
    </section> -->

<br/>
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-5">
                <h2 class="footer-heading mb-4">درباره ما</h2>
                <p class="text-right">پلتفرم ۸۱۹۰ با استفاده از بهترین و بروز ترین سیستم ها آماده ارایه خدمات طراحی سایت های فروشگاهی به صورت اتوماتیک و سئو شده می باشد</p>
              </div>
              <div class="col-md-3 ml-auto">
                <!-- <h2 class="footer-heading mb-4">Quick Links</h2>
                <ul class="list-unstyled">
                  <li><a href="#about-section" class="smoothscroll">About Us</a></li>
                  <li><a href="#trainers-section" class="smoothscroll">Trainers</a></li>
                  <li><a href="#services-section" class="smoothscroll">Services</a></li>
                  <li><a href="#testimonials-section" class="smoothscroll">Testimonials</a></li>
                  <li><a href="#contact-section" class="smoothscroll">Contact Us</a></li>
                </ul> -->
              </div>
              <div class="col-md-3">

                <!-- <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a> -->

                <!-- <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a> -->
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <h2 class="footer-heading mb-4">مارا دنبال کنید</h2>
            <a href="https://www.instagram.com/8190_org/" class="pl-3 pr-3 instafoot"><span class="icon-instagram"></span></a>
            <!-- <h2 class="footer-heading mb-4">Subscribe Newsletter</h2>
            <form action="#" method="post" class="footer-subscribe">
              <div class="input-group mb-3">
                <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary text-black" type="button" id="button-addon2">Send</button>
                </div>
              </div>
            </form> -->
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5" style="    padding-bottom: 55px;">
                {{-- TODO: zarinpal --}}
                <div id="zarinpal">
<script src="https://www.zarinpal.com/webservice/TrustCode" type="text/javascript"></script>
</div>
<br/>
              <p class="copyright" style="direction: rtl;"small>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            ۱۳۹۹ ساخته شده با <i class="icon-heart text-danger" aria-hidden="true"></i> بدست تیم <a href="#home-section" >۸۱۹۰</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></small></p>

            </div>
          </div>

        </div>
      </div>
    </footer>

  </div> <!-- .site-wrap -->

  <script src="/js/index/jquery-3.3.1.min.js"></script>
  <script src="/js/index/jquery-ui.js"></script>
  <script src="/js/index/popper.min.js"></script>
  <script src="/js/index/bootstrap.min.js"></script>
  <script src="/js/index/owl.carousel.min.js"></script>
  <script src="/js/index/jquery.countdown.min.js"></script>
  <script src="/js/index/jquery.easing.1.3.js"></script>
  <script src="/js/index/aos.js"></script>
  <script src="/js/index/jquery.fancybox.min.js"></script>
  <script src="/js/index/jquery.sticky.js"></script>
  <script src="/js/index/isotope.pkgd.min.js"></script>


  <script src="/js/index/main.js"></script>

<!-- goftino -->


<!---start GOFTINO code--->
<script type="text/javascript">
    !function(){var a=window,d=document;function g(){var g=d.createElement("script"),s="https://www.goftino.com/widget/qPW9Hv",l=localStorage.getItem("goftino");g.type="text/javascript",g.async=!0,g.src=l?s+"?o="+l:s;d.getElementsByTagName("head")[0].appendChild(g);}"complete"===d.readyState?g():a.attachEvent?a.attachEvent("onload",g):a.addEventListener("load",g,!1);}();
  </script>
  <!---end GOFTINO code--->

 <!--- Add this for Widget customization --->
 <script>
    window.addEventListener('goftino_ready', function () {
        Goftino.setWidget({
            marginRight: 30,
        marginBottom: 5,
        cssUrl: "{{ asset('css/index/goftino.css?ver=1.02') }}",
        });

        document.getElementById("new_widget_button").addEventListener("click", function () {
            Goftino.toggle();
        });
    });


    $( document ).ready(function() {
        setTimeout(function(){
            Goftino.sendMessage({
   text: 'به پلتفرم 8190 خوش آمدید، اگر سوالی دارید بپرسید'
});
         }, 16000);
});


</script>
<script>
  function startreg(){
    $("#regdiv").animate({left: '0%'});
  }
  function back(x){
    $("#regdiv").animate({left: x+'%'});
  }
  function usernamevalidate(){
        var VAL = document.getElementById("username").value;
        if (VAL.length < 3){
            alert("تعداد حروف باید بیشتر از 2 حرف باشد");
            document.getElementById("username").focus();
        }
        var email = new RegExp('(^([a-zA-Z0-9]+)(\d+)?$)');

        if (email.test(VAL)) {
            $("#regdiv").animate({left: '-100%'});
        }else{
            alert("مقدار نام کاربری فقط میتواند شامل حروف و اعداد انگلیسی باشد");
            document.getElementById("username").focus();
        }
}
function passwordvalidate(){
        var VAL = document.getElementById("password").value;
        if (VAL.length < 8){
            alert("تعداد حروف باید 8 حرف یا بیشتر باشد");
            document.getElementById("password").focus();
        }else{
            document.getElementById("regi").submit();
        }
}
function phonevalidate(){
        var VAL = document.getElementById("phone").value;
        if (VAL.length != 11 || !VAL.startsWith("09")){
            alert("شماره موبایل نامعتبر است");
            document.getElementById("phone").focus();
        }else{
            $("#regdiv").animate({left: '-200%'});
        }
}
</script>
{{-- <script type="text/javascript" defer>
  // start hantana code
  (function(d,w,u,t,s){
     s = w.createElement("script");
     d._hantanaSettings={tId:t};
     s.type = "text/javascript", s.async = !0, s.src = u + t;
     h = w.getElementsByTagName('head')[0];
     h.appendChild(s);
  })(window,document,'https://hantana.org/widget/','5f64e-5adc9-296a5-7fa3c');
</script> --}}
  </body>
</html>
