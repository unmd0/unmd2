<!DOCTYPE html>
<html lang="en">
<head>
    <div id="loadOverlay" style="    background: #9053c7;
    background: -webkit-linear-gradient(-135deg, #c850c0, #4158d0);
    background: -o-linear-gradient(-135deg, #c850c0, #4158d0);
    background: -moz-linear-gradient(-135deg, #c850c0, #4158d0);
    background: linear-gradient(-135deg, #c850c0, #4158d0); position:absolute; top:0px; left:0px; width:100%; height:100%; z-index:2000;"></div>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ورود</title>

<!--===============================================================================================-->
<link rel="stylesheet" href="/css/organi/bootstrap.min.css" type="text/css">
<link rel="stylesheet" href="/css/organi/font-awesome.min.css" type="text/css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/login/util.css">
	<link rel="stylesheet" type="text/css" href="/css/login/main.css?ver=2.0.0">
<!--===============================================================================================-->
<style>
.floatlabel {
  position:relative;
}
.floatlabel input {
  -webkit-appearance: none;
  color:#333;
  padding:10px 12px;
  font-family: Poppins-Medium;
    font-size: 15px;
    line-height: 1.5;
    color: #666666;
    display: block;
    width: 100%;
    background: #e6e6e6;
    height: 50px;
    border-radius: 25px;
    padding: 0px 24px 0 20px;
    text-align: center;
}
.floatlabel label.label {
  position:absolute;
  top:12px;
  width: 250px;
    text-align: center;
    left: calc(50% - 125px);
  transition:all 0.2s ease-out;
  background-color: #ffffff00;
  color:#999;
  font-size:16px;
  cursor:text;
  font-weight: bolder;
}
.floatlabel label.label.floatlabel-shift {
  top:-13px;
  width: 150px;
    text-align: center;
    left: calc(50% - 75px);
  padding:0 4px;
  font-size:14px;
  color:rgb(97, 97, 97);
  background-color: #ffffff00;
  text-shadow: rgb(230, 230, 230) 1px 0px 0px, rgb(230, 230, 230) 0.540302px 0.841471px 0px, rgb(230, 230, 230) -0.416147px 0.909297px 0px, rgb(230, 230, 230) -0.989992px 0.14112px 0px, rgb(230, 230, 230) -0.653644px -0.756802px 0px, rgb(230, 230, 230) 0.283662px -0.958924px 0px, rgb(230, 230, 230) 0.96017px -0.279415px 0px;
}
.floatlabel label.label.floatlabel-active {
  color:#0077c8;
  text-shadow: rgb(230, 230, 230) 1px 0px 0px, rgb(230, 230, 230) 0.540302px 0.841471px 0px, rgb(230, 230, 230) -0.416147px 0.909297px 0px, rgb(230, 230, 230) -0.989992px 0.14112px 0px, rgb(230, 230, 230) -0.653644px -0.756802px 0px, rgb(230, 230, 230) 0.283662px -0.958924px 0px, rgb(230, 230, 230) 0.96017px -0.279415px 0px;
  background-color: #ffffff00;
}


      </style>
</head>
<body>
    @yield('main-admin')
</body>
<script src="/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/bootstrap/js/popper.js"></script>
	<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
    <script src="/js/login/main.js"></script>
    <script>
        jQuery(".floatlabel input").focus(function() {
  var elementId = $(this).attr('id');
   jQuery("label[for=" + elementId + "]").addClass('floatlabel-shift');

});
jQuery(".floatlabel input").blur(function() {
   if($(this).val().length == 0) {
    var elementId = $(this).attr('id');
     jQuery("label[for=" + elementId + "]").removeClass('floatlabel-shift');
   }
});
jQuery(".floatlabel input").focus(function() {
  var elementId = $(this).attr('id');
   jQuery("label[for=" + elementId + "]").addClass('floatlabel-active');
});
jQuery(".floatlabel input").blur(function() {
  var elementId = $(this).attr('id');
   jQuery("label[for=" + elementId + "]").removeClass('floatlabel-active');
});
$( document ).ready(function() {
    // $("label[for=password]").focus();

    // setTimeout(function(){ alert($("label[for=" + "password" + "]").val()); }, 2000);
    // // jQuery("label[for=" + elementId + "]").addClass('floatlabel-shift');
    // // $("label[for=password]").focus();

    // $("label[for=phone]").trigger( "click" );
    // if($("label[for=" + "password" + "]").val().length == 0) {
    //  $("label[for=" + "password" + "]").removeClass('floatlabel-shift');
    //  }
     $("label[for=phone]").focus();
    // //  $("label[for=phone]").focus();
    // if($("label[for=" + "name" + "]").length > 0){
    //  if($("label[for=" + "name" + "]").val().length == 0) {
    //  $("label[for=" + "name" + "]").removeClass('floatlabel-shift');
    //  }
    // //  $("label[for=phone]").focus();
    //  if($("label[for=" + "password-confirm" + "]").val().length == 0) {
    //  $("label[for=" + "password-confirm" + "]").removeClass('floatlabel-shift');
    //  }
    // }
    // $("label[for=password]").focus();
    // $("label[for=phone]").focus();
});
    </script>
</html>
