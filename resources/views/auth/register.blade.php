@extends('auth.master.index')

@section('main-admin')


<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="/img/login/img-01.png" alt="IMG">
            </div>
            <form method="POST" action="{{ route('register') }}" class="login100-form validate-form">
                @csrf
                <span class="login100-form-title">
                    ثبت فروشگاه
                </span>
                @error('email')
                <span class="alert"  style="margin-bottom: 25px;">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            @error('name')
            <span class="alert"  style="margin-bottom: 25px;">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
            @error('password')
            <span class="alert"  style="margin-bottom: 25px;">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <div class="wrap-input100 validate-input form-group floatlabel" data-validate = "لطفایک نام انتخاب کنید">

                <label class="label floatlabel-shift" for="name">نام فروشگاه به انگلیسی</label>
                <input class="input100" type="text" name="name" id="name" value="{{ old('organization') }}" autocomplete="organization">

        </div>
                {{-- <div class="wrap-input100 validate-input" data-validate = "لطفا ایمیل را به درستی وارد نمایید: ex@abc.xyz">
                    <input class="input100 @error('email') is-invalid @enderror" type="text" name="email" placeholder="ایمیل"  autocomplete="email"  value=" {{ app('request')->input('email') ? app('request')->input('email') : old('email') }}">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                        <i class="fa fa-time" aria-hidden="true"></i>
                    </span>

                </div> --}}
                <div class="wrap-input100 validate-input form-group floatlabel" data-validate = "لطفا شماره موبایل خود را صحیح وارد کنید: ">

                        <label class="label floatlabel-shift" for="tel">شماره موبایل</label>
                        <input class="input100 @error('phone') is-invalid @enderror" type="tel" id="tel" maxlength="11" name="phone"  autocomplete="tel"  value="{{ app('request')->input('phone') ? app('request')->input('phone') : old('phone') }}">
                </div>

                <div class="wrap-input100 validate-input form-group floatlabel" data-validate = "لطفا یک رمز انتخاب کنید">

                        <label class="label floatlabel-shift" for="password">رمز عبور</label>
                        <input class="input100  " type="password" id="password" name="password" autocomplete="off">
                </div>

                <div class="wrap-input100 form-group floatlabel validate-input" data-validate = "لطفا یک رمز انتخاب کنید">

                        <label class="label floatlabel-shift" for="password-confirm">تکرار رمز عبور</label>
                        <input class="input100  " id="password-confirm" type="password" name="password_confirmation" autocomplete="off">

                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        ثبت نام
                    </button>
                </div>

                <div class="text-center p-t-12">
                    <span class="txt1">

                        قبلا ثبت نام کرده اید؟
                    </span>
                    <a class="txt2" href="{{ route('login') }}">
                        ورود
                    </a>

                </div>

            </form>
        </div>
    </div>
</div>
@endsection
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

