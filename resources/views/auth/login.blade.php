@extends('auth.master.index')


@section('main-admin')
<form class="container">

    </form>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="/img/login/img-01.png" alt="IMG">
            </div>
            <form method="POST" action="{{ route('login') }}" class="login100-form validate-form">
                @csrf
                <span class="login100-form-title">
                    ورود مدیران
                </span>
                @error('email')
                <span class="alert" style="margin-bottom: 25px;">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            @error('password')
            <span class="alert" style="margin-bottom: 25px;">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

                <div class="wrap-input100 validate-input" data-validate = "لطفا شماره موبایل را به درستی وارد نمایید: 09120000000">
                <div class="form-group floatlabel">
                    <label class="label" for="phone">شماره موبایل یا ایمیل</label>
                    <input type="text" name="email" id="phone"  class="field text medium  @error('phone') is-invalid @enderror" autocomplete="tel" value="{{ old('tel') }}"/>
                </div>

                </div>
                <div class="wrap-input100 validate-input" data-validate = "لطفا رمز را وارد کنید">
                    <div class="form-group floatlabel">
                        <label class="label floatlabel-shift" for="password">رمز عبور</label>
                        <input type="password" name="password" id="password"  class="field text medium @error('email') is-invalid @enderror" autocomplete="current-password"/>
                    </div>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        ورود
                    </button>
                </div>

                <div class="text-center p-t-12">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            مرا به خاطر داشته باش
                        </label>
                    </div><br/>
                    <span class="txt1">
                        فراموشی
                    </span>
                    @if (Route::has('password.request'))
                    <a class="txt2" href="{{ route('password.request') }}">
                        رمز عبور
                    </a>
                @endif
                <br/>
                <br/>
                <a class="login100-form-btn" style="background: #ff006a63;border: 1px #ff0082 solid;" href="{{route('register')}}">
                    ثبت نام در سامانه
                </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection


