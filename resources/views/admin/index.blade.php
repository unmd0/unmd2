@extends('admin.master.index')
@section('add_link')
<script src="/js/popper.js"></script>
{{-- <script src="/js//index/bootstrap.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
@endsection
@section('admin-main')
{{-- <main class="c-main"> --}}
    <div class="card-body">



            {{-- @include('admin.salechecklist')

            @include('admin.storechecklist') --}}
            <div class="c-grid__row" style="margin-top: 0px !important">
                <div class="checklistp gradiant2" style="width: fit-content" >
                    <small class="checklistp-alert">
                        آدرس فروشگاه شما:
                    </small>
                    <span class="checklistp-alert" style="font-family: monospace;margin-right: 10px;">
                        https://{{ Auth::user()->name }}.{!! str_replace("www.","",Request::getHost()) !!}
                    </span>
                    <input value="https://{{ Auth::user()->name }}.{{ Request::getHost() }}" class="itslabael" id="target">
                    <div class="iscopy apashe" style="cursor: default;" data-clipboard-target="#target">
                        کپی کردن
                    </div>
                </div>
            </div>
        <div class="c-grid__row" style="margin-top: 0px !important">
            <div class="c-grid__col c-grid__col--lg-4" style="margin-top: 25px;">

                <div class="c-card">
                    <div class="c-card__header">
                        <h2 class="c-card__title">سفارشات</h2>
                    </div>
                    <div class="c-card__body">


                        <div class="c-card__stat c-card__stat--section">
                            <a class="#" href="{{ url('admin/orders') }}">
                                <div class="c-card__stat-value c-card__stat-value--active">
                                    <span dir="ltr">{{DB::table('orders')->where("shopname","=",Auth::user()->name)->where('created_at', '>=', \Carbon\Carbon::today())->count()}}</span>
                                </div>
                                <p class="c-card__stat-description">سفارشات امروز</p>
                            </a>
                        </div>


                        <a class="c-card__stat js-change-selling-chart" href="{{ url('admin/orders') }}">
                            <div class="c-card__stat-value" data-value="137897000">
                                <span dir="ltr">{{DB::table('orders')->where("shopname","=",Auth::user()->name)->where("shopstatus","=","0")->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">سفارشات مشاهد نشده</p>
                        </a>

                        <a class="c-card__stat js-change-selling-chart" href="{{ url('admin/orders') }}">
                            <div class="c-card__stat-value">
                                <span dir="ltr">{{DB::table('orders')->where("shopname","=",Auth::user()->name)->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">سفارشات تا به امروز</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="c-grid__col c-grid__col--lg-4" style="margin-top: 25px;">
                <div class="c-card">
                    <div class="c-card__header">
                        <h2 class="c-card__title">محصولات</h2>
                    </div>
                    <div class="c-card__body">


                        <div class="c-card__stat c-card__stat--section">
                            <a class="#" href="{{ url('admin/product/show') }}">
                                <div class="c-card__stat-value c-card__stat-value--active">
                                    <span dir="ltr">{{DB::table('products')->where("show","=","0")->where("shopname","=",Auth::user()->name)->count()}}</span>
                                </div>
                                <p class="c-card__stat-description">محصولات</p>
                            </a>
                        </div>


                        <a class="c-card__stat js-change-selling-chart" href="{{ url('admin/type/show') }}">
                            <div class="c-card__stat-value" data-value="137897000">
                                <span dir="ltr">{{DB::table('big_s_products_type')->where("show","=","0")->where("shopname","=",Auth::user()->name)->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">تنوع ها</p>
                        </a>

                        <a class="c-card__stat js-change-selling-chart"  href="{{ url('admin/type/show') }}">
                            <div class="c-card__stat-value">
                                <span dir="ltr">{{DB::table('big_s_products_type')->where("show","=","0")->where("shopname","=",Auth::user()->name)->where("Avaliable","=","1")->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">تنوع های فعال</p>
                        </a>

                        <a class="c-card__stat js-change-selling-chart"  href="{{ url('admin/type/show') }}">
                            <div class="c-card__stat-value">
                                <span dir="ltr">{{DB::table('big_s_products_type')->where("show","=","0")->where("shopname","=",Auth::user()->name)->where("Avaliable","=","0")->count()}}</span>
                            </div>
                            <p class="c-card__stat-description">تنوع های غیر فعال</p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="c-grid__col c-grid__col--lg-4" style="margin-top: 25px;">
                <div class="c-card">
                    <div class="c-card__header">
                        <h2 class="c-card__title">آمار بازدید</h2>
                    </div>
                    <div class="c-card__body" >


                        <div class="c-card__stat-value" data-value="137897000">
                            <span dir="ltr">{{DB::table('sitelogs')->where("connect","=",Auth::user()->name)->where('details', 'like', '%home%')->count()}}</span>
                        </div>
                        <p class="c-card__stat-description">بازدیدهای صفحه اصلی تا به امروز</p><br />


                        <div class="c-card__stat-value" data-value="137897000">
                            <span dir="ltr">{{DB::table('sitelogs')->where("connect","=",Auth::user()->name)->where('details', 'like', '%home%')->where('created_at', '>=', \Carbon\Carbon::today())->count()}}</span>
                        </div>
                        <p class="c-card__stat-description">بازدیدهای امروز از صفحه اصلی</p><br />


                        <div class="c-card__stat-value" data-value="137897000">
                            <span dir="ltr">{{DB::table('sitelogs')->where("connect","=",Auth::user()->name)->where('details', 'like', '%details%')->count()}}</span>
                        </div>
                        <p class="c-card__stat-description">بازدیدهای محصولات تا به امروز</p><br />


                        <div class="c-card__stat-value">
                            <span dir="ltr">{{DB::table('sitelogs')->where("connect","=",Auth::user()->name)->where('details', 'like', '%details%')->where('created_at', '>=', \Carbon\Carbon::today())->count()}}</span>
                        </div>
                        <p class="c-card__stat-description">بازدید های امروز از محصولات</p><br />


                    </div>
                </div>
            </div>


        </div>



    </div>
{{-- </main> --}}
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  var clipboard = new Clipboard('.iscopy');
  clipboard.on('success', function(e) {
alert("آدرس فروشگاه شما کپی شد.");

	e.clearSelection();
});
});

</script>
@endsection
