
<script src="/js/addtype.js"></script>
<link rel="stylesheet" href="/css/dropzone.css">
<script src="/js/dropzone.min.js"></script>
<main class="c-main">
<div class="uk-container uk-container-large">
    <div id="popup1">

        <div class="uk-container uk-container-large">
        <div class="uk-container uk-container-large" style="width: auto;display: inline-block;">
        <div class="popup">
            
            @include('admin.files.listfiles')
        </div>
        </div>
    </div>
    <div><button type="button" onclick="hidepopup()" class="btn btn-danger closepopup">بستن</button></div>
</div>

    <div id="typs">
        <input type="text" class="hide" id="idhere" value={{$ID}}>
    @if ($typeAll)
        @foreach ($typeAll as $item)
        <div class="c-cart1">




            <div class="form-group">
                <label for="title">نام تنوع</label>
            <input type="text" name="name" disabled class="form-control" value="{{$item->Name}}">
            </div>
            <div class="form-group">
                <label for="body">قیمت</label><br/>
                <input type="text" name="price" disabled class="form-control" value="{{$item->Price}}" style="display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان 
                {{-- <input type="text" name="price" disabled class="form-control" value="{{$item->Price}}"> --}}
            </div>
            <div class="form-group">
                <input type="checkbox" disabled @if ($item->Avaliable==1) checked @endif style="margin-right: 5px !important;position:relative;">
                <label for="customControlInline">موجود</label>
            </div>
            <div class="form-group"> 
                @if ($tp==1)
                <hr>
                @php 
                $ofile=DB::table('filemanages')->where('id','=',$item->ofile)->first();
                $pfile=DB::table('filemanages')->where('id','=',$item->pfile)->first();
                $filename=$ofile->ect;
                if(!$item->pfile==0){
                    $filepreview=$pfile->ect; 
                    $prvid=$pfile->id;
                }else{
                    $filepreview="خالی"; 
                    $prvid=0;
                }
                
                @endphp
                <label style="display: inline-flex;flex-flow: wrap;white-space: pre-wrap;">فایل اصلی <span style="font-size: smaller;color: #949191;"> (خریدار برای دریافت این فایل هزینه پرداخت می کند)</span></label><br/>
                <div style="display: flex;align-items:baseline;">
                <button type="button" class="btn btn-info" disabled onclick="showpopup({{$item->TypeId}},'orgfile','filename')">انتخاب فایل اصلی</button>
                <input type="text" id="orgfile{{$item->TypeId}}" name="mainfile" value="{{$ofile->id}}" class="hide">
                <div class="alert alert-info" style="direction: ltr;width: fit-content;margin: 10px 0px;" id="filename{{$item->TypeId}}">{{$filename}}</div>
                </div>
                <br/>
                <label style="display: inline-flex;flex-flow: wrap;white-space: pre-wrap;">فایل پیش نمایش <span style="font-size: smaller;color: #949191;"> (این فایل به صورت رایگان و برای پیش نمایش فایل اصلی در اختیار کاربر قرار می گیرد)</span></label>
                <br/>
                <div style="display: flex;align-items:baseline;">
                <button type="button" class="btn btn-info" disabled onclick="showpopup({{$item->TypeId}},'prvfile','previewname')">انتخاب فایل پیش نمایش</button>
                <input type="text" id="prvfile{{$item->TypeId}}" name="prvfile" value="{{$prvid}}" class="hide">
                <div class="alert alert-info" style="direction: ltr;width: fit-content;margin: 10px 0px;" id="previewname{{$item->TypeId}}">{{$filepreview}}</div>
                
                </div>
                
                @endif
                <hr>
            </div>
            <div class="form-group"> 
                <button class="btn btn-success" onclick="editmebtn(this)">ویرایش</button>
                <input type="text" class="hide" value="{{$item->TypeId}}">
            </div>

        

    </div>
        @endforeach
    @endif
    <div class="c-cart1">




            <div class="form-group">
                <label for="title">نام تنوع</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="body">قیمت</label><br/>
                <input type="text" name="price" class="form-control" style="display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان 
            </div>
            <div class="form-group">
                <input type="checkbox" checked style="margin-right: 5px !important;position:relative;">
                <label for="customControlInline">موجود</label>
            </div>
            <div class="form-group"> 
                @if ($tp==1)
                <hr>
                @php $filename="خالی"; $filepreview="خالی"; @endphp
                <label style="display: inline-flex;flex-flow: wrap;white-space: pre-wrap;">فایل اصلی <span style="font-size: smaller;color: #949191;"> (خریدار برای دریافت این فایل هزینه پرداخت می کند)</span></label><br/>
                <div style="display: flex;align-items:baseline;">
                <button type="button" class="btn btn-info" onclick="showpopup(0,'orgfile','filename')">انتخاب فایل اصلی</button>
                <input type="text" id="orgfile0" name="mainfile" class="hide">
                <div class="alert alert-info" style="direction: ltr;width: fit-content;margin: 10px 0px;" id="filename0">{{$filename}}</div>
                </div>
                <br/>
                <label style="display: inline-flex;flex-flow: wrap;white-space: pre-wrap;">فایل پیش نمایش <span style="font-size: smaller;color: #949191;"> (این فایل به صورت رایگان و برای پیش نمایش فایل اصلی در اختیار کاربر قرار می گیرد)</span></label>
                <br/>
                <div style="display: flex;align-items:baseline;">
                <button type="button" class="btn btn-info" onclick="showpopup(0,'prvfile','previewname')">انتخاب فایل پیش نمایش</button>
                <input type="text" id="prvfile0" name="prvfile" class="hide" value="0">
                <div class="alert alert-info" style="direction: ltr;width: fit-content;margin: 10px 0px;" id="previewname0">{{$filepreview}}</div>
                
                </div>
                
                @endif
                <hr>
            </div>
            <div class="form-group"> 
                <button class="btn btn-success" onclick="saveme(this)">ذخیره</button>
                <input type="text" class="hide" id="typei">
            </div> 
           
 



        

    </div>
    </div>
    <button type="button" class="btn btn-info" onclick="subm2()"> اضافه کردن تنوع جدید </button>
    <a href="/admin/type/show"><button type="button" class="btn btn-info">پایان</button></a>
    <button type="submit" class="hide"></button>
</div>
</main>

<script>
   



    var fileorgad=0;
    var filepread=0;
    var fileorgad0="";
    var fileorgad1="";
    function saveme(x) {
       var mainfile=x.parentElement.parentElement.children[3].children[3].children[1].value;
       if(mainfile=="خالی" || mainfile==""){
           x.parentElement.parentElement.children[3].children[0].classList.add("hralert");
           x.parentElement.parentElement.children[3].children[3].children[2].classList.add("hralert");
           return false}
       var name = x.parentElement.parentElement.children[0].children[1].value;
       var price =  x.parentElement.parentElement.children[1].children[2].value;
       var prvfile=x.parentElement.parentElement.children[3].children[7].children[1].value;
       var avaliable = 1;
    //    var mainfilee = 
       if (!x.parentElement.parentElement.children[2].children[0].checked){avaliable = 0;}
       
       var pid = document.getElementById("idhere").value;
       var myJsonData = { "_token": "{{ csrf_token() }}", namee: name, pricee: price, ava:avaliable,pide: pid,mainfilee: mainfile,prvfilee: prvfile };
       $.post('/admin/filetype/store', myJsonData, function(response) {
        document.getElementById('typei').value = response;
           var obj1 = x.parentElement.parentElement;
      $(obj1).children(1).children(1).prop('disabled', true);
       $(x).prop('disabled', false);
       var bt1 =  x.parentElement.parentElement.children[3].children[3].children[0];
            var bt2 =  x.parentElement.parentElement.children[3].children[7].children[0];
       $(bt1).prop('disabled', true);
       $(bt2).prop('disabled', true);
       x.innerText ="ویرایش";
       x.setAttribute('onclick','editmebtn(this)');
       
   });
   }
   function editme(x){
    var mainfile=x.parentElement.parentElement.children[3].children[3].children[1].value;
    var prvfile=x.parentElement.parentElement.children[3].children[7].children[1].value;
        var name = x.parentElement.parentElement.children[0].children[1].value;
        var price = x.parentElement.parentElement.children[1].children[2].value;
        var avaliable = 1;
        if (!x.parentElement.parentElement.children[2].children[0].checked){avaliable = 0;}
        var prdid = x.nextElementSibling.value;
        var myJsonData = { "_token": "{{ csrf_token() }}", namee: name, pricee: price, ava:avaliable,pide: prdid,mainfilee: mainfile,prvfilee: prvfile }
        console.log(myJsonData);
        
        $.post('/admin/filetype/edit', myJsonData, function(response) {
            var obj1 = x.parentElement.parentElement;
            var bt1 =  x.parentElement.parentElement.children[3].children[3].children[0];
            var bt2 =  x.parentElement.parentElement.children[3].children[7].children[0];
       $(bt1).prop('disabled', true);
       $(bt2).prop('disabled', true);

       $(obj1).children(1).children(1).prop('disabled', true);
        $(x).prop('disabled', false);
        x.innerText ="ویرایش";
        x.setAttribute('onclick','editmebtn(this)');
        
    });
}
</script>
<script>
    function hidepopup(){
  $('#popup1').hide();
}
function showpopup(x,y,z){
  $('#popup1').show();
 fileorgad=x;
    filepread=x;
    fileorgad0=y;
    fileorgad1=z;
}

function selectme8(x,y,z,a){
     console.log(fileorgad0.concat(fileorgad));
  document.getElementById(fileorgad0.concat(fileorgad)).value=y;
//   alert("فایل >>"+z+"<< به عنوان فایل اصلی انتخاب شد")
  document.getElementById(fileorgad1.concat(fileorgad)).innerHTML=z;
}
var ix=1;
    function subm2() {
        $("#typs").append('<div class="c-cart1"><div class="form-group"><label for="title">نام تنوع</label> <input type="text" name="name" class="form-control"> </div> <div class="form-group"><label for="body">قیمت</label><br/><input type="text" name="price" class="form-control"> </div><div class="form-group"><input type="checkbox" checked class="custom-control-input" id="customControlInline" style="margin-right: 10px !important;position:relative;"><label class="custom-control-label" for="customControlInline">موجود</label></div>            <div class="form-group"> <hr><label style="display: inline-flex;flex-flow: wrap;white-space: pre-wrap;">فایل اصلی <span style="font-size: smaller;color: #949191;"> (خریدار برای دریافت این فایل هزینه پرداخت می کند)</span></label><br/><div style="display: flex;align-items:baseline;"><button type="button" class="btn btn-info" onclick="showpopup('+ ix*-1 +',\'orgfile\',\'filename\')">انتخاب فایل اصلی</button><input type="text" id="orgfile'+ ix*-1 +'" name="mainfile" class="hide"><div class="alert alert-info" style="direction: ltr;width: fit-content;margin: 10px 0px;" id="filename'+ ix*-1 +'">خالی</div></div><br/><label style="display: inline-flex;flex-flow: wrap;white-space: pre-wrap;">فایل پیش نمایش <span style="font-size: smaller;color: #949191;"> (این فایل به صورت رایگان و برای پیش نمایش فایل اصلی در اختیار کاربر قرار می گیرد)</span></label><br/><div style="display: flex;align-items:baseline;"><button type="button" class="btn btn-info" onclick="showpopup('+ ix*-1 +',\'prvfile\',\'previewname\')">انتخاب فایل پیش نمایش</button><input type="text" id="prvfile'+ ix*-1 +'" name="prvfile" class="hide" value="0"><div class="alert alert-info" style="direction: ltr;width: fit-content;margin: 10px 0px;" id="previewname'+ ix*-1 +'">خالی</div></div><hr></div><div class="form-group"> <button class="btn btn-success" onclick="saveme(this)">ذخیره</button><input type="text" class="hide"><button onclick="deleteme3(this)" class="btn btn-red">حذف</button></div></div>');
        ix=ix+1;
    }
</script>

