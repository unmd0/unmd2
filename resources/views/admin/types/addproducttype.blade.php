<div class="uk-container uk-container-large">
    <div class="c-grid__row">
        <div class="checklistp gradiant2 checklistp-middle">
    
    
            <p class="checklistp-alert">
                <p style="font-size: large;color: #f0ff04;margin-left: 10px;">راهنما: </p>
                شما میتوانید برای هر سایز، مدل، رنگ، کیفیت و یا هر خاصیت متغییر محصولتان یک تنوع بسازید
            </p>
        
        </div>
        </div>
    <div id="typs">
        <input type="text" class="hide" id="idhere" value={{$ID}}>
    @if ($typeAll)
        @foreach ($typeAll as $item)
        <div class="c-cart1">




            <div class="form-group">
                <label for="title">نام تنوع</label>
            <input type="text" name="name" disabled class="form-control" value="{{$item->Name}}">
            </div>
            <div class="form-group">
                <label for="body">قیمت</label><br/>
                <input type="text" name="price" disabled class="form-control" value="{{$item->Price}}" style="background: white;display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان 
                {{-- <input type="text" name="price" disabled class="form-control" value="{{$item->Price}}"> --}}
            </div>
            <div class="form-group">
                <label for="body">قیمت با تخفیف</label><br/>
                <input type="text" name="offer" disabled class="form-control" value="{{$item->offer}}" style="background: white;display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان 
                {{-- <input type="text" name="price" disabled class="form-control" value="{{$item->Price}}"> --}}
            </div>
            <div class="form-group">
                <input type="checkbox" disabled @if ($item->Avaliable==1) checked @endif style="margin-right: 5px !important;position:relative;">
                <label for="customControlInline">موجود</label>
            </div>
            <div class="form-group"> 
                <button class="btn btn-success" onclick="editmebtn(this)">ویرایش</button>
                <input type="text" class="hide" value="{{$item->TypeId}}">
            </div>

        

    </div>
        @endforeach
    @endif
    <div class="c-cart1">




            <div class="form-group">
                <label for="title">نام تنوع</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="body">قیمت</label><br/>
                <input type="text" name="price" class="form-control" style="background: white;display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان 
            </div>
            <div class="form-group">
                <label for="body">قیمت با تخفیف</label><br/>
                <input type="text" name="offer" class="form-control" style="background: white;display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان 
            </div>
            <div class="form-group">
                <input type="checkbox" checked style="margin-right: 5px !important;position:relative;">
                <label for="customControlInline">موجود</label>
            </div>
            <div class="form-group"> 
               
                <button class="btn btn-success" onclick="saveme(this)">ذخیره</button><input type="text" class="hide">
            </div>
 



        

    </div>
    </div>
    <button type="button" class="btn btn-info" onclick="subm2()"> اضافه کردن تنوع جدید </button>
    <a href="/admin/type/show"><button type="button" class="btn btn-info">پایان</button></a>
    <button type="submit" class="hide"></button>
</div>
</main>

<script>
    function subm2(){
    $("#typs").append('<div class="c-cart1"><div class="form-group"><label for="title">نام تنوع</label> <input type="text" name="name" class="form-control"> </div><div class="form-group"><label for="body">قیمت</label><br/><input type="text" name="price" class="form-control" style="background: white;display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان </div><div class="form-group"><label for="body">قیمت با تخفیف</label><br/><input type="text" name="offer" class="form-control" style="background: white;display: inline !important;max-width: 200px;border-radius: 30px;color: black;"> تومان </div><div class="form-group"><input type="checkbox" checked class="custom-control-input" id="customControlInline" style="margin-right: 10px !important;position:relative;"><label class="custom-control-label" for="customControlInline">موجود</label></div><div class="form-group"> <button class="btn btn-success" onclick="saveme(this)">ذخیره</button><input type="text" class="hide"><button onclick="deleteme3(this)" class="btn btn-red">حذف</button></div></div>');
}
    function saveme(x) {
       var name = x.parentElement.parentElement.children[0].children[1].value;
       var price =   x.parentElement.parentElement.children[1].children[2].value;
       var offer =   x.parentElement.parentElement.children[2].children[2].value;
       var avaliable = 1;
       var mainfilee = 0;
       if (!x.parentElement.parentElement.children[3].children[0].checked){avaliable = 0;}
       var pid = document.getElementById("idhere").value;
       var myJsonData = { "_token": "{{ csrf_token() }}", namee: name, pricee: price, ava:avaliable,pide: pid,offere: offer ,mainfile: mainfilee }
       $.post('/admin/type/store', myJsonData, function(response) {
           var obj1 = x.parentElement.parentElement;
      $(obj1).children(1).children(1).prop('disabled', true);
       $(x).prop('disabled', false);
       x.innerText ="ویرایش";
       x.setAttribute('onclick','editmebtn(this)');
   });
   }
   function editme(x){
        var name = x.parentElement.parentElement.children[0].children[1].value;
        var price = x.parentElement.parentElement.children[1].children[2].value;
        var offer =   x.parentElement.parentElement.children[2].children[2].value;
        var avaliable = 1;
        if (!x.parentElement.parentElement.children[3].children[0].checked){avaliable = 0;}
        var prdid = x.nextElementSibling.value;
        var myJsonData = { "_token": "{{ csrf_token() }}", namee: name, pricee: price, ava:avaliable,offere: offer ,pide: prdid }
        $.post('/admin/type/edit', myJsonData, function(response) {
            var obj1 = x.parentElement.parentElement;
       $(obj1).children(1).children(1).prop('disabled', true);
        $(x).prop('disabled', false);
        x.innerText ="ویرایش";
        x.setAttribute('onclick','editmebtn(this)');
    });
}

</script>
<script>
</script>
    