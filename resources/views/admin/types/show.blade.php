@extends('admin.master.index')
@section('meta_add')
<meta name="viewport" content="width=device-width, initial-scale=0.55">
@endsection
@section('admin-main')
<div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">مدیریت تنوع محصولات</h3>
        <br/>
    <div class="div-padd">
        <form action="/admin/type/search" method="GET">
        <input type="text" name="search1" value="{{$q ?? ''}}" class="search-inp">
            <button type="submit" class="btn btn-success">جستجو</button>
            <a href="/admin/type/show">
            <button type="button" class="btn btn-danger">
                <span>&times;</span>
              </button>
            </a>
    </form>
</div>
</div>

<div class="table-responsive">
    <table class="table align-items-center table-flush">
      <thead class="thead-light">
            <tr>
                <th> </th>
                <th>کد تنوع</th>
                <th>نام</th>
                <th>قیمت</th>
                <th>موجود</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
            @foreach($types as $type)
                <tr>
                    <td><img src="{{ URL::to('/') }}/img/<?php echo(explode("*",$type->pic)[1]); ?>.jpg" alt="" class="thum"></td>
                    <td>{{ $type->TypeId }}</td>
                    <td>{{ $type->name }} - {{ $type->Name }} </td>
                    <td>{{ $type->Price }}</td>
                    <td>{{ $type->Avaliable }}</td>
                    <td>
                        {{-- <button class="btn btn-danger">delete</button> --}}

                           <a href="/admin/type/create/{{$type->ProductId}}">
                         <button type="submit" class="btn btn-green">ویرایش</button>
                        </a>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
<div>
    {{$types->links()}}
</div>

</div>
</div>
</div>
</div>

@endsection
