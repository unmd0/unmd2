@extends('admin.master.index')
@section('admin-main')
<script src="/js/addtype.js"></script>

<main class="c-main">

    @if ($tp==1)
        @include('admin.types.addfiletype')
    @else
        @include('admin.types.addproducttype')
    @endif
</main>
@endsection
