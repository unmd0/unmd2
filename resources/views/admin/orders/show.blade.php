@extends('admin.master.index')
@section('meta_add')
<meta name="viewport" content="width=device-width, initial-scale=0.55">
@endsection
@section('admin-main')
<div class="row">
    <div class="col">
      <div class="card shadow">
        <div class="card-header border-0">
          <h3 class="mb-0">مدیریت تنوع محصولات</h3>
        </div>
        <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
            <tr>
                <th>ردیف</th>
                <th>کد سفارش</th>
                <th>مبلغ</th>
                <th>وضعیت</th>
                <th>تاریخ سفارش</th>
                <th>عملیات</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1 ?>

            @foreach($orders as $item)
                    @php $persiandate = (\Morilog\Jalali\CalendarUtils::toJalali($item->created_at->format('Y'),$item->created_at->format('m'),$item->created_at->format('d')))@endphp
                    <tr class=@if($item->shopstatus == 0) {{"neworder"}} @endif>
                    <td><?php echo($i); $i=$i + 1; ?></td>
                    <td>{{ $item->id }}</td>
                    <td> {{ $item->price }} تومان </td>
                    <td>{{ $item->status }}</td>
                    <td style="direction: ltr;">{{ $persiandate[0]}} / {{$persiandate[1]}} / {{$persiandate[2] }}</td>
                    <td>
                        {{-- <button class="btn btn-danger">delete</button> --}}

                           <a href="/admin/orders/{{$item->id}}">
                         <button type="submit" class="btn btn-green">مشاهده جزئیات</button>
                        </a>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
<div>
    {{$orders->links()}}
</div>

</div>
</div>
</div>
</div>


@endsection
