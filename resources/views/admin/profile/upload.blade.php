@extends('admin.master.index')
@section('admin-main')
<form enctype="multipart/form-data" method="post" action="{{url('admin/post/insert')}}">
    @csrf
    <div class="form-group">
        <label for="imageInput">File input</label>
        <input data-preview="#preview" name="input_img" type="file" id="imageInput">
        <img class="col-sm-6" id="preview"  src="">
        <p class="help-block">Example block-level help text here.</p>
    </div>
    <div class="form-group">
        <label for="">submit</label>
        <input class="form-control" type="submit">
    </div>
</form>
@endsection