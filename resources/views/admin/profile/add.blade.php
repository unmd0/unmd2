@extends('admin.master.index')
@section('admin-main')
@include('admin.master.layouts.loading')
<main class="c-main c-main-padd">
    <div class="uk-container uk-container-large">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
{{-- <form action="/admin/upload/png" id="upload" method="post">
    @csrf
</form> --}}
        <form action="/admin/profile/update" method="post">

@csrf
@method("PUT")

<div class="alert alert-info">تاریخ پایان بسته شما: {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(app\Http\Controllers\admin\ProductController::persian_date($profile->expire))}}</div>
            <div class="form-group">
                <label for="name">نام فروشگاه (به فارسی)</label>
                <input type="text" name="name" value="{{$profile->name}}" class="form-control">
            </div>
            <hr/>

        <input type="text" value="{{Auth::user()->name}}" class="hide" id="namehere">


        <div class="form-group">
            <label for="category">اینستاگرام</label>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <div class="input-group">
                        <input type="text" class="form-control" style="direction: ltr" value="{{$profile->instagram}}" placeholder="instagram" name="instagram">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">@</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="form-group">
            <label for="category">تلگرام</label>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <div class="input-group">
                        <input type="text" class="form-control" style="direction: ltr" value="{{$profile->telegram}}" placeholder="telegram" name="telegram">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">@</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <hr/>
            <div class="form-group">
                <output id="list">
                    @if ($profile->logo && $profile->logo != "undefined")
                    <span class="">
                        <img class="thum" style="width: auto !important" src="/img/{{$profile->logo}}.png" title="{{$profile->logo}}">
                        </span>
                    @endif

                </output>
            </div>
            <div class="form-group">
                <input type="file" class="hide" id="btn_photo"  accept=".png">
                <button type="button" class="btn btn-info" onclick="photo_select()">بارگذاری لوگو</button>
                <input type="text" id="pics1" name="logo" class="hide">
            </div>
            <hr/>
            <div class="form-group">
                <h5>هرگونه فعالیت در <a href="http://8190.org">این پلتفرم</a> به منظور پذیرفتن <a href="/admin/roles">قوانین</a> این وبسایت می باشد.</h5>
                <small id="passwordHelpInline" class="text-muted">
                {{$profile->validate_msg ? $profile->validate_msg:"صفحه اول شناسنامه خود را اسکن و یا از آن عکس بگیرید"}}
                </small><br><br>


                <output id="list2">
                    @if ($profile->validatepic && $profile->validatepic != "undefined")
                    <span class="">
                        <img class="thum" src="/img/{{$profile->validatepic}}.jpg" title="{{$profile->validatepic}}">
                        </span>
                    @endif

                </output>
            </div>
                <div class="form-group">
                    <input type="file" class="hide" id="btn_photo2"   accept=".jpg">
                    <button type="button" class="btn btn-info" onclick="photo_select2()">بارگذاری شناسنامه</button>
                    <input type="text" id="pics2" name="validatepic" class="hide">
                </div>

            <hr/>
            <div class="form-group">
                <label for="keywords">آدرس</label>
                <input type="text" name="address" value="{{$profile->address}}" class="form-control">
            </div>
            {{-- <div class="form-row">
                <div class="col-md-5 mb-3">
                    <label for="keywords">شماره تماس</label>
                    <input type="text" name="phone" class="form-control whatsappinp" value="{{$profile->phone}}" style="background: white">
                </div>
            </div> --}}
            <div class="form-group">
                <div class="form-group">
                    <label for="category">شماره شبا</label>
                    <small id="passwordHelpInline" class="text-muted">
                        جهت واریز مبلغ
                    </small>
                    <div class="form-row">
                        <div class="col-md-5 mb-3">
                            <div class="input-group">
                                <input type="text" class="form-control" style="direction: ltr" value="{{$profile->irshaba}}" placeholder="000000000000000000000000" name="irshaba">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">- IR</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="category">قیمت ارسال</label>
                    <small id="passwordHelpInline" class="text-muted">
                        مبلغ دریافتی از خریدار جهت ارسال محصولات فیزیکی
                    </small>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <div class="input-group">
                                <input type="text" class="form-control" style="direction: ltr" value="{{$profile->ship_price? $profile->ship_price:""}}" placeholder="15000" name="ship_price">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">تومان</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="form-group">
            <input type="checkbox" style="margin-right: 5px !important;position:relative;" @if ($profile->cash_on_delivery == 1) checked @endif name="cash_on_delivery">
                <label for="customControlInline">فعالسازی پرداخت در محل
                    <small id="passwordHelpInline" class="text-muted">
                        در صورتی که امکان پرداخت در محل را دارید این گزینه را فعال کنید
                    </small>
                </label>

            </div>

            {{-- <button type="submit" class="btn btn-info" form="upload">ثبت تغییرات</button> --}}
            <button type="button" class="btn btn-info" onclick="upph()">ثبت تغییرات</button>
            <button type="submit" class="hide" id="subp"></button>
        </form>
    </div>



</main>
<script>
            function upph(){

                document.getElementById("loadpage").classList.remove("hide");


        var d = new Date();
        var b= d.getTime();
        var namehere = document.getElementById("namehere").value;
        var imj = document.getElementById("list").querySelectorAll("img");
        var imj2 = document.getElementById("list2").querySelectorAll("img");
        var up1=true;
        var up2=true;
        try {
            if (imj[0].src.includes("data:")){up1=false;}
            if (imj2[0].src.includes("data:")){up2=false;}
}
catch(err) {

}

        for(i=0;i<=imj.length-1;i++){
            var nam;
            var namee;
            var namee2;
    if ($( imj2[0] ).length)
    namee2=imj2[0].title;
    else
    namee2="";
    if ($( imj[0] ).length)
    namee=imj[0].title;
    else
    namee="";



            var str = imj[i].src;
            nam=imj[i].title;
            if(str.includes("data:")){

            namee="logo/img" + (b + i);

    nam=(b + i);
    var myJsonData = { "_token": "{{ csrf_token() }}", base64image: str, name: namee }
    $.post('/admin/upload/png', myJsonData, function(response) {
        up1=true;
        if(up1==true && up2==true){
            setTimeout(function(){
                document.getElementById("pics1").value=namee;
            document.getElementById("pics2").value=namee2;
            document.getElementById("subp").click();
}, 500);
            }
});
    }
    }
    for(i=0;i<=imj2.length-1;i++){

        var nam2;
        var str2 = imj2[i].src;

        if(str2.includes("data:")){
        namee2="validate/" + namehere + (b + i);
    nam2= namehere + (b + i);
    var myJsonData = { "_token": "{{ csrf_token() }}", base64image: str2, name: namee2 }
    $.post('/admin/upload/jpg', myJsonData, function(response) {
        up2=true;
        if(up1==true && up2==true){
            setTimeout(function(){
                document.getElementById("pics1").value=namee;
            document.getElementById("pics2").value=namee2;
            document.getElementById("subp").click();
}, 500);
            }
});
    }
            };
            if(up1==true && up2==true){
            setTimeout(function(){
                document.getElementById("pics1").value=namee;
            document.getElementById("pics2").value=namee2;
            document.getElementById("subp").click();
}, 500);
            }










}


//                                                             (function () {
// var input = document.getElementById("images"),
//     formdata = false;

// function showUploadedItem (source) {
//     var list = document.getElementById("image-list"),
//         li   = document.createElement("li"),
//         img  = document.createElement("img");
//     img.src = source;
//     li.appendChild(img);
//     list.appendChild(li);
// }

// if (window.FormData) {
//     formdata = new FormData();
//     document.getElementById("btn").style.display = "none";
// }

// input.addEventListener("change", function (evt) {
//     document.getElementById("response").innerHTML = "Uploading . . ."
//     var i = 0, len = this.files.length, img, reader, file;

//     for ( ; i < len; i++ ) {
//         file = this.files[i];

//         if (!!file.type.match(/image.*/)) {
//             if ( window.FileReader ) {
//                 reader = new FileReader();
//                 reader.onloadend = function (e) {
//                     showUploadedItem(e.target.result, file.fileName);
//                 };
//                 reader.readAsDataURL(file);
//             }
//             if (formdata) {
//                 formdata.append("images[]", file);
//             }
//         }
//     }

//     if (formdata) {
//         $.ajax({
//             url: "submit_image.php",
//             type: "POST",
//             data: formdata,
//             processData: false,
//             contentType: false,
//             success: function (res) {
//                 document.getElementById("response").innerHTML = res;
//             }
//         });
//     }
// }, false);
// }());







	// jQuery.noConflict();
	// formdata = new FormData();
	// jQuery("#fg").on("change", function() {
	// 	var file = this.files[0];
	// 	if (formdata) {
	// 		formdata.append("image", file);
	// 		jQuery.ajax({
	// 			url: "/admin/upload/png",
	// 			type: "POST",
	// 			data: formdata,
	// 			processData: false,
	// 			contentType: false,
	// 			success:function(e){alert(e)}
	// 		});
	// 	}
	// });











</script>
<script src="/js/logoupload.js"></script>
@endsection
