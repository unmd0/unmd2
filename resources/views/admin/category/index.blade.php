@extends('admin.master.index')
@section('admin-main')
<script src="/js/addcategory.js"></script>
<main class="c-main" style="padding-top:20px;">
    <div class="uk-container uk-container-large">
        <input type="text" class="hide" id="idhere" value="{{$id}}">
        @isset($cname1[0])
        <a href="/admin/category/{{$cname1[0]->pid}}">{{$cname1[0]->cname}}</a>
        @endisset



        <div id="menus">
            @foreach ($categorys as $category)
            <div class="row">
                <div class="col" style="text-align: center">
                    <input type="text" disabled class="form-control-plaintext" style="text-align: center" value="نام دسته:">
                </div>
                <div class="col">
                    <input type="text" value="{{$category->cname}}" disabled class="form-control-plaintext" value="">
                </div>
                <div class="col">
                    <input type="text" class="hide" value="{{$category->id}}">
                    <button type="button" class="btn btn-info" onclick="editmebtn(this)">ویرایش</button>
                    <!-- TODO (admin) (category) (2) : add subcategory -->
                    <a href="/admin/category/{{$category->id}}" class="hide">
                        <button type="button" class="btn btn-green">اضافه کردن زیر دسته</button>
                    </a>
                    <button type="button" class="btn btn-red" onclick="deleteme(this)">حذف</button>
                </div>
                </div>
            @endforeach
        </div>
        <div class="c-cart1">
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2" style="text-align: center">نام دسته</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control" id="newname" value="">
                      <!-- class="form-control-plaintext" -->
                    </div>
                    <div class="col-sm-4" style="text-align: center">
                        <input type="text" class="hide">
                        <button type="button" class="btn btn-info" onclick="saveme(this)">اضافه کردن</button>
                        {{-- <button type="button" class="btn btn-green hide" onclick="submenu()">اضافه کردن زیر دسته</button> --}}
                        <button type="button" class="btn btn-red hide" onclick="deleteme()">حذف</button>
                    </div>
                  </div>
                <button type="submit" class="hide">ثبت محصول</button>
        </div>

    </div>
</main>
<script>
        function saveme(x) {
        var name = document.getElementById("newname").value;
        var pid = document.getElementById("idhere").value;
        var myJsonData = { "_token": "{{ csrf_token() }}", pide: pid, namee: name }
$.post('/admin/category/add', myJsonData, function(response) {
    $("#menus").append('<div class="c-cart1"><div class="form-group row"><label for="staticEmail" class="col-sm-1">نام دسته</label><div class="col-sm-5"><input type="text" value="' + name + '" disabled class="form-control-plaintext" value=""><!-- class="form-control-plaintext" --></div><div class="col-sm-6"> <input type="text" class="hide" value="'+ response +'"> <button type="button" class="btn btn-info" onclick="editmebtn(this)">ویرایش</button> <!-- TODO (admin) (category) (2) : add subcategory --><a href="/admin/category/'+ response +'" class="hide"> <button type="button" class="btn btn-green">اضافه کردن زیر دسته</button></a><button type="button" class="btn btn-red" onclick="deleteme(this)">حذف</button></div></div></div>');
            document.getElementById("newname").value="";
});

    }
    function editme(x){
        var obj1 = x.parentElement.parentElement;
        var name = obj1.children[1].children[0].value;
        var id = x.previousElementSibling.value.replace(" ",'');
        var myJsonData = { "_token": "{{ csrf_token() }}", pide: id, namee: name }
$.post('/admin/category/edit', myJsonData, function(response) {
    var obj1 = x.parentElement.parentElement;
                obj1.children[1].children[0].disabled=true;
                //$(obj1).children(2).children(1).att('disabled', false);
                obj1.children[1].children[0].classList.add("form-control-plaintext");

                obj1.children[1].children[0].classList.remove("form-control");
                x.innerText ="ویرایش";
                // x.nextElementSibling.classList.remove("hide");
                x.nextElementSibling.nextElementSibling.innerText="حذف";
                x.nextElementSibling.nextElementSibling.setAttribute('onclick','deleteme(this)');
                 x.setAttribute('onclick','editmebtn(this)');

});
    }
    function deleteme(x) {
        x.parentElement.parentElement.parentElement.remove();
        var pid1=x.previousElementSibling.previousElementSibling.previousElementSibling.value.replace(" ",'');
        var myJsonData = { "_token": "{{ csrf_token() }}", pide: pid1 }
$.post('/admin/category/delete', myJsonData, function(response) {

});
    }
</script>
@endsection
