<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>مدیریت</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/login/util.css">
	<link rel="stylesheet" type="text/css" href="/css/login/main.css">
    @include('admin.master.layouts.links.index')
    <style>
        html,
body {
    color: white;
    height: 100%;
    background-image: url("{{asset('img/4_21.jpg')}}");
    background-repeat: repeat;
    background: #9053c7;
    background: -webkit-linear-gradient(-135deg, #c850c0, #4158d0);
    background: -o-linear-gradient(-135deg, #c850c0, #4158d0);
    background: -moz-linear-gradient(-135deg, #c850c0, #4158d0);
    background: linear-gradient(-135deg, #c850c0, #4158d0);
}
::-webkit-input-placeholder {
  text-align: center;
}

:-moz-placeholder {
  text-align: center;
}
body::before{

    transform: rotate(30deg);
}
        .containerv {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
            }
        </style>
</head>

<body>

    {{-- <main class="c-main c-main-padd"> --}}
        <div class="containerv">
            <div>
                @if (Auth::user()->phone == "0")
                <form style="text-align: center;" action="/admin/otpaddphone" method="post">
                    @csrf
                    @method("PUT")
                    @if(isset($errorMessageDuration))
                        <div class="alert alert-danger">
                            {{ $errorMessageDuration }}
                        </div>
                    @endif
                    <div class="form-group mb-2">
                        <h2 style="color: white;">شماره موبایل خود را وارد کنید</h2>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                      <label for="inputPassword2" class="sr-only" style="color: white;">شماره موبایل</label>
                      <input name="phone" type="tell" maxlength="11" minlength="11" style="text-align: center;" class="form-control form-control-lg" id="inputPassword2" placeholder="شماره موبایل">
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            تایید
                        </button>
                    </div>
                  </form>
                @else
                <form style="text-align: center;" action="/admin/otpcheck" method="post">

                    @csrf
                    @method("PUT")
                    @if(isset($errorMessageDuration))
                        <div class="alert alert-danger">
                            {{ $errorMessageDuration }}
                        </div>
                    @endif
                    <div class="form-group mb-2">
                        <h2 style="color: white;">کد فعالسازی را وارد کنید</h2>
                        <label style="color: white;">پیامکی حاوی کد فعالسازی برای شماره زیر ارسال شده است.</label>

                      <label for="staticEmail2" class="sr-only">شماره</label>
                      <input type="text" readonly class="form-control-plaintext" style="text-align: center;color: #fdfdfd; font-size: 20px;" id="staticEmail2" value="{{ MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Auth::user()->phone) }}">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                      <label for="inputPassword2" class="sr-only">کد فعالسازی</label>
                      <input name="phone_code" type="tell" maxlength="4" minlength="4" style="text-align: center;" class="form-control form-control-lg" id="inputPassword2" placeholder="کد فعالسازی">
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            تایید
                        </button>
                    </div>
                  </form>
                @endif
                <form action="{{route('logout')}}" style="    text-align: center;" method="POST">
                    @csrf
                    <br/>
                    <button class="btn btn-danger mb-2" type="submit">
                        <span uk-icon="icon: ban;" class="uk-icon"></span>
                        خروج

                </button>
                </form>
            </div>
          </div>
    {{-- </main> --}}

</body>

</html>



