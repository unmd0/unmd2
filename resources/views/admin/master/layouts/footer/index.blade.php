<footer class="c-footer">
    <div class="c-footer__top">
        <div class="uk-container uk-container-large">
            <div class="uk-flex uk-flex-between uk-flex-middle">
                <ul class="uk-subnav uk-subnav-divider">

                    <li>
                        <a
                            href="">
                            پشتیبانی
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="c-footer__bottom">
        <div class="uk-container uk-container-large">
            <div class="uk-flex uk-flex-between">
                <div></div>
                <div><a href="/">www.8190.org</a> 2020</div>
            </div>
        </div>
    </div>
</footer>
<script src="/js/index.js"></script>
<script src="/js/dropdown.min.js"></script>
<script src="/js/package.js"></script>