<li class="first-level">
    <a href="/admin/files">فایل ها
        <span class="chevron-down"></span>
    </a>
    <div class="uk-navbar-dropdown">
        <ul class="uk-nav uk-navbar-dropdown-nav">
            <li>
                <a href="/admin/files/upload">
                    بارگذاری فایل
                </a>
            </li>
            <li>
                <a href="/admin/files">
                   مدیریت فایل ها
                </a>
            </li>
        </ul>
    </div>
</li>