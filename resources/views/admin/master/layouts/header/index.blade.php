@php
    $expr=app\Http\Controllers\admin\ProductController::expiredays();
    $site=Auth::user()->name;
    $aler="info";
@endphp
@if( $expr <= 30 && $expr > 15)
    @php $aler="warning" @endphp
    @elseif($expr <= 15)
        @php $aler="danger" @endphp
        @endif
        @if(!Auth::user()->hasVerifiedEmail())
            <div class="alert alert-danger" role="alert" style="margin: 0px;border-radius: 0;">
                    <h4 class="alert-heading">تایید ایمیل</h4>
                    <p>لطفا وارد ایمیل خودتان شوید و حسابتان را فعال کنید</p>
                    <p>در صورت تایید نکردن ایمیلتان قادر به انجام تغییرات در پنل مدیریت نمیباشید</p>
                    <hr>
                    <p class="mb-0" style="display: contents;">ایمیلی دریافت نکرده اید؟ </p>
                <form action="{{ route('verification.resend') }}" style="display: contents"
                    method="POST">
                    @csrf
                    <button type="submit" class="btn btn-warning">
                        ارسال مجدد

                    </button>
                </form>
            </div>
        @else

        @endif
        @php $aler="success" @endphp
        <div class="alert alert-{{ $aler }}" style="margin: 0px;border-radius: 0;">
            <div class="uk-container uk-container-large fullalert">

                {{ MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($expr) }}
                {{-- {{ MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(369)}}   --}}
              روز تا پایان بسته شما <a
                  href="{{ url('/admin/buy') }}"> | میخواهم بیشتر بفروشم |</a>
            </div>
</div>
        <header class="c-header js-header">
            <div class="uk-container uk-container-large">
                <div class="c-header__top">
                    <h1 class="c-header__logo"><a
                            href="https://{{ Auth::user()->name }}.{!! str_replace("www.","",Request::getHost()) !!}" target="_blank"
                            style="text-decoration: none;">
                            <h2 class="btn btn-green" style="float: left;margin: -10px 0px;"> مشاهده فروشگاه</h2>
                        </a></h1>
                    <a href="/admin" style="text-decoration: none;">
                        <h2 class="c-header__tag">مدیر
                            {{ DB::table('profiles')->where('userid','=',$site)->first()->name ? DB::table('profiles')->where('userid','=',$site)->first()->name : $site }}
                        </h2>
                    </a>

                </div>
            </div>
        </header>
