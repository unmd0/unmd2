<!DOCTYPE html>
<html dir="rtl">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>مدیریت</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
@yield('meta_add')
    @include('admin.master.layouts.links.index')
    @yield('add_link')
</head>

<body>
    @include('admin.master.layouts.header.index')
    @include('admin.master.layouts.menu.index')
    @yield('admin-main')
    @include('admin.master.layouts.footer.index')
</body>
    @yield('add_script')
    <script type="text/javascript" defer>
        // start hantana code
        (function(d,w,u,t,s){
           s = w.createElement("script");
           d._hantanaSettings={tId:t};
           s.type = "text/javascript", s.async = !0, s.src = u + t;
           h = w.getElementsByTagName('head')[0];
           h.appendChild(s);
        })(window,document,'https://hantana.org/widget/','5f64e-5adc9-296a5-7fa3c');
      </script>
</html>
