@extends('admin.master.index')
@section('admin-main')
<main class="c-main c-main-padd">
    <div class="uk-container uk-container-large">

        <div class="alert alert-danger">لطفا قوانین را به دقت مطالعه فرمایید</div>
        <h4 style="line-height: 30px">
        1.درصورت استفاده از درگاه پرداخت این پلتفرم شما مجاب به پیروی از قوانین زرین پال که در آدرس <a href="https://www.zarinpal.com/terms.html">https://www.zarinpal.com/terms.html</a> موجود میباشد هستید. <br><br>
        2.این پلتفرم هیچ تعهدی برای تامین و ارسال محصولات سفارش گذاری شده را ندارد و این کار بر عهده فروشنده میباشد.<br><br>
        3.فروشنده ملزم به تبعیت از قوانین جمهوری اسلامی ایران میباشد.<br><br>
        4.در صورت رعایت نکردن قوانین سریعا با فروشگاه قطع همکاری میگردد.<br><br>
        </h4>   
    </div>
</main>
@endsection