
@if (isset($msg))
<div class="alert alert-{{$alert}}">{{$msg}}</div>
@endif
<section class="site-section" style="padding: 0 0 0 0 !important;margin-top: -30px;" id="pricing-section">
    <div class="container">
      <div class="row justify-content-center" data-aos="fade-up">
        <div class="col-lg-7 text-center heading-section mb-5">
          <div class="paws">
            <span class="icon-paw"></span>
          </div>
          <h2 class="mb-2 text-black heading">بسته ها</h2>
          <p>لطفا بسته مورد نظر را انتخاب نمایید</p>
        </div>
      </div>
      <div class="row no-gutters rtl">

        <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-dark  p-3 p-md-5" style="    background-color: #cfc8ff !important;" data-aos="fade-up" data-aos-delay="100">
            <div class="pricing">
              <h4 class="text-center text-white text-uppercase rtl text-white special_sug" style="background-color: #ab9fff ">ارزان ترین</h4>
              <h3 class="text-center text-white text-uppercase rtl text-black" style="margin-top: 30px;margin-bottom: 14px;">۶۹ روزه</h3>

              <div class="price text-center mb-4 rtl text-black">
                <span class="rtl text-black"><span  style="color: #ff046c !important;">۶۹</span> هزار تومان</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">
                
                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li class="remove">Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
              <a href="{{url("admin/buy/1")}}" class="btn btn-secondary">خرید</a>
              </p>
            </div>
          </div>



          <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-dark  p-3 p-md-5" style="    background-color: #ff0087 !important" data-aos="fade-up" data-aos-delay="100">
            <div class="pricing">
              <h4 class="text-center text-white text-uppercase rtl text-white special_sug">پیشنهاد ویژه</h4>
              <h3 class="text-center text-white text-uppercase rtl text-white" style="margin-top: 30px;margin-bottom: 14px;">۹۶ روزه</h3>
              <div class="price text-center mb-4 rtl text-black">
                <span class="rtl" style="color: #ffeb00 !important;"><span  style="color: #ffeb00 !important;">۹۶</span> هزار تومان</span>
              </div>
              <!-- <ul class="list-unstyled ul-check success mb-5">
                
                <li>Officia quaerat eaque neque</li>
                <li>Possimus aut consequuntur incidunt</li>
                <li>Lorem ipsum dolor sit amet</li>
                <li>Consectetur adipisicing elit</li>
                <li class="remove">Dolorum esse odio quas architecto sint</li>
              </ul> -->
              <p class="text-center">
                <a href="{{url("admin/buy/2")}}" class="btn btn-secondary" style="background-color: #c30469;">خرید</a>
              </p>
            </div>
          </div>



        <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-dark  p-3 p-md-5" style="    background-color: #d1e1f5 !important;" data-aos="fade-up" data-aos-delay="100">
          <div class="pricing">
            <h4 class="text-center text-white text-uppercase rtl text-white special_sug" style="background-color: #70afff ">% بیشترین تخفیف %</h4>
            <h3 class="text-center text-white text-uppercase rtl text-black" style="margin-top: 30px;margin-bottom: 14px;">۱۹۶ روزه</h3>
            <div class="price text-center mb-4 rtl text-black">
              <span class="rtl text-black"><span  style="color: #ff046c !important;" >۱۶۹</span> هزار تومان</span>
            </div>
            <!-- <ul class="list-unstyled ul-check success mb-5">
              
              <li>Officia quaerat eaque neque</li>
              <li>Possimus aut consequuntur incidunt</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Consectetur adipisicing elit</li>
              <li class="remove">Dolorum esse odio quas architecto sint</li>
            </ul> -->
            <p class="text-center">
              <a href="{{url("admin/buy/3")}}" class="btn btn-secondary">خرید</a>
            </p>
          </div>
        </div>


        <div class="col-12 col-sm-6 col-md-6 col-lg-4 bg-primary  p-3 p-md-5" style="background-color: #c5daf5 !important;margin: auto;" data-aos="fade-up" data-aos-delay="200">
          <div class="pricing">
            <h4 class="text-center text-white text-uppercase rtl text-white special_sug" style="background-color: #328cff ">* کامل ترین *</h4>
            <h3 class="text-center  text-uppercase rtl" style="margin-top: 30px;margin-bottom: 14px;">۳۶۹ روزه</h3>
            <div class="price text-center mb-4 rtl text-black">
              <span class="rtl text-black"><span class="text-black" style="color: #ff046c !important;" >۳۶۹</span> هزار تومان</span>
            </div>
            <!-- <ul class="list-unstyled ul-check success mb-5">
              
              <li>Officia quaerat eaque neque</li>
              <li>Possimus aut consequuntur incidunt</li>
              <li>Lorem ipsum dolor sit amet</li>
              <li>Consectetur adipisicing elit</li>
              <li>Dolorum esse odio quas architecto sint</li>
            </ul> -->
            <p class="text-center">
              <a href="{{url("admin/buy/4")}}" class="btn btn-secondary">خرید</a>
            </p>
          </div>
        </div>






      </div>
    </div>
  </section>
