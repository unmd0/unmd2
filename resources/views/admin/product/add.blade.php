@extends('admin.master.index')
@section('admin-main')
@include('admin.master.layouts.loading')
<main class="c-main c-main-padd">



    <div class="uk-container uk-container-large">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="/admin/product/create" method="post">
@csrf
@method("PUT")
<div class="form-group">
    <input type="checkbox" style="margin-right: 5px !important;position:relative;" name="type">
    <label for="customControlInline">محصول موردنظر به صورت فایل میباشد.</label>
</div>
            <div class="form-group">
                <label for="name">نام محصول</label>
                <input type="text" name="name" id="name_prd" class="form-control">
            </div>
            <div class="form-group">
                <label for="desc">توضیحات</label>
                <textarea name="desc" id="desc" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label for="category">دسته بندی</label>
                @if (!$category->isempty())
                <select class="ui search dropdown" name="category">



                    @foreach ($category as $item)
                    <option value="{{$item->cname}}" >{{$item->cname}}</option>
                    @endforeach

      </select>
      @else
      <div class="alert alert-danger">
          <ul>
      لطفا در قسمت <a href="/admin/category/0">"مدیریت دسته بندی"</a> اقدام به ساخت دسته بندی جدید نمایید و سپس اقدام به ساخت محصول کنید.
    </ul>
      </div>
      @endif
            </div>
            <div class="form-group">
                <input type="file" multiple class="hide" id="btn_photo"   accept=".jpg">
                <button type="button" class="btn btn-info" onclick="photo_select()">افزودن عکس</button>
                <input type="text" id="pics1" name="pic" class="hide">
            </div>
            <div class="form-group">
                <output id="list"></output>
            </div>



            <div class="form-group">
                <label for="keywords">کلمات کلیدی (جداسازی با ، )</label>
                <input type="text" name="keywords" class="form-control">
            </div>
            <div class="c-cart1" style="display: block;text-align: center;">
                میخواهم این محصول به تنوع های محصول
                    <select class="form-control" id="sel1" name="link_type" style="width: auto;display: inline;">
                        <option selected value>خالی</option>
                        @foreach ($autoc as $itemc)
                        @if (DB::table('big_s_products_type')->where("big_s_products_type.show","=","0")->where('ProductId',"=",$itemc->productID)->get()->isNotEmpty())
                        <option value="{{$itemc->productID}}" >{{$itemc->name}}</option>
                        @endif

                        @endforeach
                    </select>
                متصل شود.
                <br/>
            </div>
            <button type="button" class="btn btn-info" onclick="subm()">ثبت محصول و رفتن به مرحله بعد</button>
            <button type="submit" class="hide" id="subp"></button>
        </form>
    </div>



</main>
<script>




    function subm() {
        document.getElementById("loadpage").classList.remove("hide");
var d = new Date();
var b= d.getTime();
var c1="";
var imj = document.getElementById("list").querySelectorAll("img");
var jobs=[];
var countup=0;
// console.log(imj.length);
for(i=0;i<=imj.length-1;i++){
    var namee;
    var str = imj[i].src;
    namee=imj[i].title;
    if(str.includes("data:")){
        countup=countup+1;
    namee=b + i;
    var myJsonData = { "_token": "{{ csrf_token() }}", base64image: str, name: namee }
$.post('/admin/upload/jpg', myJsonData, function(response) {
    jobs.push(true);
    if(jobs.length==countup){


        document.getElementById("pics1").value=c1.substring(0, c1.length - 1);
        setTimeout(function(){
    document.getElementById("subp").click();
}, 500);
        }
});
}
if (imj[i].parentElement.classList.contains("select-main-pic")){namee= "*" + namee + "*" ;}

c1 = c1 + namee + ",";
// console.log("c1="+c1);
    };

}
</script>
@endsection
