@extends('admin.master.index')



{{-- add link to header --}}
@section('add_link')
<link rel="stylesheet" href="/css/dropzone.css">
<script src="/js/dropzone.min.js"></script>
@endsection
{{-- END add link to header --}}


{{-- Main --}}
@section('admin-main')

<main class="c-main" style="padding-top:20px;">
    <div class="uk-container uk-container-large">
        @include('admin.files.listfiles')
    </div>
</main>

@endsection
{{-- END Main --}}



{{-- add script to footer --}}
@section('add_script')

@endsection
{{-- END script to footer --}}