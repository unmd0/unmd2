<div class="filelist">
    <div>
        @if ($filelist->extension == "jpg" || $filelist->extension == "jpeg" || $filelist->extension == "png")
        <img src="{{ URL::to('/') }}{{$filelist->address}}" class="thum" alt="{{$filelist->ect}}">
        @else
        <div class="thum ext"><span class="showext">{{$filelist->extension}}.</span></div>
        @endif

    </div>
       <span class="spanlist">
            نام فایل:  <span class="caption-info" >{{$filelist->ect}}</span><br/>
            <span style="display: contents;font-size: smaller;">حجم: <span class="alert-danger" >{{$filelist->size}}MB</span></span><br/><br/>
            
        </span>
        @if ($listtype==0)
        <div style="display: flex;padding-right: 30px;padding-left: 30px;"><button class="btn btn-danger" onclick="deleteme(this,{{$filelist->id}})" style="margin: auto">حذف</button></div>
        @elseif($listtype==1)
        <div style="display: grid;">
        <div style="display: flex;padding-right: 30px;padding-left: 30px;"><button type="button" class="btn btn-success" onclick="selectme8(this,{{$filelist->id}},'{{$filelist->ect}}','{{ URL::to('/') }}{{$filelist->address}}')" style="margin: auto"> انتخاب</button></div><br/>
        </div>
        @endif
        
</div>