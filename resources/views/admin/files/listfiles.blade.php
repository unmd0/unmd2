
<div id="uploadalert" class="alert alert-danger  {{isset($list[0]) ? 'hide':''}}">
    <div style="width: 100%;display: flex;">
        <span style="margin: auto">لطفا اقدام به اضافه کردن فایل نمایید.</span>
    </div>
</div>







<div id="file">
    <form action="/admin/uploadfile" class="dropzone" id="dropzone" method="POST">
      @csrf
      <div class="dz-message" data-dz-message><span>لطفا فایل/فایلهای خود را در این قسمت رها کنید</span><br/><span>و یا در این قسمت کلیلک کنید</span></div>
        <div class="fallback">
          
          <input name="file" type="file" class="hide" multiple />
        </div>
      </form>
    </div>







<div id="filessection">

@if(isset($list[0]))

@foreach ($list as $filelist)

@include('admin.files.filesection',["filelist" => $filelist])

@endforeach

@endif

</div>














@section('add_script')
<script>
  function deleteme(x,y){
      var myJsonData = { "_token": "{{ csrf_token() }}", id: y }
        $.post('/admin/destroy', myJsonData, function(response) {
          x.parentElement.parentElement.remove();
          if ($(".uk-container-large").find(".filelist").length <= 0){$("#uploadalert").removeClass("hide")}
        });
  }
</script>



<script type="text/javascript">
Dropzone.options.dropzone =
 {
   
  headers: {
    'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
},
    maxFilesize: 70,
    addRemoveLinks: true,
    timeout: 500000,
    // autoProcessQueue:false,
    dictCancelUpload: "لغو بارگذاری",
    dictCancelUploadConfirmation: "آیا از لغو بارگذاری اطمینان دارید؟",
    dictRemoveFile: "حذف",
    dictFileTooBig: "حجم فایل بیش از حد مجاز میباشد (حد مجاز: 70 مگابایت(",
    dictResponseError:"مشکلی در سرور پیش آمده. لطفا با پشتیبانی در ارتباط باشید.",
    dictUploadCanceled:"بارگذاری لغو شد",
    removedfile: function(file) 
          {
              var name = file.upload.filename;
              var myJsonData = { "_token": "{{ csrf_token() }}", filename: name }
        $.post('/admin/destroy', myJsonData, function(response) {
          console.log(response);
        });
                  var fileRef;
                  return (fileRef = file.previewElement) != null ? 
                  fileRef.parentNode.removeChild(file.previewElement) : void 0;
          },
    success: function(file, response) 
    {
      this.removeFile(file);
      jsa=JSON.parse(response);
      jsa.size = (file.size/1000000).toFixed(2);
      console.log(jsa.size);
      var myJsonData = { "_token": "{{ csrf_token() }}", address: jsa.address, shopname: jsa.shopname, extension: jsa.extension, ect:jsa.ect, size: jsa.size, listtype:{{$listtype}} }
        $.post('/admin/storefile', myJsonData, function(response) {
          $("#filessection").prepend(response);
        });
    },
    error: function(file, response)
    {
      // alert(response+"آپلود نشد");
    }
};


</script>
@endsection
