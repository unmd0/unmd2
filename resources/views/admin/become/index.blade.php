@php
        $list=App\filemanage::where("shopname","=",Auth::user()->name)->orderBy('created_at', 'DESC')->get();
        $listtype=1;
@endphp
@extends('shop.master.index')
@section('shop-main')
@include('admin.master.layouts.loading')
<link rel="stylesheet" href="/css/become/index.css">
<link rel="stylesheet" href="/css/dropzone.css">
<script src="/js/dropzone.min.js"></script>



<div id="popup1">
  <button type="button" onclick="hidepopup()"  class="btn btn-danger closepopup">بستن</button>
  <div class="popup">
      @include('admin.files.listfiles')

</div>

</div>







 <!-- Modal -->
 <div class="modal fade" style="" id="myModal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">تنظیمات</h4>
      </div>
      <div class="modal-body" style="overflow: hidden auto;">
        <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
      </div>
    </div>

  </div>
</div>









    <div class="container" id="gd">




    @php $count=0; @endphp
    @foreach (explode(',', $page->content) as $pg)
    {{--  --}}
    @if($pg <> "")

            {{-- app/gadget_propertie::where("id","=","1")->first()->main_file --}}
          @php
                $gadgetid=App\gadget_setting::where("id","=",$pg)->first()->gadget_id;
                $add = App\gadget_propertie::where("id","=",$gadgetid)->first()->main_file;

                $json = App\gadget_setting::where("id","=",$pg)->first()->setting;

                $parametr = json_decode($json);
                $count+=1;
          @endphp

  <div class="gdg_div" id="gdg_div-{{$count}}" data-id="{{$count}}">

            @include($add,["pmr" => $parametr,"global_gdgid" => $pg])

            </section>
          </div>


    @endif
    @endforeach



  </div>








<div id="gad-sec" class="gad-sec">
  @foreach ($gadgets as $gad)
    <div class="gad">
      <div style="height: 100px;display: flex;margin-bottom: 10px;">
    <img src="{{asset($gad->pic)}}" alt="{{$gad->name}}">
      </div>
    <label>{{$gad->description}}</label>
      <input type="button" class="btn btn-info" value="اضافه کردن" onclick="addgadget({{$gad->id}})">
    </div>
    <hr/>
  @endforeach

</div>
<div class="manage">


      <input type="button" id="gad-btn" class="btn btn-success" value="+/- مدیریت بخش ها" onclick="open_close_gad()">
      <a href="/admin" ><input type="button" id="gad-btn" class="btn btn-info" style="float: left;margin-right: 8px;" value="بازگشت "></a>
      <a href="/admin/gadget/preview" target="_blank"><input type="button" id="gad-btn" class="btn btn-success" style="float: left" value="پیش نمایش"></a>

</div>















<script>

  var count = {{$count}};


  ////:::::::::::::::::::::::::::::::::::::////
  ////:::::::::::::add gadget:::::::::::////
  ////:::::::::::::::::::::::::::::::::::::////
    function addgadget(x){
      document.getElementById("loadpage").classList.remove("hide");
      count = count+1;
    var myJsonData = { "_token": "{{ csrf_token() }}", idd:x , pagee:"{{$pgname}}" }
       $.post('/admin/gadget/add', myJsonData, function(response) {

var gdgmanagestr=
       '<div class="gdg_div" id="gdg_div-' + count + '" data-id="' + count + '">' +
    response +
    '</section></div>';

$("#gd").append(gdgmanagestr);
setTimeout(function() {
    document.getElementById("loadpage").classList.add("hide");
    }, 100);
   });
}
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////






    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::Gadget Setting:::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    function gdgsetting(x){
      document.getElementById("loadpage").classList.remove("hide");
        $(".modal-body").html("");
       var myJsonData = { "_token": "{{ csrf_token() }}", idd:x , pagee:"{{$pgname}}"}
       $.post('/admin/gadget/setting', myJsonData, function(response) {
       $(".modal-body").html(response);
       setTimeout(function() {
    document.getElementById("loadpage").classList.add("hide");
    }, 100);
    });

    }
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////






    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::Gadget Update:::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    function gdgupdate(x){
      document.getElementById("loadpage").classList.remove("hide");
    var myJsonData = $("form").serialize();
       $.post('/admin/gadget/setting-update', myJsonData, function(response) {
        $("#gdgsetting_n" + x).html(response);
        setTimeout(function() {
    document.getElementById("loadpage").classList.add("hide");
    }, 100);
    });
}
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////




    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::Gadget Up:::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    function gdgsetting_up(x,y){

      var gdgdiv = Number($(x).closest( ".gdg_div" ).attr("data-id"));


      if ($("#gdg_div-" + (gdgdiv-1)).length ){

      $("#gdg_div-" + gdgdiv).insertBefore( "#gdg_div-" + (gdgdiv-1) );


      var myJsonData = { "_token": "{{ csrf_token() }}", xx: gdgdiv, yy: gdgdiv-1 , pagee:"{{$pgname}}"}
       $.post('/admin/gadget/swap', myJsonData, function(response) {

    });
    $("#gdg_div-" + gdgdiv).attr("data-id", (gdgdiv-1));
    $("#gdg_div-" + (gdgdiv-1)).attr("data-id", gdgdiv);


    $("#gdg_div-" + gdgdiv).attr("id", "tmpj");
    $("#gdg_div-" + (gdgdiv-1)).attr("id", "gdg_div-" + gdgdiv);
    $("#tmpj").attr("id", "gdg_div-" + (gdgdiv-1));

  }
  // var swiper = new Swiper('.swiper-container', {
  //         navigation: {
  //           nextEl: '.swiper-button-next',
  //           prevEl: '.swiper-button-prev',
  //         },
  //       });
    }
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////






    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::Gadget Down:::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    function gdgsetting_down(x,y){
      var gdgdiv = Number($(x).closest( ".gdg_div" ).attr("data-id"));


      if ($("#gdg_div-" + (gdgdiv+1)).length ){
        $("#gdg_div-" + gdgdiv).attr("data-id", (gdgdiv+1) );
        $("#gdg_div-" + (gdgdiv+1)).attr("data-id", gdgdiv);
      $("#gdg_div-" + (gdgdiv+1)).insertBefore( "#gdg_div-" + gdgdiv );

      $("#gdg_div-" + gdgdiv).attr("id","gdg_div-" + (gdgdiv+1));

      $("#gdg_div-" + (gdgdiv+1)).attr("id","gdg_div-" + gdgdiv);



      var myJsonData = { "_token": "{{ csrf_token() }}", xx: gdgdiv, yy: gdgdiv + 1 , pagee:"{{$pgname}}" }
       $.post('/admin/gadget/swap', myJsonData, function(response) {

    });
  }
    }
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////







    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::Close Gadget:::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    function gdgsetting_close(x,y){

      var gdgdiv = Number($(x).closest( ".gdg_div" ).attr("data-id"));
      for (i=gdgdiv;i<150;i++){
        if ($("#gdg_div-" + (i+1)).length ){

        var tmp2 = $("#gdg_div-" + (i+1)).html() ;

        $("#gdg_div-" + i).html(tmp2) ;
        $("#gdg_div-" + (i+1)).html("") ;
        }else{
          count = count-1;
          $("#gdg_div-" + (i)).remove();
          break;
        }
      }

      var myJsonData = { "_token": "{{ csrf_token() }}", xx: y , pagee:"{{$pgname}}" }
       $.post('/admin/gadget/delete', myJsonData, function(response) {

        });
  // }
    }
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////






    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::Open Close Gadget Section:::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    function open_close_gad(){
      $( "#gad-sec" ).toggleClass( "gad-hide" );

    }
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////
    ////:::::::::::::::::::::::::::::::::::::////



  document.getElementById("loadpage").classList.add("blackloader");
  document.getElementById("loadpage").classList.remove("hide");


  $(document).ready(function(){
    setTimeout(function() {
    document.getElementById("loadpage").classList.add("hide");
    document.getElementById("loadpage").classList.remove("blackloader");
    $( "#gad-sec" ).toggleClass( "gad-hide" );
    }, 2000);
    $('.gdg_div a').click(function(e) {
        e.preventDefault();
    });



});




</script>
<script type="text/javascript">
  Dropzone.options.dropzone =
   {

    headers: {
      'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
  },
      maxFilesize: 70,
      addRemoveLinks: true,
      timeout: 500000,
      // autoProcessQueue:false,
      dictCancelUpload: "لغو بارگذاری",
      dictCancelUploadConfirmation: "آیا از لغو بارگذاری اطمینان دارید؟",
      dictRemoveFile: "حذف",
      dictFileTooBig: "حجم فایل بیش از حد مجاز میباشد (حد مجاز: 70 مگابایت(",
      dictResponseError:"مشکلی در سرور پیش آمده. لطفا با پشتیبانی در ارتباط باشید.",
      dictUploadCanceled:"بارگذاری لغو شد",
      removedfile: function(file)
            {
                var name = file.upload.filename;
                var myJsonData = { "_token": "{{ csrf_token() }}", filename: name }
          $.post('/admin/destroy', myJsonData, function(response) {
            console.log(response);
          });
                    var fileRef;
                    return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
      success: function(file, response)
      {
        this.removeFile(file);
        jsa=JSON.parse(response);
        jsa.size = (file.size/1000000).toFixed(2);
        console.log(jsa.size);
        var myJsonData = { "_token": "{{ csrf_token() }}", address: jsa.address, shopname: jsa.shopname, extension: jsa.extension, ect:jsa.ect, size: jsa.size, listtype:{{$listtype}} }
          $.post('/admin/storefile', myJsonData, function(response) {
            $("#filessection").prepend(response);
          });
      },
      error: function(file, response)
      {
        // alert(response+"آپلود نشد");
      }
  };


  </script>
@endsection
