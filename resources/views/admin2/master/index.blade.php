<!DOCTYPE html>

<html lang="en" dir="rtl">
@include('admin2.master.head')
<body class="">
@include('admin2.master.navbar')
  <div class="main-content">
    <!-- Navbar -->
@include('admin2.master.topnavbar')
    <!-- End Navbar -->
@include('admin2.master.content')

  </div>

@include('admin2.master.footer')

</body></html>
