<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta_add')
    <title>پنل مدیریت</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <!-- Fonts -->
    <link href="{{ asset('newadmin/header/css?ver=1.001') }}" rel="stylesheet">
    <!-- Icons -->
    <link href="{{ asset('newadmin/header/nucleo.css') }}" rel="stylesheet">
    <link href="{{ asset('newadmin/header/all.min.css') }}" rel="stylesheet">
    <!-- CSS Files -->
    <link href="{{ asset('newadmin/header/argon-dashboard.css?ver=1.001') }}" rel="stylesheet">
    <!-- CSS fonts -->


    {{-- <link rel="stylesheet" href="/bootstrap-4.4.1-dist/css/rtl/bootstrap.min.css"> --}}
    <link rel="stylesheet" href="/css/a49e977c.css">
    <link rel="stylesheet" href="/css/08a18db4.css">
    <link rel="stylesheet" href="/css/index.css">
    {{-- <link rel="stylesheet" href="/css/dropdown.min.css"> --}}
    <script src="/js/11b2f4e1.js.download"></script>
     <script src="/js/add.js"></script>

    <script src="/js/jquery.js"></script>


    @yield('add_link')
    <link type="text/css" href="{{ asset('newadmin/header/style.css?ver=1.001') }}" rel="stylesheet">
  <style type="text/css">/* Chart.js */

  @keyframes chartjs-render-animation{from{opacity:.99}to{opacity:1}}.chartjs-render-monitor{animation:chartjs-render-animation 1ms}.chartjs-size-monitor,.chartjs-size-monitor-expand,.chartjs-size-monitor-shrink{position:absolute;direction:ltr;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1}.chartjs-size-monitor-expand>div{position:absolute;width:1000000px;height:1000000px;left:0;top:0}.chartjs-size-monitor-shrink>div{position:absolute;width:200%;height:200%;left:0;top:0}</style></head>
