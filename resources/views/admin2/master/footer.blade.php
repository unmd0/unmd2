  <!--   Core   -->
  <script src="{{ asset('newadmin/footer/jquery.min.js.download')}}"></script>
  <script src="{{ asset('newadmin/footer/bootstrap.bundle.min.js.download')}}"></script>
  <!--   Optional JS   -->
  <script src="{{ asset('newadmin/footer/Chart.min.js.download')}}"></script>
  <script src="{{ asset('newadmin/footer/Chart.extension.js.download')}}"></script>
  <!--   Argon JS   -->
  <script src="{{ asset('newadmin/footer/argon-dashboard.min.js.download')}}"></script>
  <script src="{{ asset('newadmin/footer/t.js.download')}}"></script>
  <script>
    window.TrackJS &&
      TrackJS.install({
        token: "ee6fab19c5a04ac1a32a645abde4613a",
        application: "argon-dashboard-free"
      });
  </script>
  @yield('add_script')
