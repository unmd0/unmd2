<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="ناوبری را تغییر دهید">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      {{-- <a class="navbar-brand pt-0" href="file:///D:/new%208190/admin/index.html">
        <img src="./داشبورد آرگون - داشبورد رایگان برای بوت استرپ 4 توسط Creative Tim_files/blue.png" class="navbar-brand-img" alt="...">
      </a> --}}
      <!-- User -->
      {{-- <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link nav-link-icon" href="file:///D:/new%208190/admin/index.html#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
            <a class="dropdown-item" href="file:///D:/new%208190/admin/index.html#">اقدام اقدام </a>
            <a class="dropdown-item" href="file:///D:/new%208190/admin/index.html#">دیگر</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="file:///D:/new%208190/admin/index.html#">چیز دیگری در اینجا</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="file:///D:/new%208190/admin/index.html#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="مکان یاب تصویر" src="./داشبورد آرگون - داشبورد رایگان برای بوت استرپ 4 توسط Creative Tim_files/team-1-800x800.jpg">
              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">خوش آمدی!</h6>
            </div>
            <a href="file:///D:/new%208190/admin/examples/profile.html" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>پروفایل من</span>
            </a>
            <a href="file:///D:/new%208190/admin/examples/profile.html" class="dropdown-item">
              <i class="ni ni-settings-gear-65"></i>
              <span>تنظیمات</span>
            </a>
            <a href="file:///D:/new%208190/admin/examples/profile.html" class="dropdown-item">
              <i class="ni ni-calendar-grid-58"></i>
              <span>فعالیت</span>
            </a>
            <a href="file:///D:/new%208190/admin/examples/profile.html" class="dropdown-item">
              <i class="ni ni-support-16"></i>
              <span>پشتیبانی</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="file:///D:/new%208190/admin/index.html#!" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>خروج</span>
            </a>
          </div>
        </li>
      </ul> --}}
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="سایدوانا را تغییر دهید">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        {{-- <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="جستجو کردن" aria-label="جستجو کردن">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form> --}}
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item {{ Request::is('admin') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('admin') ? 'active' : '' }}" href="{{ url('admin/') }}">
              <i class="ni ni-tv-2 text-primary"></i> عملکرد
            </a>
          </li>
          <li class="nav-item {{ Request::is('admin/product/add') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('admin/product/add') ? 'active' : '' }}"  href="{{ url('admin/product/add') }}">
              <i class="ni ni-fat-add text-blue"></i>  درج محصول
            </a>
          </li>
          <li class="nav-item {{ Request::is('admin/product/show') ? 'active' : '' }}">
            <a class="nav-link  {{ Request::is('admin/product/show') ? 'active' : '' }}" href="{{ url('admin/product/show') }}">
              <i class="ni ni-ungroup text-orange"></i> مدیریت محصولات
            </a>
          </li>
          <li class="nav-item {{ Request::is('admin/type/show') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('admin/type/show') ? 'active' : '' }}" href="{{ url('admin/type/show') }}">
              <i class="ni ni-ungroup text-orange"></i> مدیریت تنوع ها
            </a>
          </li>
          <li class="nav-item {{ Request::is('admin/orders') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('admin/orders') ? 'active' : '' }}" href="{{ url('admin/orders') }}">
              <i class="ni ni-basket text-yellow"></i> مدیریت سفارشات
            </a>
          </li>
          <li class="nav-item {{ Request::is('/admin/category/') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('/admin/category/') ? 'active' : '' }}" href="/admin/category/0">
              <i class="ni ni-bullet-list-67 text-red"></i> مدیریت دسته بندی ها
            </a>
          </li>
          <li class="nav-item {{ Request::is('/admin/files') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('/admin/files') ? 'active' : '' }}" href="/admin/files">
              <i class="ni ni-folder-17 text-info"></i> مدیریت فایل ها
            </a>
          </li>
          <li class="nav-item {{ Request::is('/admin/pagebuilder') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('/admin/pagebuilder') ? 'active' : '' }}" href="/admin/pagebuilder">
              <i class="ni ni-settings-gear-65 text-pink"></i> تنظیمات پوسته
            </a>
          </li>
          <li class="nav-item {{ Request::is('/admin/pagebuilder/aboutus') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('/admin/pagebuilder/aboutus') ? 'active' : '' }}" href="/admin/pagebuilder/aboutus">
              <i class="ni ni-ui-04 text-pink"></i>تنظیمات صفحه درباره ما
            </a>
          </li>
        </ul>
        <!-- Divider -->
        <hr class="my-3">
        <!-- Heading -->
        {{-- <h6 class="navbar-heading text-muted">مستندات</h6> --}}
        <!-- Navigation -->
        <ul class="navbar-nav mb-md-3">
          <li class="nav-item {{ Request::is('admin/profile') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('admin/profile') ? 'active' : '' }}" href="{{ url('admin/profile') }}">
              <i class="ni ni-circle-08"></i> پروفایل
            </a>
          </li>
          <li class="nav-item {{ Request::is('admin/buy') ? 'active' : '' }}">
            <a class="nav-link {{ Request::is('admin/buy') ? 'active' : '' }}" style="    background-color: #d3c6ff85;    color: black;" href="{{ url('admin/buy') }}">
              <i class="ni ni-diamond" style="color: black;"></i> خرید بسته
            </a>
          </li>
        </ul>
        <ul class="navbar-nav">
          <li class="nav-item active active-pro">
            <form action="{{route('logout')}}" method="POST">
                @csrf
            <a class="nav-link" href="{{ route('logout') }}" onclick="this.closest('form').submit();return false;">
              <i class="ni ni-user-run text-dark"></i> خروج
            </a>
            </form>
          </li>
        </ul>
      </div>
    </div>
  </nav>
