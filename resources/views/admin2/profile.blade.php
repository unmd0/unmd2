@extends('admin2.master.index')
@section('main')
<div class="container-fluid mt--7">
    <div class="row">
      <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
        <div class="card card-profile shadow">
          <div class="row justify-content-center">
            <div class="col-lg-3 order-lg-2">
              <div class="card-profile-image">
                <a href="#">
                  <img src="../assets/img/theme/team-4-800x800.jpg" class="rounded-circle">
                </a>
              </div>
            </div>
          </div>
          <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
            <div class="d-flex justify-content-between">
              <a href="#" class="btn btn-sm btn-info mr-4">اتصال </a>
              <a href="#" class="btn btn-sm btn-default float-right">پیام</a>
            </div>
          </div>
          <div class="card-body pt-0 pt-md-4">
            <div class="row">
              <div class="col">
                <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                  <div>
                    <span class="heading">22 </span>
                    <span class="description">دوست</span>
                  </div>
                  <div>
                    <span class="heading">10 </span>
                    <span class="description">عکس</span>
                  </div>
                  <div>
                    <span class="heading">89 </span>
                    <span class="description">نظر</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="text-center">
              <h3>
                جسیکا جونز <span class="font-weight-light">، 27 ساله</span>
              </h3>
              <div class="h5 font-weight-300">
                <i class="ni location_pin mr-2"></i>بخارست ، رومانی
              </div>
              <div class="h5 mt-4">
                <i class="ni business_briefcase-24 mr-2"></i>مدیر راه حل - افسر تیم خلاق
              </div>
              <div>
                <i class="ni education_hat mr-2"></i>دانشگاه علوم کامپیوتر
              </div>
              <hr class="my-4">
              <p>رایان - نامی که نیک مورفی با نام ملبورن مطرح شده و در بروکلین مستقر کرده است - تمام موسیقی های خودش را می نویسد ، اجرا و ضبط می کند.</p>
              <a href="#">بیشتر نشان بده، اطلاعات بیشتر</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-8 order-xl-1">
        <div class="card bg-secondary shadow">
          <div class="card-header bg-white border-0">
            <div class="row align-items-center">
              <div class="col-8">
                <h3 class="mb-0">حساب من</h3>
              </div>
              <div class="col-4 text-right">
                <a href="#!" class="btn btn-sm btn-primary">تنظیمات</a>
              </div>
            </div>
          </div>
          <div class="card-body">
            <form>
              <h6 class="heading-small text-muted mb-4">اطلاعات کاربر</h6>
              <div class="pl-lg-4">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-username">نام کاربری</label>
                      <input type="text" id="input-username" class="form-control form-control-alternative" placeholder="نام کاربری" value="a.afshar">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">آدرس ایمیل</label>
                      <input type="email" id="input-email" class="form-control form-control-alternative" placeholder="info@mellatweb.com">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-first-name">نام کوچک</label>
                      <input type="text" id="input-first-name" class="form-control form-control-alternative" placeholder="نام کوچک" value="اکبر">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-last-name">نام خانوادگی</label>
                      <input type="text" id="input-last-name" class="form-control form-control-alternative" placeholder="نام خانوادگی" value="افشار">
                    </div>
                  </div>
                </div>
              </div>
              <hr class="my-4">
              <!-- Address -->
              <h6 class="heading-small text-muted mb-4">اطلاعات تماس</h6>
              <div class="pl-lg-4">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="input-address">نشانی</label>
                      <input id="input-address" class="form-control form-control-alternative" placeholder="آدرس خانه" value="آدرس و نشانی خود" type="text">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-city">شهر</label>
                      <input type="text" id="input-city" class="form-control form-control-alternative" placeholder="شهر" value="تبریز">
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-country">کشور</label>
                      <input type="text" id="input-country" class="form-control form-control-alternative" placeholder="کشور" value="ایران">
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label class="form-control-label" for="input-country">کد پستی</label>
                      <input type="number" id="input-postal-code" class="form-control form-control-alternative" placeholder="کد پستی">
                    </div>
                  </div>
                </div>
              </div>
              <hr class="my-4">
              <!-- Description -->
              <h6 class="heading-small text-muted mb-4">درمورد من</h6>
              <div class="pl-lg-4">
                <div class="form-group">
                  <label>درمورد من</label>
                  <textarea rows="4" class="form-control form-control-alternative" placeholder="چند کلمه در مورد شما ...">لورم ایپسوم یا طرح‌نما به متنی آزمایشی و بی‌معنی در صنعت چاپ، صفحه‌آرایی و طراحی گرافیک گفته می‌شود.</textarea>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <footer class="footer">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
          <div class="copyright text-center text-xl-left text-muted">
           <a href="https://www.mellatweb.com/" target="_blank"></a> ترجمه و بازبینی توسط : <a href="https://mellatweb.com/">ملت وب</a>
          </div>
        </div>
        <div class="col-xl-6">
          <ul class="nav nav-footer justify-content-center justify-content-xl-end">
            <li class="nav-item">
              <a href="#" class="nav-link" target="_blank">تیم خلاق</a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" target="_blank">درباره ما</a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" target="_blank">وبلاگ</a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link" target="_blank">مجوز MIT</a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  </div>
@endsection
