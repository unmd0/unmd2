
<main class="c-main c-main-padd">
    <div class="uk-container uk-container-large">
        <div class="alert alert-danger">تاییدیه موردنظر منقضی شده است. لطفا اقدام به ارسال مجدد ایمیل تاییدیه نمایید</div>
    </div>
</main>
