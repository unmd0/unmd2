<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOfileAndPfileToTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('big_s_products_type', function (Blueprint $table) {
            $table->string('ofile')->require()->default(" ");
            $table->string('pfile')->require()->default(" ");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('big_s_products_type', function (Blueprint $table) {
            $table->dropColumn('pfile');
            $table->dropColumn('ofile');
        });
    }
}
