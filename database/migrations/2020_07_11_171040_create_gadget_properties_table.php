<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGadgetPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gadget_properties', function (Blueprint $table) {
            $table->id();
            $table->text("name")->nullable();
            $table->text("category")->nullable();
            $table->text("unallowed_place")->nullable();
            $table->text("allowed_place")->nullable();
            $table->text("size")->nullable();
            $table->text("allowed_page")->nullable();
            $table->text("unallowed_page")->nullable();
            $table->text("main_file")->nullable();
            $table->text("description")->nullable();
            $table->text("pic")->nullable();
            $table->text("setting_file")->nullable();
            $table->text("ect")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gadget_properties');
    }
}
