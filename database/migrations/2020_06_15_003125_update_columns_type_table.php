<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            DB::statement('ALTER TABLE `orders` MODIFY `state` VARCHAR(255);');
            DB::statement('ALTER TABLE `orders` MODIFY `city` VARCHAR(255);');
            DB::statement('ALTER TABLE `orders` MODIFY `address` VARCHAR(255);');
            DB::statement('ALTER TABLE `orders` MODIFY `zipcode` VARCHAR(255);');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {

        });
    }
}
