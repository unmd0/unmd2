@include('setting')

    <div class="col-lg-12">
    <div style="
    text-align: {{$pmr->direction}};
    font-family: {{$pmr->font}};
    font-size: @if(isset($pmr->font_size)) {{$pmr->font_size."px"}} @else {{'45px'}} @endif;
    font-weight: @if(isset($pmr->weight)) {{$pmr->weight}} @else {{'bold'}} @endif;
    ">
            {{$pmr->sa}}
        </div>
    </div>
