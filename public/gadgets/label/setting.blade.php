@if(isset($pmr->weight)) @php $weight=$pmr->weight @endphp @else @php $weight="bold" @endphp @endif


<div class="row">
    <div class="col-lg-12">
        <form method="post" style="font-size:{{$pmr->font}}" >
            @csrf
            {{-- ***REQUIRE***  --}}
            {{-- pass gadget ID by gdgid --}}
            <input type="text" value="{{$get_gdgid}}" class="hide" name="gdgid">

            <div class="form-group">
              <label for="name">نوشته</label>
              {{-- pass setting value by [name of gadget] + [name of key] --}}
              <input type="text" name="label_sa" class="form-control" value="{{$pmr->sa}}">
          </div>

            <label for="name">چینش</label>
            <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="label_direction" id="exampleRadios1" value="center"  @if($pmr->direction=="center") checked @endif >
                <label class="form-check-label" for="exampleRadios1">
                  وسط چین
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="label_direction" id="exampleRadios2" value="right" @if($pmr->direction=="right") checked @endif>
                <label class="form-check-label" for="exampleRadios2">
                  راست چین
                </label>
              </div>
              <hr/>

              <label for="name">فونت</label>
              <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="label_font" id="fontfcheck" value="IRANYekan"  @if($pmr->font=="IRANYekan") checked @endif >
                <label class="form-check-label" for="fontfcheck" style="font-family: IRANYekan">
                  ایران یکان
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="label_font" id="fontfcheck2" value="kalameh" @if($pmr->font=="kalameh") checked @endif>
                <label class="form-check-label" for="fontfcheck2" style="font-family: kalameh">
                  کلمه
                </label>
              </div>
              <hr/>
              <label class="form-check-label" for="exampleRadios2">
                انداز فونت
              </label>
              <input type="number" name="label_font_size" class="form-control" value="@if(isset($pmr->font_size)){{$pmr->font_size}}@else{{'45'}}@endif">
              <hr/>
              <hr/>
              <label for="name">وزن فونت</label>
              <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="label_weight" id="fontfcheck" value="bold"  @if($weight=="bold") checked @endif >
                <label class="form-check-label" for="fontfcheck" style="font-weight: bold">
                  چاق
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="label_weight" id="fontfcheck2" value="100" @if($weight=="100") checked @endif>
                <label class="form-check-label" for="fontfcheck2" style="font-weight: 100">
                  لاغر
                </label>
              </div>
              <br/>








                <button class="btn btn-success" type="button" data-dismiss="modal" onclick="gdgupdate({{$get_gdgid}})">ذخیره و بستن</button>
        </form>
    </div>
</div>
