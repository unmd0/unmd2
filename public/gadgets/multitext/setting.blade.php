<div class="row">
    <div class="col-lg-12">
        <form method="post">
            @csrf
            {{-- ***REQUIRE***  --}}
            {{-- pass gadget ID by gdgid --}}
            <input type="text" value="{{$get_gdgid}}" class="hide" name="gdgid">




            <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="mtxt_direction" id="exampleRadios1" value="center"  @if($pmr->direction=="center") checked @endif >
                <label class="form-check-label" for="exampleRadios1">
                  وسط چین
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="mtxt_direction" id="exampleRadios2" value="right" @if($pmr->direction=="right") checked @endif>
                <label class="form-check-label" for="exampleRadios2">
                  راست چین
                </label>
              </div>
              <hr/>


                <div class="form-group">
                    <label for="name">نوشته</label>
                    {{-- pass setting value by [name of gadget] + [name of key] --}}
                    <textarea class="form-control" id="mtxtsa">{{$pmr->sa}}</textarea>
                    <input style="display: none" type="text" name="mtxt_sa" id="mtxtsa2">
                </div>





                <button class="btn btn-success" type="button" onclick="breakline()">ذخیره و بستن</button>
                <button style="display: none" id="mtxtlestsgo" type="button" data-dismiss="modal" onclick="gdgupdate({{$get_gdgid}})"></button>
        </form>
    </div>
</div>
<script>
    function breakline(){
      $("#mtxtsa2").val($("#mtxtsa").val().replace(/\r\n|\r|\n/g,'\\n'));
       $( "#mtxtlestsgo" ).click();
    }
</script>
