<link rel="stylesheet" href="{{ asset('gadgets/menutop/index.css') }}">
<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                @include('shop.master.layouts.menu.index')
            </div>
        </div>

    </div>

</header>
