@include('setting')
@php
if(!isset($site)){
 $site = Auth::user()->name ;
}
$categor_y= $pmr->category;
if($categor_y=="all")
$categor_y=DB::table('category')->where('shopname','=',$site)->first()->cname;
@endphp

{{-- <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css"> --}}
@if (DB::table('products')->where("show","=","0")->where('shopname','=',$site)->where('category','=',$categor_y)->first())
<hr/>
<h3 style="margin-right: 20px;margin-bottom: 7px;font-size: 1.7rem;font-family: 'nKalameh';color:#454545">{{$categor_y}}</h3>
<div class="swiper-productcategory">
    <div class="swiper-wrapper">
        @php
            if(!isset($site)){
            $site = Auth::user()->name ;
            }
            $products = App\product::where("show","=","0")->where("shopname","=",$site)->where("category","=",$categor_y)->orderBy('productID' , 'desc')->get();
        @endphp
        @foreach ($products as $product)
        @if (DB::table('big_s_products_type')->where("ProductId","=",$product->productID)->first()!="")
        {{-- <div class="swiper-slide"> --}}
        <div class="col-lg-3 col-md-4 col-sm-6 mix oranges swiper-slide">
            <a href="{{url()->current()}}/details/{{$product->productID}}">
            <div class="featured__item">

                <div class="featured__item__pic s-bg lazy"
                    data-bg="{{ URL::to('/') }}/img/<?php echo(explode("*",$product->pic)[1]); ?>.jpg"
                    data-bg-hidpi="{{ URL::to('/') }}/img/<?php echo(explode("*",$product->pic)[1]); ?>.jpg"
                    >
                </div>
                @php $isoff=0; @endphp
                @foreach (DB::table('big_s_products_type')->where("ProductId","=",$product->productID)->orderByRaw('LENGTH(offer)', 'ASC')->orderBy('offer' , 'asc')->get() as $prc)
                    @if ($prc->offer > 0)
                        @php
                        $pric = $prc->Price;
                        $off = $prc->offer;
                        $isoff=1;
                        @endphp
                @break
                    @endif
                @endforeach
                <div class="featured__item__text">
                <h6>{{$product->name}}</h6>
                <hr/>
                @if ($isoff == 0)
                <h5 class="normal_price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(DB::table('big_s_products_type')->where("ProductId","=",$product->productID)->orderByRaw('LENGTH(Price)', 'ASC')->orderBy('Price' , 'asc')->first()->Price,3)}} <span>تومان</span></h5>
                @else
                <h5 style="color: #000;text-decoration: line-through;color: #6d757b;text-decoration: line-through;padding-bottom: 10px;font-size: 12px;">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($pric,3)}} <span>تومان</span></h5>
                <h5 class="offer_price">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($off,3)}} <span style="color: #c55151;">تومان</span></h5>
                @endif

                </div>

            </div>
        </a>
        </div>
    {{-- </div> --}}
    @endif
    @endforeach



    </div>
</div>
@endif


{{-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> --}}

<!-- Initialize Swiper -->
<script>
    var swiper = new Swiper('.swiper-productcategory', {
  slidesPerView: "1.3",
  spaceBetween: 10,
  freeMode: true,
  pagination: {
    clickable: true,
  },
});
</script>
