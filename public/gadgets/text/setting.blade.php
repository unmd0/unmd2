<div class="row">
    <div class="col-lg-12">
        <form method="post">
            @csrf
            {{-- ***REQUIRE***  --}}
            {{-- pass gadget ID by gdgid --}}
            <input type="text" value="{{$get_gdgid}}" class="hide" name="gdgid">




            <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="txt_direction" id="exampleRadios1" value="center"  @if($pmr->direction=="center") checked @endif >
                <label class="form-check-label" for="exampleRadios1">
                  وسط چین
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input-rtl" type="radio" name="txt_direction" id="exampleRadios2" value="right" @if($pmr->direction=="right") checked @endif>
                <label class="form-check-label" for="exampleRadios2">
                  راست چین
                </label>
              </div>
              <hr/>



                <div class="form-group">
                    <label for="name">نوشته</label>
                    {{-- pass setting value by [name of gadget] + [name of key] --}}
                    <input type="text" name="txt_sa" class="form-control" value="{{$pmr->sa}}">
                </div>





                <button class="btn btn-success" type="button" data-dismiss="modal" onclick="gdgupdate({{$get_gdgid}})">ذخیره و بستن</button>
        </form>
    </div>
</div>
