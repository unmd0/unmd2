


<form method="post">
    @csrf
    {{-- ***REQUIRE***  --}}
    {{-- pass gadget ID by gdgid --}}
    <input type="text" value="{{$get_gdgid}}" class="hide" name="gdgid">

    <div class="alert alert-warning">سایز صحیح: 1170px X 659px</div>
    <button type="button" class="btn btn-info" onclick="showpopup(0,'prvfile','previewname')">انتخاب عکس</button>
    <div class="form-group">
        <output id="list" style="width: 100%">
        @php $count=0; @endphp
    @foreach (explode(",",$pmr->address) as $item)
        @if($item <> "")
        @php $count+=1; @endphp
                <div class="gdg_div" style="background-color: white" id="album1_div--{{$count}}" data-id="-{{$count}}">
                    <section id="album1_n-{{$count}}">
                    <div style="">
                        <div style="height: auto;text-align: center;margin: 5px 0px 5px 0px;display: flow-root;">
                            <span class="btn btn-danger btn-setting" onclick="piccart_close(this,'-{{$count}}')" style="float: left"><span class="fa fa-times"></span></span>
                            <span class="btn btn-info btn-setting" style="float: left;cursor: all-scroll;" id="movepics"><span class="fa fa-arrows"></span></span>
                    </div>
                    <hr/>
                    <img class="thum" src="{{$item}}" title="" style="display: block;margin: auto;width: 100%;height: auto;margin-bottom: 15px;"/>
                    
                    </div>
                    </section>
                
                </div>
        @endif
    @endforeach
        </output>
    </div>
    <input type="text" id="addresshere" value="{{$pmr->address}}" name="album1_address" class="hide" name="gdgid">


    <button class="btn btn-success" type="button" data-dismiss="modal" onclick="gdgupdate({{$get_gdgid}})">ذخیره و بستن</button>
</form>





<script>
    !function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);
</script>

    <script>
        function hidepopup(){
        $('#popup1').hide();
        }
        function showpopup(){
        $('#popup1').show();
        }
        function selectme8(x,y,z,a){
        var myJsonData = { "_token": "{{ csrf_token() }}", idd: y}
       $.post('/admin/gadget/managepicture', myJsonData, function(response) {
           $("#list").append(response);
           result = "";
           $('#list').find("img").each(function(){
                result += $(this).attr("src") + ",";
            });
            // result = result.replace(/,\s*$/, "");
             $("#addresshere").val(result);
        });
        $('#popup1').hide();

            



}
    </script>

    


<script>
    $( "#list" ).sortable({
    handle: '#movepics',
    connectWith: "#list",
    stop: function(event, ui) {
        $('#list').each(function() {
            result = "";

            $(this).find("img").each(function(){
                result += $(this).attr("src") + ",";
            });
            // result = result.replace(/,\s*$/, "");
            //  alert(result);
             $("#addresshere").val(result);
        });
    }
});
$("#list").disableSelection();
</script>
<script>
    function piccart_close(x,y){
    $('#album1_div-'+y).remove();
    result = "";
           $('#list').find("img").each(function(){
                result += $(this).attr("src") + ",";
            });
            // result = result.replace(/,\s*$/, "");
             $("#addresshere").val(result);
    }
    
</script>