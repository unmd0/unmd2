@include('setting')
@php
$number = str_replace(['+', '-'], '', filter_var($pmr->address, FILTER_SANITIZE_NUMBER_INT));
@endphp




<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <link rel="stylesheet" href="{{ asset('gadgets/album1/index.css?ver=1.4') }}">
    <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>



<div class="swp">
<div class="swiper-container swi{{$number}}" >
  <div class="swiper-wrapper">
@if ($pmr->address != "")
  @foreach (explode(',', $pmr->address) as $addr)
    @if($addr <> "")
      <div class="swiper-slide swiper-slide-album"><img src="{{$addr}}" alt=""></div>
    @endif
  @endforeach
@else
<div class="swiper-slide swiper-slide-album"><img src="{{asset('gadgets/album1/sample1.jpg')}}" alt=""></div>
<div class="swiper-slide swiper-slide-album"><img src="{{asset('gadgets/album1/sample2.jpg')}}" alt=""></div>
@endif
  </div>
  <!-- Add Arrows -->
  <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>
</div>
</div>
<script>
  var swiper = new Swiper('.swi{{$number}}', {
    loop: true,
    autoplay: {
    delay: 5000,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },


});
</script>
