
<div class="header___data">
    {{-- <ul>
        <li><a href="/cart"><i class="fal fa-search"></i></a></li>
    </ul> --}}
    <ul>
        <li><i class="fal fa-list" onclick="listshow(this)"></i></li>
    </ul>
    <ul>
        <li><a href="/"><i class="fal fa-home"></i></a></li>
    </ul>
    <ul>
        <li><a href="/cart"><i class="fal fa-shopping-bag" id="bagi"></i><span id="cartcount">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::count())}}</span></a></li>
    </ul>
    @php
        $logo1 = DB::table('profiles')->where('userid','=',$site)->first()->logo;
    @endphp
    @if ($logo1 != "" && $logo1 != "undefined")
    <a href="/"><img src="/img/{{DB::table('profiles')->where('userid','=',$site)->first()->logo}}.png" alt=""></a>
    @endif

</div>

<div id="macmenu-data">
    <ul class="macmenumenu">
    @foreach (DB::table('category')->where('shopname','=',$site)->get() as $item)
    @if (DB::table('products')->where('shopname','=',$site)->where('category','=',$item->cname)->first())
    <a href="{{url("/ctg/".$item->id)}}"><li>{{$item->cname}}</li></a>
    @endif

    @endforeach
</ul>




</div>
<div class="humberger__open">
    <i class="fas fa-bars"></i>
</div>
</div>
<script>
    function listshow(x){
        $( ".header" ).toggleClass( "showdata" );
        $( x ).toggleClass( "toclose" );
    }
</script>


























{{-- TODO cart on macmenu --}}


{{-- @php
    $carts=Gloudemans\Shoppingcart\Facades\Cart::content();
@endphp --}}




   {{-- <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shoping__cart__table">
                    <div id="emptycart" class="hide">
                        <div class="col-lg-12">
                            <div class="section-title">
                                <h3>سبد کالا خالی میباشد</h3>
                            </div>
                            <div class="section-title">
                                <a href="/"><h5 style="color: #538ce0 !important;">محصولات</h5></a>
                            </div>
                        </div>
                    </div>
                    <table id="fillcart" class="hide">
                        <thead>
                            <tr>
                                <th class="shoping__product">محصول</th>
                                <th>قیمت</th>
                                <th>تعداد</th>
                                <th>قیمت کل</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                                @foreach ($carts as $item)

                                <tr>
                                <td class="shoping__cart__item">
                                    <img src="{{$item->options->pic}}" alt=""><br class="br" style="display: none;"><br class="br" style="display: none;">
                                    <h5>{{$item->name}} - {{$item->options->type->Name}}</h5>
                                </td>
                                <td class="shoping__cart__price" style="text-align: bottom;">
                                    {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->price,3)}}
                                </td>
                                @if ($item->options->type->ofile == 0)
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                        <div class="pro-qty" data-id="{{$item->rowId}}">
                                                <span class="dec qtybtn" >-</span>
                                                <input type="text" value="{{$item->qty}}">
                                                <span class="inc qtybtn">+</span>
                                            </div>
                                        </div>
                                    </td>
                                @else
                                <td><span style="background-color: whitesmoke;padding: 2px 15px;">{{$item->qty}}</span></td>
                                @endif

                                <td class="shoping__cart__total" id="{{$item->rowId}}">
                                    {{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers($item->subtotal,3)}}
                                </td>
                                <td class="shoping__cart__item__close" onclick="deletecart(this,'{{$item->rowId}}')">
                                    <span class="icon_close"></span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6">
                <div class="shoping__checkout hide" id="checkoutf">
                    <h5>سبد کالا</h5>
                    <ul>
                        <li>قیمت مجموع <span class="price" id="subtotal">{{MPCO\EnglishPersianNumber\Numbers::toPersianNumbers(Cart::subtotal(0,0,""),3)}}</span><span>تومان</span></li>

                    </ul>
                    <a href="/checkout" class="primary-btn">رفتن به مرحله بعد</a>
                </div>
            </div>
        </div>
    </div>

<script>
    $( document ).ready(function() {

     emptycart({{Cart::count()}});

});
</script>
 --}}
