
    function selectme2(x) {
        var imj = document.querySelectorAll(".picture_preview");
        imj.forEach(element => {
            element.classList.remove("select-main-pic")
        });
        x.parentElement.classList.add("select-main-pic");
    }
    function deleteme1(x) {
        x.parentElement.remove();
        if (x.parentElement.classList.contains("select-main-pic")) {
            try {
                var imj = document.querySelectorAll(".picture_preview")[0];
                imj.classList.add("select-main-pic");
              }
              catch(err) {

              }

        }
    }
    function photo_select() {
        document.getElementById("btn_photo").click();
    }
    function handleFileSelect(evt) {

        var ido = true;
        var files = evt.target.files;

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            var filesize = ((f.size/1024)/1024).toFixed(4);
            // Only process image files.
              if (!f.type.match('image/jpeg') || filesize >= 3) {
                alert("فرمت فایل باید .jpg و اندازه فایل زیر 3 مگابایت باشد");
                continue;
              }
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML =
                        [
                            '<img class="thum" src="',
                            e.target.result,
                            '" title="', escape(theFile.name),
                            '"/><button type="button" class="btn btn-red btn-med" onclick="deleteme1(this)">حذف</button> <button type="button" class="btn btn-green btn-med" onclick="selectme2(this)">عکس اصلی</button>'
                        ].join('');
                    span.classList.add("picture_preview");
                    span.classList.add("c-grid__col");
                    span.classList.add("width-med");
                    document.getElementById('list').insertBefore(span, null);
                    var imj2 = document.querySelectorAll(".picture_preview");
                    imj2.forEach(element => {
                        if (element.classList.contains("select-main-pic")) {
                            ido = false;
                        }
                    });
                    if (ido == true) {
                        var imj = document.querySelectorAll(".picture_preview")[0];
                        imj.classList.add("select-main-pic");
                    }

                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }
    window.onload=function(){
        document.getElementById('btn_photo').addEventListener('change', handleFileSelect, false);
      }
   



