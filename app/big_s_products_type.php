<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class big_s_products_type extends Model
{
    protected $table = 'big_s_products_type';
    protected $fillable = ['ProductId' , 'Name' , 'Price' , 'Avaliable', "show"];
}
