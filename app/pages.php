<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pages extends Model
{
    protected $fillable = ['URL' , 'shopname' , 'content' , 'name' , 'nickname'];
}
