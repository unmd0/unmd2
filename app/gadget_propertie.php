<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gadget_propertie extends Model
{
    protected $fillable = ['name' , 'category' , 'main_file' , 'setting_file' , 'description' , 'pic'];
}
