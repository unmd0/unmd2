<?php

namespace App\Http\Controllers;

use App\filemanage;
use App\gadget_propertie;
use App\Http\Requests\OrderRequest;
use App\Mail\mailfile;
use App\order;
use App\orderproduct;
use App\pages;
use App\product;
use App\profile;
use App\sitelog;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Zarinpal\Zarinpal;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;
use Morilog\Jalali\Jalalian;
use SoapClient;

class StoreController extends Controller
{
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------product Controller-------\\
    //|_______|\\
    //|_______|\\
    //|_______|\\
    private $shopname0;
    public function get_shopname(){
        $this->shopname0 = explode('.', Request()->server('HTTP_HOST'))[0];
        return $this;
    }
    public function logger($details){

        $viewCounter = Session::get('visitCount', []);
        // dd( $viewCounter);
       if(!in_array($details, $viewCounter)){
        $agent = new Agent();
        $device = $agent->device();
        $platform = $agent->platform();
        $webkit = Request()->server('HTTP_USER_AGENT');
        $version = $agent->version($platform);
        sitelog::create([
            'type' => 'visitor',
            'connect' => explode('.', Request()->server('HTTP_HOST'))[0],
            'details' => $details. "&ip=". Request()->ip() . "&platform=" . $platform . "-ver " . $version . "&device=" . $device . "&webkit=". $webkit
        ]);
            Session::push('visitCount', $details);
        }/*End of hit counter*/

    }
    public function have_file($x){
        foreach ($x as $y){
            if($y->ofile != 0){return 1;}
        }
        return 0;
    }
    public function home(){
        $products = product::orderBy('productID' , 'desc')->get();
        // return view('shop.index',['products'=>$products]);
        $site=$this->get_shopname()->shopname0;
        $products = product::where("show","=","0")->where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","index")->first();
        $gadgets = gadget_propertie::all();
        return view('admin.become.preview',['products'=>$products,'site'=>$site,"page"=>$pageinfo, "gadgets"=>$gadgets]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|SHOW PRODUCT|\\
    public function details($id){
        $products = DB::table('products')->where("show","=","0")->where("productID","=",$id)->first();
        if(DB::table('big_s_products_type')->where("ProductId","=",$id)->where("offer",">",0)->orderByRaw('LENGTH(offer)', 'ASC')->orderBy('offer' , 'asc')->get()->count() > 0 )
            {$tp0 = DB::table('big_s_products_type')->where("ProductId","=",$id)->where("offer",">",0)->orderByRaw('LENGTH(offer)', 'ASC')->orderBy('offer' , 'asc')->first();}
        else
            {$tp0 = DB::table('big_s_products_type')->where("ProductId","=",$id)->orderByRaw('LENGTH(Price)', 'ASC')->orderBy('Price' , 'asc')->first();}
         $types = DB::table('big_s_products_type')->where("ProductId","=",$id)->orderByRaw('LENGTH(Price)', 'ASC')->orderBy('Price' , 'asc')->get();
         $this->logger('details='.$id);
        return view('shop.details',['products'=>$products,'types'=>$types, 'tp0'=>$tp0 ,'id'=>$id,'site'=>$this->get_shopname()->shopname0]);

    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------END product Controller-------\\






    //-------Cart Controller-------\\
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function addcart($id2,Request $request) {
        $id = $request->input('id');
        $tid = $request->input('tid');
        $name = $request->input('name');
        $pic = $request->input('pic');
        $type = DB::table('big_s_products_type')->where("TypeId","=",$tid)->first();
       $qty = $request->input('qty');
        $price = $request->input('price');
        $ofile = $request->input('ofile');
        if ($id==$id2){
        Cart::add($id, $name, $qty, $price,0, ["type" => $type, "pic" => $pic, "file" => $ofile]);
        }
        echo(Cart::count());
    }
    //|_______|\\
    //|CART-AJAX|\\
    //|UPDATE CART QUANTITY|\\
    public function updatecart(Request $request) {
        $id = $request->input('id');
       $qty = $request->input('qty');
        Cart::update($id, $qty);
        $back=[Cart::count(),Cart::get($id)->subtotal(0),Cart::subtotal(0)];
        echo(json_encode($back));
    }
    //|_______|\\
    //|CART-AJAX|\\
    //|DELETE PRODUCT FROM CART|\\
    public function deletecart(Request $request) {
        $id = $request->input('id');
        Cart::remove($id);
        $back=[Cart::count(),Cart::subtotal(0)];
        echo(json_encode($back));
    }
    //|_______|\\
    //|REDIRECT|\\
    //|SHOW CART|\\
    public function cart(){
        $carts=Cart::content();
        return view("shop.cart",['carts'=>$carts,'site'=>$this->get_shopname()->shopname0]);
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------END cart Controller-------\\








    //-------checkout Controller-------\\
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function checkout(){
        $carts=Cart::content();

        $ship = profile::where("userid","=",$this->get_shopname()->shopname0)->first()->ship_price;
        if ($ship == ""){
            $ship = "10000";
        }
        return view("shop.checkout",['carts'=>$carts,'site'=>$this->get_shopname()->shopname0,"ship" => $ship]);
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function checkoutstore(OrderRequest $request){
        $validate_data = $request->validated();
        $ship = profile::where("userid","=",$this->get_shopname()->shopname0)->first()->ship_price;
        if ($ship == ""){
            $ship = "10000";
        }
        $prc=Cart::subtotal(0,0,"")+$ship;
        $zarinpal = new Zarinpal("607aee45-f7fa-4283-a8f1-17c40769def1");
        // $zarinpal->enableSandbox();
        $results = $zarinpal->request(
            url('/checker'),       //required
            $prc,                                  //required
            "خرید از فروشگاه " . $this->get_shopname()->shopname0,      //required
            $request['email'],                   //optional
            $request['phone'],
        );

        // $zarinpal->setAmount($prc);
        // $zarinpal->setDescription("خرید از فروشگاه " . $this->get_shopname()->shopname0);
        // $zarinpal->setEmail( $request['email']);
        // $zarinpal->setMobile($request['phone']);
        // $zarinpal->setCallback(url('/checker'));
        // if (env("ZARIN_SAND", "") != ""){
        //     $zarinpal->enableSandbox();
        // }


        // $result  =  $zarinpal->request();



        $carts=Cart::content();
            $statu="در انتظار پرداخت";
        if($validate_data['mny']==0){
            $statu="پرداخت در محل";
        }
        $idgenerator = rand(0, 9) . Carbon::now()->milli . substr ($validate_data['phone'], -3) . rand(0, 9);
        order::create([
            'firstname' => $validate_data['firstname'],
            'lastname' => $validate_data['lastname'],
            'state' => $validate_data['state'],
            'city' => $validate_data['city'],
            'address' => $validate_data['address'],
            'zipcode' => $validate_data['zipcode'],
            'phone' => $validate_data['phone'],
            'email' => $validate_data['email'],
            'desc' => $validate_data['desc'],
            'status' => $statu,
            'id' => $idgenerator,
            'resnumber' => $results['Authority'],
            'price' => $prc,
            'shopname' => $this->get_shopname()->shopname0
        ]);
        foreach($carts as $item)
        {
            $n=$item->name;
            $t=$item->options->type->Name;
            $n= $n . "-" . $t;
            orderproduct::create([
                'orderid' => $idgenerator,
                'typeid' => $item->id,
                'name' =>   $n,
                'pic' => $item->options->pic,
                'priceeach' => number_Format($item->price),
                'qty' => $item->qty,
                'ofile' => $item->options->file,
                'subtotal' => $item->subtotal(0)
            ]);
        }





        if($validate_data['mny']==1){
            if (isset($results['Authority'])) {
                return redirect('https://www.zarinpal.com/pg/StartPay/'.$results['Authority']);
            } else{
                return view("We have an error");
            }
        }else{
            $order=orderproduct::where("orderid","=",$idgenerator)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
            $isfile=$this->have_file($order);
            Cart::destroy();
            return view('shop.checkoutresponse' , ['orderID'=>$idgenerator,'site'=>$this->get_shopname()->shopname0,'orders'=>$order,'havefile'=>$isfile,'res'=>"1"]);
        }





        //  return redirect()->route('trk',$idgenerator);

    }
    public function zarinpal(){
        $orderres=order::where("resnumber","=",request('Authority'))->firstOrFail();
        $MerchantID = '607aee45-f7fa-4283-a8f1-17c40769def1';
        $Amount = $orderres->price; //Amount will be based on Toman
        $Authority = request('Authority');
        $id=$orderres->id;
        $res="";
        $resmsg="";
        $order="";
        $isfile="0";
         if (request('Status') == 'OK') {
        //     if (env("ZARIN_SAND", "") != ""){
                // $client = new SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
            // }
        $client = new SoapClient('https://zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentVerification(
        [
        'MerchantID' => $MerchantID,
        'Authority' => $Authority,
        'Amount' => $Amount,
        ]
        );

        if ($result->Status == 100) {
            Cart::destroy();
            $res="1";
            $resmsg="پرداخت انجام شد";
            $order=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
            $isfile=$this->have_file($order);
            } else {
            $res="-1";
            $resmsg="پرداخت انجام نشد";
            }
            } else {
            $res="0";
            $resmsg="پرداخت لغو شد";
            }

            order::where("resnumber","=",request('Authority'))->update([
                'status' => $resmsg
            ]);

            return view('shop.checkoutresponse' , ['orderID'=>$id,'site'=>$this->get_shopname()->shopname0,'orders'=>$order,'havefile'=>$isfile,'res'=>$res]);
    }
    public function trk2(){
        return view('fd');
    }
    public function sendmailorde(Request $request){

         Mail::to($request->input('mail'))->send(new mailfile($request->input('orderID'),$request->input('site'),$request->input('havefile')));
    }
    public function getfile($file,$order){
        if (orderproduct::where('orderid', '=', $order)->where('ofile', '=', $file)->exists()) {
        $pth=filemanage::where('id','=',$file)->first();

        $file1= public_path(). $pth->address;

        return response()->download($file1, $pth->ect );
        }
        return "";
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    public function ordertrack(Request $request){

        $order="";
        $q="";
    if($request->q){
            $q=$request->q;
            $id = $request->q;
            $order=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
    }

        return view("shop.ordertracking",['order'=>$order,'q'=>$q,'site'=>$this->get_shopname()->shopname0]);
    }

    public function ordertrackid($id){

        $order="";
        $q="";
    if($id){
            $q=$id;
            $id = $id;
            $order=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
    }

        return view("shop.ordertracking",['order'=>$order,'q'=>$q,'site'=>$this->get_shopname()->shopname0]);
    }
    //-------End checkout Controller-------\\


    public function showmenu(){

        return view('shop.master.layouts.menu.xmenu');

    }
    public function showcategory($id){
        $site=$this->get_shopname()->shopname0;
        // $products = product::where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $ctg = DB::table('category')->where('shopname','=',$site)->where('id','=',$id)->first();
        $products = product::where("show","=","0")->where("shopname","=",$site)->where("category","=",$ctg->cname)->orderBy('productID' , 'desc')->get();
        return view('shop.category',['site'=>$site,'products'=>$products,'ctg' => $ctg]);
    }

    public function aboutus(){
        $products = product::orderBy('productID' , 'desc')->get();
        // return view('shop.index',['products'=>$products]);
        $site=$this->get_shopname()->shopname0;
        $products = product::where("show","=","0")->where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","aboutus")->first();
        $gadgets = gadget_propertie::all();
        return view('admin.become.aboutpreview',['products'=>$products,'site'=>$site,"page"=>$pageinfo, "gadgets"=>$gadgets]);
    }

}
