<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\sitelog;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Jenssegers\Agent\Agent;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function credentials(Request $request)
    {
      if(is_numeric($request->get('email'))){
        return ['phone'=>$request->get('email'),'password'=>$request->get('password')];
      }
    //   elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
        return ['email' => $request->get('email'), 'password'=>$request->get('password')];
    //   }
    //   return ['username' => $request->get('email'), 'password'=>$request->get('password')];
    }
 protected function authenticated(Request $request, $user)
{
    $agent = new Agent();
    $device = $agent->device();
    $platform = $agent->platform();
    $webkit = Request()->server('HTTP_USER_AGENT');
    $version = $agent->version($platform);
     sitelog::create([
         'type' => 'login',
         'connect' => $user->name,
         'details' => "ip=". Request()->ip() . "&platform=" . $platform . "-ver " . $version . "&device=" . $device . "&webkit=". $webkit
     ]);
}
}
