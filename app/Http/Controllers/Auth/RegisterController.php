<?php

namespace App\Http\Controllers\Auth;

use App\gadget_setting;
use App\Http\Controllers\Controller;
use App\pages;
use App\profile;
use App\Providers\RouteServiceProvider;
use App\sitelog;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Jenssegers\Agent\Agent;
use phplusir\smsir\Smsir;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::PROFILE;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:18','min:3', 'unique:users,name','string','regex:/^\S*$/u','regex:/(^([a-zA-Z]+)(\d+)?$)/u'],
            'email' => ['string', 'email', 'max:255', 'unique:users,email'],
            'phone' => ['required', 'regex:/(09)[0-9]{9}/', 'digits:11', 'numeric'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $cnt="";


        $ih=gadget_setting::create([
            'setting' => '{"address":""}',
            'shopname' => $data['name'],
            'gadget_id' => '4'
        ]);
        $cnt.= $ih->id . ",";


        $ih=gadget_setting::create([
            'setting' => '{"sa":"فروشگاه لباس","direction":"center","font":"kalameh","font_size":"55","weight":"bold"}',
            'shopname' => $data['name'],
            'gadget_id' => '3'
        ]);
        $cnt.= $ih->id . ",";


        $ih=gadget_setting::create([
            'setting' => '{"sa":"فروشگاه لباس پر از تنوع های شگفت انگیز!","direction":"center","font":"kalameh","font_size":"25","weight":"100"}',
            'shopname' => $data['name'],
            'gadget_id' => '3'
        ]);
        $cnt.= $ih->id . ",";


        $ih=gadget_setting::create([
            'setting' => '{"sa": "ds"}',
            'shopname' => $data['name'],
            'gadget_id' => '2'
        ]);
        $cnt.= $ih->id . ",";

        pages::create([
            'URL' => $data['name']. "." .request()->getHost(),
            'shopname' => $data['name'],
            'content' => $cnt,
            'name' => 'index',
            'nickname' => 'صفحه اصلی'
        ]);

        profile::create([
            'userid' => $data['name'],
            'expire' => Carbon::now()->addDays(37)->toDateString(),
        ]);
        $agent = new Agent();
        $device = $agent->device();
        $platform = $agent->platform();
        $webkit = Request()->server('HTTP_USER_AGENT');
        $version = $agent->version($platform);
        sitelog::create([
            'type' => 'register',
            'connect' => $data['name'],
            'details' => $data['phone'] . "&ip=". Request()->ip() . "&platform=" . $platform . "-ver " . $version . "&device=" . $device . "&webkit=". $webkit
        ]);

        $phone_code2 = rand(1423,9562);
        // Smsir::sendVerification($phone_code2,$data['phone']);
        Smsir::ultraFastSend(['VerificationCode'=>$phone_code2],32779,$data['phone']);
        return User::create([
            'name' => $data['name'],
            // 'email' => $data['email'],
            'phone' => $data['phone'],
            'phone_code' => $phone_code2,
            'password' => Hash::make($data['password']),
        ]);

    }
}
