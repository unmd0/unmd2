<?php

namespace App\Http\Controllers\admin;

use App\big_s_products_type;
use App\filemanage;
use App\gadget_propertie;
use App\gadget_setting;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProfileRequest;
use App\order;
use App\orderproduct;
use App\pages;
use App\product;
use App\profile;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Morilog\Jalali\CalendarUtils;
use Morilog\Jalali\Jalalian;
use SoapClient;
use Intervention\Image\ImageManagerStatic as Image;
use phplusir\smsir\Smsir;
use Zarinpal\Zarinpal;

class ProductController extends Controller
{
    public function createindexpage(){
        $cnt="";

        $ih=gadget_setting::create([
            'setting' => '{"address":""}',
            'shopname' => Auth::user()->name,
            'gadget_id' => '4'
        ]);
        $cnt.= $ih->id . ",";


        $ih=gadget_setting::create([
            'setting' => '{"sa":"فروشگاه لباس","direction":"center","font":"kalameh","font_size":"55","weight":"bold"}',
            'shopname' => Auth::user()->name,
            'gadget_id' => '3'
        ]);
        $cnt.= $ih->id . ",";


        $ih=gadget_setting::create([
            'setting' => '{"sa":"فروشگاه لباس پر از تنوع های شگفت انگیز!","direction":"center","font":"kalameh","font_size":"25","weight":"100"}',
            'shopname' => Auth::user()->name,
            'gadget_id' => '3'
        ]);
        $cnt.= $ih->id . ",";


        $ih=gadget_setting::create([
            'setting' => '{"sa": "ds"}',
            'shopname' => Auth::user()->name,
            'gadget_id' => '2'
        ]);
        $cnt.= $ih->id . ",";

        pages::create([
            'URL' => Auth::user()->name. "." .request()->getHost(),
            'shopname' => Auth::user()->name,
            'content' => $cnt,
            'name' => 'index',
            'nickname' => 'صفحه اصلی'
        ]);

    }

    public function createaboutuspage(){
        $cnt="";


        $ih=gadget_setting::create([
            'setting' => '{"sa":"درباره ما","direction":"center","font":"kalameh","font_size":"55","weight":"bold"}',
            'shopname' => Auth::user()->name,
            'gadget_id' => '3'
        ]);
        $cnt.= $ih->id . ",";


        $ih=gadget_setting::create([
            'setting' => '{"sa":"مارو بیشتر بشناسید.","direction":"center","font":"kalameh","font_size":"25","weight":"100"}',
            'shopname' => Auth::user()->name,
            'gadget_id' => '3'
        ]);
        $cnt.= $ih->id . ",";


        pages::create([
            'URL' => Auth::user()->name. "." .request()->getHost() . "/aboutus",
            'shopname' => Auth::user()->name,
            'content' => $cnt,
            'name' => 'aboutus',
            'nickname' => 'درباره ما'
        ]);

    }
    public function fakeuser($x){
        if (Auth::user()->name=="admin")
        {
            Auth::logout();
            Auth::loginUsingId($x);
        }
    }
    public static function expiredays(){
        $x=profile::where('userid',"=",Auth::user()->name)->first()->expire;
        $date = Carbon::parse($x);
        $now = Carbon::now();
        $diff = $date->diffInDays($now);
        return $diff;
    }
    public static function persian_date($x){
        $x=Carbon::parse($x);
        $persiandate = (CalendarUtils::toJalali( $x->format('Y'), $x->format('m'), $x->format('d')));
        return $persiandate[0] . '/' . $persiandate[1] . '/' . $persiandate[2];
    }
    public function home()
    {
        $articles = product::orderBy('id' , 'desc')->get();
        return view('index');
    }
    public function index(){
        // dd(Auth::user());
        return view('admin.index');
    }
    public function addproduct(){
        $category = DB::table('category')->where("shopname","=",Auth::user()->name)->get();
        // $autoc=product::all('name')->first();
        // $autoc=str_replace('},{"name":',",",$autoc);
        // $autoc=str_replace('{"name":',"",$autoc);
        // $autoc=str_replace(']',"",$autoc);
        // $autoc=str_replace('}',"",$autoc);
        // dd($autoc);
        $autoc=product::where("show","=","0")->where("shopname","=",Auth::user()->name)->get();
        return view('admin.product.add',['category'=>$category, 'autoc'=>$autoc]);
    }
    //-----------------------------\\
    //-----------------------------\\
    //-----product Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|STORE|\\
    //|SORE PRODUCT|\\
    public function create(ProductRequest $request)
    {
        $type=0;
        if($request->has('type')){$type=1;}
        $validate_data = $request->validated();

        product::create([
            'name' => $validate_data['name'],
            'pic' => $validate_data['pic'],
            'category' => $validate_data['category'],
            'keywords' => $validate_data['keywords'],
            'desc' => $validate_data['desc'],
            'type' => $type,
            'shopname' => Auth::user()->name
        ]);
        $lastid= DB::getPdo()->LastInsertId();
        if ($validate_data['link_type']){
            $tps=DB::table('big_s_products_type')->where("show","=","0")->where("shopname","=",Auth::user()->name)->where('ProductId',"=",$validate_data['link_type'])->get();
            foreach($tps as $tp){
                DB::table('big_s_products_type')->insert([
                    'name' => $tp->Name,
                    'Price' =>  $tp->Price ,
                    'offer' =>  $tp->offer,
                    'Avaliable' =>  $tp->Avaliable,
                    'ProductId' => $lastid,
                    'shopname' => Auth::user()->name
                ]);
            }
            return redirect('/admin/product/add');
        }

        return redirect('/admin/type/create/' . $lastid);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|SHOW PRODUCT|\\
    public function showproduct()
    {

        // dd($autoc);
        $alltypes=product::where("show","=","0")->where("shopname","=",Auth::user()->name)->paginate(10);
        return view('admin.product.show',["types"=>$alltypes]);
    }
    //|_______|\\
    //|SEARCH---$_GET|\\
    //|SEARCH ON PRODUCTS LIST|\\
    public function searchproduct()
    {
        $search1 =$_GET["search1"];
        $alltypes=product::where("show","=","0")->where("shopname","=",Auth::user()->name)->where("name","LIKE","%".$search1."%")->paginate(10);

    }
    //|_______|\\
    //|REDIRECT|\\
    //|PAGE OF EDIT PRODUCT|\\
    public function editproduct($ID)
    {
        $alltypes=product::where('productID',"=",$ID)->firstOrFail();
        $category = DB::table('category')->where("shopname","=",Auth::user()->name)->get();
        $autoc=product::where("show","=","0")->where("shopname","=",Auth::user()->name)->get();
        return view('admin.product.edit',['types'=>$alltypes,'category'=>$category, 'autoc'=>$autoc]);
    }
    //|_______|\\
    //|UPDATE|\\
    //|UPDATE PRODUCTS|\\
    public function updateproduct(ProductRequest $request)
    {
        $validate_data = $request->validated();

        product::where('productID',"=",$validate_data['productID'])->update([
            'name' => $validate_data['name'],
            'pic' => $validate_data['pic'],
            'category' => $validate_data['category'],
            'keywords' => $validate_data['keywords'],
            'desc' => $validate_data['desc']
        ]);
        $lastid= DB::getPdo()->LastInsertId();
        if ($validate_data['link_type']){
            $tps=DB::table('big_s_products_type')->where("show","=","0")->where("shopname","=",Auth::user()->name)->where('ProductId',"=",$validate_data['link_type'])->get();
            foreach($tps as $tp){
                DB::table('big_s_products_type')->insert([
                    'name' => $tp->Name,
                    'Price' =>  $tp->Price ,
                    'offer' =>  $tp->offer,
                    'Avaliable' =>  $tp->Avaliable,
                    'ProductId' => $lastid,
                    'shopname' => Auth::user()->name
                ]);
            }
            return redirect('/admin/product/add');
        }
        return redirect('/admin/product/show');
    }
    //|_______|\\
    //|UPDATE|\\
    //|DELETE PRODUCTS|\\
    public function deleteproduct($ID)
    {
        product::where("shopname","=",Auth::user()->name)->where('productID',"=",$ID)->update([
            'show' => "1"
        ]);
        DB::table('big_s_products_type')->where('ProductId' , "=", $ID)->update([
            'show' => "1"
        ]);
        return redirect('/admin/product/show');
    }
    //|_______|\\
    //|_______|\\
    //|_______|\\
    //-------END product Controller-------\\












    //-----------------------------\\
    //-----------------------------\\
    //-------Type Controller-------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|CREATE & EDIT TYPES|\\
    public function createtype($id)
    {
        $types=DB::table('big_s_products_type')->where("big_s_products_type.show","=","0")->where('ProductId',"=",$id)->get();
        $tp=product::where('productID',"=",$id)->first()->type;
        $list=filemanage::where("shopname","=",Auth::user()->name)->orderBy('created_at', 'DESC')->get();
        $listtype=1;
        return view('admin.types.add',["ID"=>$id,"typeAll"=>$types,'list'=>$list,'listtype'=>$listtype,"tp"=>$tp]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|LIST TYPES|\\
    public function showtype()
    {
        $alltypes=DB::table('big_s_products_type')->where("big_s_products_type.show","=","0")->where("big_s_products_type.shopname","=",Auth::user()->name)->join('products','products.productID','=','big_s_products_type.ProductId')->paginate(10);
        return view('admin.types.show',["types"=>$alltypes]);
    }
    //|_______|\\
    //|SEARCH---$_GET|\\
    //|SEARCH TYPE|\\
    public function searchtype()
    {
        $search1 =$_GET["search1"];
        $alltypes=DB::table('big_s_products_type')->where("big_s_products_type.show","=","0")->where("shopname","=",Auth::user()->name)->join('products','products.productID','=','big_s_products_type.ProductId')->where("big_s_products_type.Name","LIKE","%" . $search1 . "%")->orWhere("products.name","LIKE","%".$search1."%")->paginate(10);
        return view('admin.types.show',["types"=>$alltypes,"q"=>$search1]);
    }
    //|_______|\\
    //|STORE---Ajax--post|\\
    //|Store type|\\
    public function type_store(Request $req)
    {
        //if (!$req->has('offere')){$offer=0;}
        $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
        $english = [ 0 ,  1 ,  2 ,  3 ,  4 ,  4 ,  5 ,  5 ,  6 ,  6 ,  7 ,  8 ,  9 ];
        $price = str_replace($persian, $english, $req->input('pricee'));
        $offer =  str_replace($persian, $english, $req->input('offere'));
        DB::table('big_s_products_type')->insert([
        'name' => $req->input('namee'),
        'Price' =>$price ,
        'offer' => $offer,
        'Avaliable' => $req->input('ava'),
        'ProductId' => $req->input('pide'),
        'shopname' => Auth::user()->name
    ]);
    }
    //|__________________|\\
    //|STORE---Ajax--post|\\
    //|Store type________|\\
    public function type_edit(Request $req)
    {
        $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
        $english = [ 0 ,  1 ,  2 ,  3 ,  4 ,  4 ,  5 ,  5 ,  6 ,  6 ,  7 ,  8 ,  9 ];
        $price = $req->input('pricee');

        $price = str_replace($persian, $english, $req->input('pricee'));
        $offer =  str_replace($persian, $english, $req->input('offere'));
        DB::table('big_s_products_type')->where('TypeId' , "=", $req->input('pide'))->update([
        'name' => $req->input('namee'),
        'Price' => $price,
        'offer' => $offer,
        'Avaliable' => $req->input('ava')
    ]);
    }
    //|__________________|\\
    //|STORE---Ajax--post|\\
    //|Store type________|\\
    public function file_type_store(Request $req)
    {
        $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
        $english = [ 0 ,  1 ,  2 ,  3 ,  4 ,  4 ,  5 ,  5 ,  6 ,  6 ,  7 ,  8 ,  9 ];
        $price = str_replace($persian, $english, $req->input('pricee'));
        $res=DB::table('big_s_products_type')->insertGetId([
        'name' => $req->input('namee'),
        'Price' =>$price ,
        'Avaliable' => $req->input('ava'),
        'ProductId' => $req->input('pide'),
        'pfile' => $req->input('prvfilee'),
        'ofile' => $req->input('mainfilee'),
        'shopname' => Auth::user()->name
    ]);
    echo($res);
    }
    //|__________________|\\
    //|STORE---Ajax--post|\\
    //|Store type________|\\
    public function file_type_edit(Request $req)
    {
        $persian = ['۰', '۱', '۲', '۳', '۴', '٤', '۵', '٥', '٦', '۶', '۷', '۸', '۹'];
        $english = [ 0 ,  1 ,  2 ,  3 ,  4 ,  4 ,  5 ,  5 ,  6 ,  6 ,  7 ,  8 ,  9 ];
        $price = $req->input('pricee');
        $price = str_replace($persian, $english, $req->input('pricee'));
        DB::table('big_s_products_type')->where('TypeId' , "=", $req->input('pide'))->update([
        'name' => $req->input('namee'),
        'Price' => $price,
        'pfile' => $req->input('prvfilee'),
        'ofile' => $req->input('mainfilee'),
        'Avaliable' => $req->input('ava')
    ]);
    }
    //-----------------------------\\
    //-----------------------------\\
    //-----END Type Controller-----\\
    //-----------------------------\\
    //-----------------------------\\










    //-----------------------------\\
    //-----------------------------\\
    //-----Category Controller-----\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|CATEGORY SHOW & EDIT & UPDATE|\\
    public function category($id)
    {
        $category = DB::table('category')->where("shopname","=",Auth::user()->name)->where('pid',"=",$id)->get();
        $menuname = DB::table('category')->where("shopname","=",Auth::user()->name)->where('id',"=",$id)->get();
        return view('admin.category.index',['categorys'=>$category,'id'=>$id,'cname1'=>$menuname]);
    }
    //|_______|\\
    //|Ajax_POST|\\
    //|CATEGORY SHOW & EDIT & UPDATE|\\
    public function categoryadd(Request $req)
    {
        DB::table('category')->insert([
            'cname' => $req->input('namee'),
            'pid' => $req->input('pide'),
            'shopname' => Auth::user()->name
        ]);
    }
    //|_______|\\
    //|Ajax_POST|\\
    //|CATEGORY SHOW & EDIT & UPDATE|\\
    public function categoryedit(Request $req)
    {
        DB::table('category')->where("id","=",$req->input('pide'))->update([
            'cname' => $req->input('namee'),
        ]);
    }
    //|_______|\\
    //|Ajax_POST|\\
    //|CATEGORY SHOW & EDIT & UPDATE|\\
    public function categorydelete(Request $req)
    {
        DB::table('category')->where("id","=",$req->input('pide'))->delete();
    }
    //-----------------------------\\
    //-----------------------------\\
    //---END Category Controller---\\
    //-----------------------------\\
    //-----------------------------\\







    //-----------------------------\\
    //-----------------------------\\
    //------Orders Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|Orders SHOW|\\
    public function orders()
    {
        $alltypes=order::where("shopname","=",Auth::user()->name)->orderBy('created_at', 'DESC')->paginate(10);
        return view('admin.orders.show',["orders"=>$alltypes]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function ordersdetails($id)
    {
         if (order::where("id","=",$id)->first()->shopstatus == "0"){
            order::where("id","=",$id)->update([
                'shopstatus' => '1'
                ]);
         }

        $alltypes=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
        // $alltypes=DB::table('big_s_products_type')->join('products','products.productID','=','big_s_products_type.ProductId')->paginate(10);
        return view('admin.orders.details',["orders"=>$alltypes]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function ordership($id)
    {
        order::where("id","=",$id)->update([
            'shopstatus' => '2'
            ]);
        return redirect()->back()->with('message', 'تغییر وضعیت سفارش به آماده ارسال');
    }
    //-----------------------------\\
    //-----------------------------\\
    //---END Orders Controller---\\
    //-----------------------------\\
    //-----------------------------\\







    //-----------------------------\\
    //-----------------------------\\
    //------Profile Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|REDIRECT|\\
    //|Orders SHOW|\\
    public function profile()
    {

        $profile=profile::where('userid',"=",Auth::user()->name)->first();
        return view('admin.profile.add',['profile'=>$profile]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function profileadd($id)
    {
        $alltypes=orderproduct::where("orderid","=",$id)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);
        // $alltypes=DB::table('big_s_products_type')->join('products','products.productID','=','big_s_products_type.ProductId')->paginate(10);
        return view('admin.orders.details',["orders"=>$alltypes]);
    }
    //|_______|\\
    //|REDIRECT|\\
    //|Orders Details SHOW|\\
    public function updateprofile(ProfileRequest $request)
    {

        $validate_data = $request->validated();
        $cash_on_delivery = 0;
        if($request->has('cash_on_delivery')){ $cash_on_delivery = 1; }
        //
        profile::where('userid',"=",Auth::user()->name)->update([
            'name' => $validate_data['name'],
            'telegram' => $validate_data['telegram'],
            'instagram' => $validate_data['instagram'],
            'logo' => $validate_data['logo'],
            'address' => $validate_data['address'],
            'irshaba' => $validate_data['irshaba'],
            'cash_on_delivery' => $cash_on_delivery,
            'ship_price' => $validate_data['ship_price'],
            'validatepic' => $validate_data['validatepic']
        ]);
         return redirect()->back()->with('message', 'تغییرات با موفقیت انجام شد');
    }
    //-----------------------------\\
    //-----------------------------\\
    //---END Orders Controller---\\
    //-----------------------------\\
    //-----------------------------\\




    //-----------------------------\\
    //-----------------------------\\
    //------upload Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_______|\\
    //|POST|\\
    //|Upload photo .PNG|\\
    public function uploadpng(Request $request)
    {
        $image_64 = $request->input('base64image');

        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

        $replace = substr($image_64, 0, strpos($image_64, ',')+1);

      // find substring fro replace here eg: data:image/png;base64,

       $image = str_replace($replace, '', $image_64);

       $image = str_replace(' ', '+', $image);

       $imageName = "img/".$request->input('name').'.'."png";
    //    Image::make( base64_decode($image))->save($imageName,50);
         File::put($imageName, base64_decode($image));
    }
    //|_______|\\
    //|POST|\\
    //|Upload photo .JPG|\\
    public function uploadjpg(Request $request)
    {
        echo($request->input('base64image'));
         $image_64 = $request->input('base64image');

        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

        $replace = substr($image_64, 0, strpos($image_64, ',')+1);

      // find substring fro replace here eg: data:image/png;base64,

       $image = str_replace($replace, '', $image_64);

       $image = str_replace(' ', '+', $image);

       $imageName = "img/".$request->input('name').'.'."jpg";
       Image::make( base64_decode($image))->save($imageName,50);
        //  File::put($imageName, base64_decode($image));

    }
    //-----------------------------\\
    //-----------------------------\\
    //---END upload Controller---\\
    //-----------------------------\\
    //-----------------------------\\







    //-----------------------------\\
    //-----------------------------\\
    //------FILES Controller-------\\
    //-----------------------------\\
    //-----------------------------\\
    //|_________________|\\
    //|GET______________|\\
    //|manage files SHOW|\\
    public function files(Request $request){
        $list=filemanage::where("shopname","=",Auth::user()->name)->orderBy('created_at', 'DESC')->get();
        $listtype=0;
        return view('admin.files.mange',['list'=>$list,"listtype"=>$listtype]);
    }
    //|_________________|\\
    //|GET______________|\\
    //|upload files SHOW|\\
    public function filesupload(Request $request){
        return view('admin.files.upload');
    }
    //|_________________|\\
    //|POST_____________|\\
    //|DO upload files__|\\
    public function dofilesupload(Request $request){
        $image = $request->file('file');
        $ex=$image->getClientOriginalExtension();
        $milliseconds = round(microtime(true) * 1000);
        $imageName = $milliseconds.".".$ex;
        $title = $image->getClientOriginalName();

        if ($ex == "jpg" || $ex == "png" || $ex == "jpeg"){
        Image::make($image)->save(public_path('img/files').'/'.$imageName,50);
    }


        // $image->move(public_path('img/files'),$imageName);
        // $sz=$image->getSize();

        // ,"size" => $request->file('file')->getSize()
        $data = ['address' => "/img/files/".$imageName,"shopname" =>  Auth::user()->name,"extension" => $ex,"ect" =>  $title, 'size' => ""];
        return (json_encode($data));
        // $imageUpload = new filemanage();
        // $imageUpload->filename = $imageName;
        // $imageUpload->save();
        // return response()->json(['success'=>$imageName]);
    }
    public function storefilesupload(Request $request){
        $filelist=filemanage::create([
            'address' =>  $request->input('address'),
            'shopname' =>  $request->input('shopname'),
            'size' =>  $request->input('size'),
            'extension' =>  $request->input('extension'),
            'ect' =>  $request->input('ect')
        ]);
        $listtype=$request->input('listtype');
        //echo($filelist->id);
         return view ('admin.files.filesection',["filelist" => $filelist,"listtype" => $listtype]);
    }

    public function destroyfilesupload(Request $request){

        if( $request->input('filename')){
            $filename =  $request->input('filename');
            $file=filemanage::where('ect','=',$filename)->orderBy('id', 'DESC')->first();
            filemanage::where('ect','=',$filename)->orderBy('id', 'DESC')->first()->delete();
        }elseif($request->input('id')){
            $filename =  $request->input('id');
            $file=filemanage::where('id','=',$filename)->first();
            filemanage::where('id','=',$filename)->first()->delete();
        }
        $path=public_path().$file->address;
        if (file_exists($path)) {
            unlink($path);
        }
        return $file->address;
    }
    //-----------------------------\\
    //-----------------------------\\
    //----END FILES Controller-----\\
    //-----------------------------\\
    //-----------------------------\\



    //-----------------------------\\
    //-----------------------------\\
    //--------BUY Controller-------\\
    //-----------------------------\\
    //-----------------------------\\

    //|_________________|\\
    //|REDIRECT_________|\\
    //|Route To buy page|\\
    public function buy(){
        return view('admin.buy.master');
    }
    public function buytype($type){
        switch ($type) {
            case 1:
                $prc="69000";
                break;
            case 2:
                $prc="96000";
                break;
            case 3:
                $prc="169000";
                break;
            case 4:
                $prc="369000";
                break;
            default:
                return redirect()->back();
        }
        $zarinpal = new Zarinpal("607aee45-f7fa-4283-a8f1-17c40769def1");
        // $zarinpal->enableSandbox();
        $results = $zarinpal->request(
            url('/admin/purchase/' . $type),       //required
            $prc,                                  //required
            "خرید از بسته از پلتفرم 8190 ",      //required
            Auth::user()->email,                   //optional
        );
        // $zarinpal->setAmount($prc);
        // $zarinpal->setDescription("خرید از بسته از پلتفرم 8190 " );
        // $zarinpal->setEmail( Auth::user()->email);
        // // $zarinpal->setMobile($request['phone']);
        // $zarinpal->setCallback(url('/admin/purchase/' . $type));
        // $zarinpal->enableSandbox();
        if (isset($results['Authority'])) {
            return redirect('https://www.zarinpal.com/pg/StartPay/'.$results['Authority']);

        } else{
            return view("We have an error");
        }
    }
    public function purchasetype($type){
        switch ($type) {
            case 1:
                $prc="69000";
                $adddays=69;
                break;
            case 2:
                $prc="96000";
                $adddays=96;
                break;
            case 3:
                $prc="169000";
                $adddays=196;
                break;
            case 4:
                $prc="369000";
                $adddays=369;
                break;
            default:
                return redirect()->back();
        }
        $MerchantID = '607aee45-f7fa-4283-a8f1-17c40769def1';
        $Amount = $prc; //Amount will be based on Toman
        $Authority = request('Authority');
        if (request('Status') == 'OK') {

        //  $client = new SoapClient('https://sandbox.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $client = new SoapClient('https://zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);
        $result = $client->PaymentVerification(
        [
        'MerchantID' => $MerchantID,
        'Authority' => $Authority,
        'Amount' => $Amount,
        ]
        );

        if ($result->Status == 100) {

            $resmsg="پرداخت انجام شد";
            $profile=profile::where('userid',"=",Auth::user()->name)->first();
            if ($profile->permision >= $type){$type=$profile->permision;}
            $expire=Carbon::parse($profile->expire)->addDays($adddays)->toDateString();
            profile::where('userid',"=",Auth::user()->name)->update([
                'permision' => $type,
                'expire' => $expire
            ]);
            $alert="success";
            } else {

            $resmsg="پرداخت انجام نشد";
            $alert="danger";
            }
            } else {

            $resmsg="پرداخت لغو شد";
            $alert="danger";
            }



            return view('admin.buy.master',[ 'msg' => $resmsg,'alert' => $alert]);
    }
    //-----------------------------\\
    //-----------------------------\\
    //-----END BUY Controller------\\
    //-----------------------------\\
    //-----------------------------\\
    public function imgod($x){
        $this->fakeuser($x);
        return view('admin.index');
    }

    public function roles()
    {
        return view('admin.roles');
    }

    public function pagebuilder(){
        $site=Auth::user()->name;
        $products = product::where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","index")->first();
        if(!isset($pageinfo))
        {
            $this->createindexpage();
        }
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","index")->first();
        $gadgets = gadget_propertie::all();
        return view('admin.become.index',['products'=>$products,'site'=>$site,"page"=>$pageinfo, "gadgets"=>$gadgets,"pgname"=>"index"]);
    }

    public function aboutusbuilder(){
        $site=Auth::user()->name;
        $products = product::where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","aboutus")->first();
        if(!isset($pageinfo))
        {
            $this->createaboutuspage();
        }
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","aboutus")->first();
        $gadgets = gadget_propertie::all();
        return view('admin.become.aboutus',['products'=>$products,'site'=>$site,"page"=>$pageinfo, "gadgets"=>$gadgets,"pgname"=>"aboutus"]);
    }
    public function wlc(){
        $site=Auth::user()->name;
        return view("admin.welcome.index");
    }
    public function otp(){
        $site=Auth::user()->name;
        return view("admin.otp");
    }
    public function otp_checker(Request $req){
        $vercode=Auth::user()->phone_code;
        if (Auth::user()->phone_code == $req->input('phone_code') && strlen($vercode) == 4)
        {
            User::where("name","=",Auth::user()->name)->update([
                'phone_verified_at' => Carbon::now(),
                'email_verified_at' => Carbon::now(),
                'phone_code' => '0'
            ]);
            return redirect('admin/profile');
        }
        return view("admin.otp",['errorMessageDuration' => 'کد فعالسازی صحیح نیست']);
    }
    public function otpaddphone(Request $req){
        if (strlen($req->input('phone')) == 11)
        {
            $phone_code2 = rand(1423,9562);
            // Smsir::sendVerification($phone_code2,$req->input('phone'));
             Smsir::ultraFastSend(['VerificationCode'=>$phone_code2],32779,$req->input('phone'));
            User::where("name","=",Auth::user()->name)->update([
                'phone' => $req->input('phone'),
                'phone_code' => $phone_code2
            ]);
            return redirect('admin/otp');
        }
        return view("admin.otp",['errorMessageDuration' => 'شماره صحیح نیست']);
    }

    public function add_gadget(Request $req){


        $json = gadget_propertie::where("id","=",$req->input('idd'))->first()->ect;
        $parametr = json_decode($json);
        $address=gadget_propertie::where("id","=",$req->input('idd'))->first()->main_file;
        $default = gadget_propertie::where("id","=",$req->input('idd'))->first()->ect;




        $site=Auth::user()->name;
        $cntnew = pages::where("shopname","=",$site)->where("name","=",$req->input('pagee'))->first()->content;



        $id = DB::table('gadget_settings')->insertGetId([
            'setting' => $default,
            'gadget_id' => $req->input('idd'),
            'shopname' => $site
        ]);

        $cnt= $cntnew . $id . ",";

        pages::where("shopname","=",$site)->where("name","=",$req->input('pagee'))->update([
            'content' => $cnt
        ]);




        return view($address,["pmr" => $parametr,"global_gdgid" => $id]);




    }
    public function setting_gadget(Request $req){
        $gadget_id = gadget_setting::where("id","=",$req->input('idd'))->first()->gadget_id;
        $json = gadget_setting::where("id","=",$req->input('idd'))->first()->setting;
        $parametr = json_decode($json);
        $address = gadget_propertie::where("id","=",$gadget_id)->first()->setting_file;
        $get_gdgid = $req->input('idd');
        return view($address,["pmr" => $parametr,"get_gdgid" => $get_gdgid]);
    }
    public function update_setting_gadget(Request $req)
    {
        $gadget_id = gadget_setting::where("id","=",$req->input('gdgid'))->first()->gadget_id;

        $allreq = $req->all();

        $chr=gadget_propertie::where("id","=",$gadget_id)->first()->name . "_";

        $out="{";
        foreach($allreq as $key => $val) {
            if (strpos($key, $chr)  !== false ){
            $key=str_replace($chr,"",$key);
            $out .= '"' . $key . '":"' . $val . '",';
        }
        }
        $out=rtrim($out,", ");
        $out.="}";




        gadget_setting::where("id","=",$req->input('gdgid'))->update([
            'setting' => $out
        ]);


        $pg = gadget_setting::where("id","=",$req->input('gdgid'))->first()->id;
        $json = $out;
        $parametr = json_decode($json);
        $address = gadget_propertie::where("id","=",$gadget_id)->first()->main_file;
        return view($address,["pmr" => $parametr,"global_gdgid" => $pg]);

    }




    public function swap_setting_gadget(Request $req){
        $req->input('yy');
        $req->input('xx');
        $cnt=pages::where("shopname","=",Auth::user()->name)->where("name","=",$req->input('pagee'))->first()->content;
        $expld = explode(',', $cnt);
        $xx=$expld[$req->input('xx')-1];
        $yy=$expld[$req->input('yy')-1];

        $out="";
        foreach ($expld as $pg){
            if ($pg != ""){
            if ($pg==$xx) $pg=$yy;
            elseif ($pg==$yy) $pg=$xx;
            $out .= $pg . ",";
            }
        }
        echo($out);
        pages::where("shopname","=",Auth::user()->name)->where("name","=",$req->input('pagee'))->update([
            'content' => $out
        ]);




    }


    public function delete_setting_gadget(Request $req){
        $x = $req->input('xx');
        $cnt=pages::where("shopname","=",Auth::user()->name)->where("name","=",$req->input('pagee'))->first()->content;
        $expld = explode(',', $cnt);
        $out="";
        foreach ($expld as $pg){
            if ($pg != ""){
            if ($pg!=$x)
            {$out .= $pg . ",";}
            }
        }
        gadget_setting::where("id","=",$x)->delete();
        pages::where("shopname","=",Auth::user()->name)->where("name","=",$req->input('pagee'))->update([
            'content' => $out
        ]);
    }


    public function preview_setting_gadget(Request $req){
        $site=Auth::user()->name;
        $products = product::where("show","=","0")->where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","index")->first();
        $gadgets = gadget_propertie::all();
        return view('admin.become.preview',['products'=>$products,'site'=>$site,"page"=>$pageinfo, "gadgets"=>$gadgets]);
    }
    public function managepicture_setting_gadget(Request $req){
        $file=filemanage::where("id","=",$req->input("idd"))->where("shopname","=",Auth::user()->name)->firstOrFail();

        return view('admin.become.extention.piccard',['file'=>$file]);
    }




    public function previewaboutus_setting_gadget(Request $req){
        $site=Auth::user()->name;
        $products = product::where("show","=","0")->where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","aboutus")->first();
        $gadgets = gadget_propertie::all();
        return view('admin.become.preview',['products'=>$products,'site'=>$site,"page"=>$pageinfo, "gadgets"=>$gadgets]);
    }


    public function new_admin(){
        return view('admin2.master.index');
    }

    //-----------------------------\\
    //-----------------------------\\
    //------example Controller-----\\
    //-----------------------------\\
    //-----------------------------\\

    //-----------------------------\\
    //-----------------------------\\
    //---END EXAMPLE Controller----\\
    //-----------------------------\\
    //-----------------------------\\


}
