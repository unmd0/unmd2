<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $default=[
            'firstname' => 'required|max:50',
            'mny' => 'required|max:50',
            'lastname' => 'required|max:50',
            'phone' => 'required|digits_between:10,11|numeric',
            'email' => 'email:rfc,dns|max:70|nullable',
            'desc' => 'max:255'
        ];
        $addr=[
            'state' => 'required_if:ofile,1|max:50',
            'city' => 'required_if:ofile,1|max:50',
            'address' => 'required_if:ofile,1|max:120',
            'zipcode' => 'required_if:ofile,1|max:50'

        ];
        // if($this->attributes->get('ofile') == 0){
        //     $addr=[
        //         'state' => 'max:50',
        //         'city' => 'max:50',
        //         'address' => 'max:120',
        //         'zipcode' => 'max:50'
        //           ];
        // }

        return array_merge($default, $addr);
    }
    public function messages()
{
    return [
        'state.required_if:ofile,1' => 'تکمیل گزینه لستان الزامی است',
        'city.required_if:ofile,1'  => 'تکمیل گزینه شهر الزامی است',
        'address.required_if:ofile,1'  => 'تکمیل گزینه آدرس الزامی است',
        'zipcode.required_if:ofile,1'  => 'تکمیل گزینه کدپستی الزامی است',
    ];
}
}
