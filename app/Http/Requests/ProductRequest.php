<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link_type' => 'nullable|numeric',
            'name' => 'required|max:50',
            'pic' => 'required',
            'productID' => 'max:10',
            'category' => 'required',
            'keywords' => 'max:100',
            'type' => 'max:2',
            'desc' => 'max:600'
        ];
    }
}
