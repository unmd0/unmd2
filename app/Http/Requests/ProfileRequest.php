<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'present|Nullable|max:50',
            'logo' => 'present|Nullable|max:150',
            'instagram' => 'present|Nullable|max:50',
            'telegram' => 'present|Nullable|max:50',
            'address' => 'present|Nullable|max:250',
            'ship_price' => 'present|Nullable|numeric',
            'irshaba' => 'present|Nullable|digits:24',
            'cash_on_delivery' => 'Nullable',
            'validatepic' => 'present|Nullable|max:150'
        ];
    }
}
