<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $fillable = [ 'userid' , 'name' , 'logo' , 'instagram' , 'telegram' , 'phone' , "address", "credit", "irshaba", "zarinpalurl", 'validatepic', 'expire', 'ship_price', 'cash_on_delivery'];
}
