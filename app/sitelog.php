<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sitelog extends Model
{
    protected $fillable = [
        'type', 'connect', 'details',
    ];
}
