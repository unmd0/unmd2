<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $fillable = ['name' , 'pic' , 'desc' , 'keywords' , 'category' , "productID","shopname","type", "show"];
}
