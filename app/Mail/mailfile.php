<?php

namespace App\Mail;

use App\orderproduct;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class mailfile extends Mailable
{
    use Queueable, SerializesModels;
    public $orderID;
    public $site;
    public $orders;
    public $havefile;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderID,$site,$havefile)
    {
        $this->orderID=$orderID;
        $this->site=$site;
        $this->orders=orderproduct::where("orderid","=",$orderID)->join('orders','orders.id','=','orderproducts.orderid')->paginate(10);;
        $this->havefile=$havefile;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.sendfile')
                    ->subject('سفارش شما ثبت شد.')
                    ->from(env("MAIL_FROM_ADDRESS", "noreply@8190.org"),$this->site);
    }
}
