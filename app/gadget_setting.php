<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gadget_setting extends Model
{
    protected $fillable = ['setting' , 'shopname' , 'gadget_id'];
}
