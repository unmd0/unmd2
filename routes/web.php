<?php

use App\filemanage;
use App\gadget_propertie;
use App\pages;
use App\product;
use App\sitelog;
use App\User;
use Gloudemans\Shoppingcart\Facades\Cart;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request ;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use App\Http\Middleware\phone_valid;
//
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/addcart{id?}{name?}{qty?}{price?}{_?}', function ($id,$name,$qty,$price) {
//     Cart::add($id, $name, $qty, $price);
//     return Cart::count();
// });

// Route::get('register{email?}', function ($id) {
//     return dd($id);
// });

Route::get('/',function(){
    $server = explode('.', Request()->server('HTTP_HOST'));
    $agent = new Agent();
    $device = $agent->device();
    $platform = $agent->platform();
    $webkit = Request()->server('HTTP_USER_AGENT');
    $version = $agent->version($platform);
    if (User::where('name', '=', $server[0])->exists()){
        $viewCounter = Session::get('visitCount', []);

       if(!in_array('home', $viewCounter)){
        sitelog::create([
            'type' => 'visitor',
            'connect' => explode('.', Request()->server('HTTP_HOST'))[0],
            'details' => "home&ip=". Request()->ip() . "&platform=" . $platform . "-ver " . $version . "&device=" . $device . "&webkit=". $webkit
        ]);
            Session::push('visitCount', 'home');
        }

        // $products = product::where("shopname","=",$server[0])->orderBy('productID' , 'desc')->get();
        // return view('shop.index',['products'=>$products,'site'=>$server[0]]);
        $site=$server[0];
        $products = product::where("shopname","=",$site)->orderBy('productID' , 'desc')->get();
        $pageinfo = pages::where("shopname","=",$site)->where("name","=","index")->first();
        $gadgets = gadget_propertie::all();
        if($gadgets != "" && $pageinfo != ""){
        return view('admin.become.preview',['products'=>$products,'site'=>$site,"page"=>$pageinfo, "gadgets"=>$gadgets]);}
        return view('shop.index',['products'=>$products,'site'=>$server[0]]);
    }
    // $detect = new Mobile_Detect;

    $viewCounter = Session::get('visitCount', []);
    if(!in_array('home', $viewCounter)){
    sitelog::create([
        'type' => 'visitor',
        'connect' => Request()->server('HTTP_HOST'),
        'details' => "home&ip=". Request()->ip() . "&platform=" . $platform . "-ver " . $version . "&device=" . $device . "&webkit=". $webkit
    ]);
        Session::push('visitCount', 'home');
    }/*End of hit counter*/

    return view('index.index');
});
Auth::routes(['verify' => true]);


Route::prefix('admin')->middleware('auth')->group( function () {
        Route::get('/', 'admin\ProductController@index')->name("adminpage")->middleware(phone_valid::class);

        //PRODUCTS Routes
        Route::get('/product/add', 'admin\ProductController@addproduct')->name("addproduct")->middleware(phone_valid::class);
        Route::put('/product/create', 'admin\ProductController@create')->name("p_create")->middleware("verified")->middleware(phone_valid::class);
        Route::get('/product/show', 'admin\ProductController@showproduct')->middleware(phone_valid::class);
        Route::get('/product/search{search1?}', 'admin\ProductController@searchproduct')->middleware(phone_valid::class);
        Route::get('/product/edit/{ID}', 'admin\ProductController@editproduct')->middleware(phone_valid::class);
        Route::put('/product/edit', 'admin\ProductController@updateproduct')->middleware("verified")->middleware(phone_valid::class);
        Route::get('/product/delete/{ID}', 'admin\ProductController@deleteproduct')->middleware("verified")->middleware(phone_valid::class);

        //TYPES Routes
        Route::get('/type/create/{ID}', 'admin\ProductController@createtype')->middleware(phone_valid::class);
        Route::get('/type/show', 'admin\ProductController@showtype')->middleware(phone_valid::class);
        Route::get('/type/search{search1?}', 'admin\ProductController@searchtype')->middleware(phone_valid::class);
        Route::post('/type/store', 'admin\ProductController@type_store')->middleware("verified")->middleware(phone_valid::class);
        Route::post('/filetype/store', 'admin\ProductController@file_type_store')->middleware("verified")->middleware(phone_valid::class);
        Route::post('/type/edit', 'admin\ProductController@type_edit')->middleware("verified")->middleware(phone_valid::class);
        Route::post('/filetype/edit', 'admin\ProductController@file_type_edit')->middleware("verified")->middleware(phone_valid::class);

        //CATEGORYS Routes
        Route::get('/category/{id}', 'admin\ProductController@category')->middleware(phone_valid::class);
        Route::post('/category/add', 'admin\ProductController@categoryadd')->middleware("verified")->middleware(phone_valid::class);
        Route::post('/category/edit', 'admin\ProductController@categoryedit')->middleware("verified")->middleware(phone_valid::class);
        Route::post('/category/delete', 'admin\ProductController@categorydelete')->middleware("verified")->middleware(phone_valid::class);

        //ORDERS Routes
        Route::get('/orders/', 'admin\ProductController@orders')->middleware(phone_valid::class);
        Route::get('/orders/{id}', 'admin\ProductController@ordersdetails')->middleware(phone_valid::class);
        Route::get('/orders/ship/{id}', 'admin\ProductController@ordership')->middleware(phone_valid::class);

        //PRODFILE Routes
        Route::get('/profile', 'admin\ProductController@profile')->middleware(phone_valid::class);
        Route::put('/profile/update', 'admin\ProductController@updateprofile')->middleware(phone_valid::class);

        //UPLOAD Routes
        Route::post('/upload/png', 'admin\ProductController@uploadpng')->middleware(phone_valid::class);
        Route::post('/upload/jpg', 'admin\ProductController@uploadjpg')->middleware(phone_valid::class)->middleware("verified");


        //FILES Routes
        Route::get('/files', 'admin\ProductController@files')->middleware(phone_valid::class);
        Route::get('/files/upload', 'admin\ProductController@filesupload')->middleware(phone_valid::class);
        Route::post('/uploadfile', 'admin\ProductController@dofilesupload')->middleware("verified")->middleware(phone_valid::class);
        Route::post('/storefile', 'admin\ProductController@storefilesupload')->middleware("verified")->middleware(phone_valid::class);
        Route::post('/destroy', 'admin\ProductController@destroyfilesupload')->middleware("verified")->middleware(phone_valid::class);


        //ECT Routes
        Route::get('/profiletest', 'admin\ProductController@ptest')->middleware(phone_valid::class);
        Route::post('/post/insert', 'admin\ProductController@fileUpload')->middleware(phone_valid::class)->middleware("verified");
        Route::get('/imgod/{ID}', 'admin\ProductController@imgod')->middleware(phone_valid::class);
        Route::get('/roles', 'admin\ProductController@roles')->middleware(phone_valid::class);
        Route::get('/wlc', 'admin\ProductController@wlc')->middleware(phone_valid::class);
        Route::get('/otp', 'admin\ProductController@otp');
        Route::put('/otpcheck', 'admin\ProductController@otp_checker');
        Route::put('/otpaddphone', 'admin\ProductController@otpaddphone');
        Route::get('/newadmin', 'admin\ProductController@new_admin');

        //Buy Routes
        Route::get('/buy', 'admin\ProductController@buy')->middleware(phone_valid::class);
        Route::get('/buy/{type}', 'admin\ProductController@buytype')->middleware(phone_valid::class);
        Route::get('/purchase/{type}', 'admin\ProductController@purchasetype')->middleware(phone_valid::class);
        // Route::post('/post/insert', 'admin\ProductController@fileUpload');
        //page builder
        Route::get('/pagebuilder', 'admin\ProductController@pagebuilder')->middleware(phone_valid::class);
        Route::get('/pagebuilder/aboutus', 'admin\ProductController@aboutusbuilder')->middleware(phone_valid::class);
        Route::post('/gadget/add', 'admin\ProductController@add_gadget')->middleware(phone_valid::class);
        Route::post('/gadget/setting', 'admin\ProductController@setting_gadget')->middleware(phone_valid::class);
        Route::post('/gadget/setting-update', 'admin\ProductController@update_setting_gadget')->middleware(phone_valid::class);
        Route::post('/gadget/swap', 'admin\ProductController@swap_setting_gadget')->middleware(phone_valid::class);
        Route::post('/gadget/delete', 'admin\ProductController@delete_setting_gadget')->middleware(phone_valid::class);
        Route::get('/gadget/preview', 'admin\ProductController@preview_setting_gadget')->middleware(phone_valid::class);
        Route::get('/gadget/previewaboutus', 'admin\ProductController@previewaboutus_setting_gadget')->middleware(phone_valid::class);
        Route::post('/gadget/managepicture', 'admin\ProductController@managepicture_setting_gadget')->middleware(phone_valid::class);

});













Route::prefix('')->group( function () {
Route::get('/aboutus', 'StoreController@aboutus');
Route::get('/details/{id}', 'StoreController@details');
Route::post('/details/{id}/addcart', 'StoreController@addcart');
Route::post('/cart/updatecart', 'StoreController@updatecart');
Route::post('/cart/deletecart', 'StoreController@deletecart');
Route::get('/cart', 'StoreController@cart');
Route::get('/checkout', 'StoreController@checkout');
Route::get('/checkoutresult/{id}', 'StoreController@trk')->name('trk');
Route::get('/checker{Authority?}{Status?}', 'StoreController@zarinpal')->name('zarinpal');
Route::post('/checkoutresult', 'StoreController@trk2')->name('trk2');
Route::post('/sendmail', 'StoreController@sendmailorde')->name('sendmailorde1');
Route::get('/getfile/{ofile}/{orderid}', 'StoreController@getfile')->name('getfile');
Route::put('/checkoutstore', 'StoreController@checkoutstore');
Route::post('/order/track', 'StoreController@ordertrack');
Route::get('/order/track/{id}', 'StoreController@ordertrackid');
Route::get('/showmenu', 'StoreController@showmenu');
Route::get('/ctg/{id}', 'StoreController@showcategory');
});


Auth::routes();
